FROM sirehna/base-image-debian11-gcc10:2021-12-12

ARG PYTHON_MAJOR_VERSION=3
ARG PYTHON_MINOR_VERSION=10
ARG PYTHON_PATCH_VERSION=2
RUN apt-get update -yq || true \
 && apt-get install --yes --no-install-recommends \
    build-essential \
    zlib1g-dev \
    libncurses5-dev \
    libgdbm-dev \
    libnss3-dev \
    libssl-dev \
    libreadline-dev \
    libffi-dev \
    libsqlite3-dev \
    wget \
    libbz2-dev \
 && wget https://www.python.org/ftp/python/${PYTHON_MAJOR_VERSION}.${PYTHON_MINOR_VERSION}.${PYTHON_PATCH_VERSION}/Python-${PYTHON_MAJOR_VERSION}.${PYTHON_MINOR_VERSION}.${PYTHON_PATCH_VERSION}.tgz -O python.tgz \
 && mkdir -p python_src \
 && tar -xzf python.tgz --strip 1 -C python_src \
 && cd python_src \
 && ./configure --enable-optimizations \
 && make altinstall \
 && cd .. \
 && rm -rf python_src \
 && rm -f python.tgz \
 && python${PYTHON_MAJOR_VERSION}.${PYTHON_MINOR_VERSION} --version \
 && which python${PYTHON_MAJOR_VERSION}.${PYTHON_MINOR_VERSION} \
 && ln -s /usr/local/bin/python${PYTHON_MAJOR_VERSION}.${PYTHON_MINOR_VERSION} /usr/local/bin/python${PYTHON_MAJOR_VERSION} \
 && which python3
RUN python3 -m pip install -U pip \
 && python3 -m pip install \
    wheel \
    pybind11[global]==2.10.0
