#ifndef PY_SSC_HPP
#define PY_SSC_HPP

#include <pybind11/pybind11.h>

namespace py = pybind11;

void py_add_module_ssc(py::module& m);

#endif
