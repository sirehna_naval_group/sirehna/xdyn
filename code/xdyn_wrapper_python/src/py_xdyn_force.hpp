#ifndef PY_XDYN_FORCE_HPP
#define PY_XDYN_FORCE_HPP

#include <pybind11/pybind11.h>

namespace py = pybind11;

void py_add_module_xdyn_force(py::module& m);

#endif
