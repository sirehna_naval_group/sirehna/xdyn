#ifndef PY_XDYN_HDB_HPP
#define PY_XDYN_HDB_HPP

#include <pybind11/pybind11.h>

namespace py = pybind11;

void py_add_module_xdyn_hdb(py::module& m);

#endif
