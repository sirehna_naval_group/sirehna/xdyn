#ifndef PY_XDYN_ENV_HPP
#define PY_XDYN_ENV_HPP

#include <pybind11/pybind11.h>

namespace py = pybind11;

void py_add_module_xdyn_env(py::module& m);

#endif
