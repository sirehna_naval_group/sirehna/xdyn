.. xdyn documentation master file

Welcome to xdyn's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   xdyn
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
