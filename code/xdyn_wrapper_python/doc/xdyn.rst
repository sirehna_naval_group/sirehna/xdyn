.. _xdyn-description:

xdyn description
================

xdyn is a lightweight time-domain ship simulator modelling the dynamic behaviour
of a ship at sea, with its actuators, including some non-linear aspects of that
behaviour and featuring a customizable maneuvring model.
It simulates the mechanical behaviour of a solid body in a fluid environment by
solving Newton's second law of motion, taking hydrodynamic forces into account.