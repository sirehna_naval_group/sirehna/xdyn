.. _api-reference:

API Reference
=============

.. autosummary::
   :toctree: _autosummary
   :recursive:

   xdyn
   xdyn.force
   xdyn.env
   xdyn.env.wave
   xdyn.env.wave.io
   xdyn.env.wind
   xdyn.env.wind.io
   xdyn.hdb
   xdyn.core
   xdyn.core.io
