/*
 * generate_test_ship.hpp
 *
 *  Created on: Aug 26, 2014
 *      Author: cady
 */
#ifndef GENERATE_TEST_SHIP_HPP_
#define GENERATE_TEST_SHIP_HPP_

#include "xdyn/external_data_structures/GeometricTypes3d.hpp"

VectorOfVectorOfPoints test_ship();

#endif
