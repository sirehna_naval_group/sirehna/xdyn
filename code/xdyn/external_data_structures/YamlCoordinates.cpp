/*
 * YamlCoordinates.cpp
 *
 *  Created on: 16 avr. 2014
 *      Author: cady
 */

#include "YamlCoordinates.hpp"

YamlCoordinates::YamlCoordinates() : x(0), y(0), z(0)
{
}

YamlCoordinates::YamlCoordinates(const double x, const double y, const double z) : x(x), y(y), z(z)
{
}
