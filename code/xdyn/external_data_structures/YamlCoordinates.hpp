/*
 * YamlCoordinates.hpp
 *
 *  Created on: 16 avr. 2014
 *      Author: cady
 */

#ifndef YAMLCOORDINATES_HPP_
#define YAMLCOORDINATES_HPP_

struct YamlCoordinates
{
    YamlCoordinates();
    YamlCoordinates(const double x, const double y, const double z);
    virtual ~YamlCoordinates(){}
    double x;
    double y;
    double z;
};

#endif /* YAMLCOORDINATES_HPP_ */
