/*
 * YamlBody.hpp
 *
 *  Created on: 16 avr. 2014
 *      Author: cady
 */

#ifndef YAMLBODY_HPP_
#define YAMLBODY_HPP_

#include "xdyn/external_data_structures/YamlPosition.hpp"
#include "xdyn/external_data_structures/YamlSpeed.hpp"
#include "xdyn/external_data_structures/YamlDynamics.hpp"
#include "xdyn/external_data_structures/YamlModel.hpp"

#include <vector>
#include <string>

enum class InterpolationType {PIECEWISE_CONSTANT, LINEAR, SPLINE};
enum class BlockableState {U, V, W, P, Q, R};
enum class FilterableState {X, Y, Z, U, V, W, P, Q, R, PHI, THETA, PSI};

template <typename T> struct YamlDOF
{
    YamlDOF() :
        state(BlockableState::U),
        t(),
        value(),
        interpolation(InterpolationType::PIECEWISE_CONSTANT)
    {}
    virtual ~YamlDOF() {}
    BlockableState state;
    T t;
    T value;
    InterpolationType interpolation;
};

struct YamlCSVDOF : public YamlDOF<std::string>
{
    YamlCSVDOF();
    std::string filename;
};

struct YamlBlockedDOF
{
    YamlBlockedDOF();
    std::vector<YamlDOF<std::vector<double> > > from_yaml;
    std::vector<YamlCSVDOF> from_csv;
};

struct YamlFilteredStates
{
    YamlFilteredStates();
    std::string x;
    std::string y;
    std::string z;
    std::string u;
    std::string v;
    std::string w;
    std::string p;
    std::string q;
    std::string r;
    std::string phi;
    std::string theta;
    std::string psi;
};

struct YamlBody
{
    YamlBody();
    std::string name;
    std::string mesh;
    YamlPosition position_of_body_frame_relative_to_mesh;
    YamlPosition initial_position_of_body_frame_relative_to_NED_projected_in_NED;
    YamlSpeed initial_velocity_of_body_frame_relative_to_NED_projected_in_body;
    YamlDynamics dynamics;
    std::vector<YamlModel> external_forces;
    YamlBlockedDOF blocked_dof;
    YamlFilteredStates filtered_states;
};

#endif /* YAMLBODY_HPP_ */
