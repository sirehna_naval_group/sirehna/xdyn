/*
 * YamlDynamics6x6Matrix.cpp
 *
 *  Created on: 16 avr. 2014
 *      Author: cady
 */

#include "YamlDynamics6x6Matrix.hpp"

YamlDynamics6x6Matrix::YamlDynamics6x6Matrix():
    read_from_file(false),
    hdb_filename(),
    precal_filename(),
    frame(),
    row_convention_xdyn_with_z_down(true),
    row_1(),
    row_2(),
    row_3(),
    row_4(),
    row_5(),
    row_6()
{}
