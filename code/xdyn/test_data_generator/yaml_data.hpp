/*
 * yaml_data.hpp
 *
 *  Created on: 16 avr. 2014
 *      Author: cady
 */

#ifndef YAML_DATA_HPP_
#define YAML_DATA_HPP_

#include <string>

namespace test_data
{
    std::string bug_2655();
    std::string hydrostatic_test();
    std::string added_mass_from_hdb_file();
    std::string full_example();
    std::string full_example_with_diagonal_inertia();
    std::string full_example_with_propulsion();
    std::string full_example_with_propulsion_and_old_key_name();
    std::string falling_ball_example();
    std::string oscillating_cube_example();
    std::string new_oscillating_cube_example();
    std::string stable_cube_example();
    std::string stable_rolling_cube_test();
    std::string test_ship_hydrostatic_test(const std::string& type);
    std::string test_ship_exact_hydrostatic_test();
    std::string test_ship_fast_hydrostatic_test();
    std::string test_ship_propulsion();
    std::string test_ship_waves_test();
    std::string test_ship_diffraction();
    std::string waves();
    std::string simple_waves();
    std::string cube_in_waves();
    std::string waves_from_a_list_of_rays();
    std::string waves_for_parser_validation_only();
    std::string waves_from_a_list_of_rays_for_parser_validation_only();
    std::string waves_from_an_external_list_of_rays_for_parser_validation_only();
    std::string waves_from_an_external_list_of_rays_for_parser_validation_only_csv();
    std::string test_ship_froude_krylov();
    std::string falling_cube();
    std::string rolling_cube();
    std::string test_ship_damping();
    std::string controlled_forces();
    std::string bug_2961();
    std::string setpoints();
    std::string unknown_controller();
    std::string grpc_controller();
    std::string controllers();
    std::string wageningen();
    std::string resistance_curve();
    std::string propulsion_and_resistance();
    std::string dummy_controllers_and_commands_for_propulsion_and_resistance();
    std::string heading_keeping_base();
    std::string heading_keeping_controllers();
    std::string radiation_damping();
    std::string diffraction();
    std::string diffraction_precalr();
    std::string test_ship_radiation_damping();
    std::string simple_track_keeping();
    std::string maneuvering_commands();
    std::string maneuvering_with_same_frame_of_reference();
    std::string maneuvering();
    std::string bug_2641();
    std::string simple_station_keeping();
    std::string rudder();
    std::string bug_in_exact_hydrostatic();
    std::string bug_2714_heading_keeping();
    std::string bug_2714_station_keeping();
    std::string bug_2732();
    std::string L_config();
    std::string GM_cube();
    std::string bug_2838();
    std::string bug_2845();
    std::string maneuvering_with_commands();
    std::string kt_kq();
    std::string linear_hydrostatics();
    std::string test_ship_linear_hydrostatics_without_waves();
    std::string test_ship_linear_hydrostatics_with_waves();
    std::string fmi();
    std::string dummy_history();
    std::string complete_yaml_message_from_gui();
    std::string JSON_message_with_requested_output();
    std::string JSON_server_request_GM_cube_with_output();
    std::string complete_yaml_message_for_falling_ball();
    std::string bug_2963_hs_fast();
    std::string bug_2963_hs_exact();
    std::string bug_2963_fk();
    std::string bug_2963_diff();
    std::string bug_2963_gm();
    std::string bug_3004();
    std::string bug_3003();
    std::string bug_2984();
    std::string bug_3217();
    std::string bug_3227();
    std::string yml_bug_3230();
    std::string bug_3235();
    std::string bug_3207_yml();
    std::string bug_3241();
    std::string constant_force();
    std::string issue_20();
    std::string simserver_test_with_commands_and_delay();
    std::string simserver_message_without_Dt();
    std::string man_with_delay();
    std::string invalid_json_for_cs();
    std::string tutorial_09_gRPC_wave_model();
    std::string tutorial_10_gRPC_force_model();
    std::string tutorial_10_gRPC_force_model_commands();
    std::string grpc_setpoints();
    std::string gRPC_force_model();
    std::string gRPC_controller();
    std::string bug_3187();
    std::string bug_3185_with_invalid_frame();
    std::string bug_3185();
    std::string tutorial_11_gRPC_controller();
    std::string added_mass_from_precal_file();
    std::string tutorial_13_hdb_force_model();
    std::string tutorial_13_precal_r_force_model();
    std::string tutorial_14_filtered_states();
    std::string test_ship_gm_test();
}

#endif /* YAML_DATA_HPP_ */
