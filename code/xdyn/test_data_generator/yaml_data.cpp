/*
 * yaml_data.cpp
 *
 *  Created on: 16 avr. 2014
 *      Author: cady
 */

#include "yaml_data.hpp"
#include <boost/algorithm/string.hpp>
#include <sstream>

std::string rotation_convention();
std::string rotation_convention()
{
    return "rotations convention: [psi, theta', phi'']\n";
}

std::string environmental_constants();
std::string environmental_constants()
{
    return "environmental constants:\n"
           "    g: {value: 9.81, unit: m/s^2}\n"
           "    rho: {value: 1025, unit: kg/m^3}\n"
           "    nu: {value: 1.18e-6, unit: m^2/s}\n";
}

std::string environmental_models_no_waves_no_wind();
std::string environmental_models_no_waves_no_wind()
{
    return "environment models:\n"
           "  - model: no wind\n"
           "  - model: no waves\n"
           "    frame: NED\n"
           "    constant sea elevation in NED frame: {value: 0, unit: m}\n";
}

std::string directional_spreading();
std::string directional_spreading()
{
    return "        directional spreading:\n"
           "           type: dirac\n"
           "           waves propagating to: {value: 90, unit: deg}\n";
}

std::string discretization();
std::string discretization()
{
    return "    discretization:\n"
           "       n: 10\n"
           "       omega min: {value: 0.1, unit: rad/s}\n"
           "       omega max: {value: 6, unit: rad/s}\n"
           "       energy fraction: 0.999\n";
}

std::string discretization_with_ndir_nfreq();
std::string discretization_with_ndir_nfreq()
{
    return "    discretization:\n"
           "       ndir: 128\n"
           "       nfreq: 128\n"
           "       omega min: {value: 0.1, unit: rad/s}\n"
           "       omega max: {value: 6, unit: rad/s}\n"
           "       energy fraction: 0.999\n"
           "       equal energy bins: false\n";
}

std::string stretching();
std::string stretching()
{
    return "        stretching:\n"
           "          delta: 1\n"
           "          h: {unit: m, value: 0}\n";
}

std::string airy_depth_100();
std::string airy_depth_100()
{
    return "    spectra:\n"
           "      - model: airy\n"
           "        depth: {value: 100, unit: m}\n"
           "        seed of the random data generator: 0\n";
}

std::string position_relative_to_mesh(const double x, const double y, const double z, const double phi, const double theta, const double psi);
std::string position_relative_to_mesh(const double x, const double y, const double z, const double phi, const double theta, const double psi)
{
    std::stringstream ss;
    ss << "    position of body frame relative to mesh:\n"
       << "        frame: mesh\n"
       << "        x: {value: " << x << ", unit: m}\n"
       << "        y: {value: " << y << ", unit: m}\n"
       << "        z: {value: " << z << ", unit: m}\n"
       << "        phi: {value: " << phi << ", unit: rad}\n"
       << "        theta: {value: " << theta << ", unit: rad}\n"
       << "        psi: {value: " << psi << ", unit: rad}\n";
    return ss.str();
}

std::string initial_position_of_body_frame(const double x, const double y, const double z, const double phi, const double theta, const double psi);
std::string initial_position_of_body_frame(const double x, const double y, const double z, const double phi, const double theta, const double psi)
{
    std::stringstream ss;
    ss << "    initial position of body frame relative to NED:\n"
       << "        frame: NED\n"
       << "        x: {value: " << x << ", unit: m}\n"
       << "        y: {value: " << y << ", unit: m}\n"
       << "        z: {value: " << z << ", unit: m}\n"
       << "        phi: {value: " << phi << ", unit: rad}\n"
       << "        theta: {value: " << theta << ", unit: rad}\n"
       << "        psi: {value: " << psi << ", unit: rad}\n";
    return ss.str();
}

std::string initial_position_of_body_frame_deg(const double x, const double y, const double z, const double phi, const double theta, const double psi);
std::string initial_position_of_body_frame_deg(const double x, const double y, const double z, const double phi, const double theta, const double psi)
{
    std::stringstream ss;
    ss << "    initial position of body frame relative to NED:\n"
       << "        frame: NED\n"
       << "        x: {value: " << x << ", unit: m}\n"
       << "        y: {value: " << y << ", unit: m}\n"
       << "        z: {value: " << z << ", unit: m}\n"
       << "        phi: {value: " << phi << ", unit: deg}\n"
       << "        theta: {value: " << theta << ", unit: deg}\n"
       << "        psi: {value: " << psi << ", unit: deg}\n";
    return ss.str();
}

std::string initial_velocity(const std::string& frame, const double u, const double v, const double w, const double p, const double q, const double r);
std::string initial_velocity(const std::string& frame, const double u, const double v, const double w, const double p, const double q, const double r)
{
    std::stringstream ss;
    ss << "    initial velocity of body frame relative to NED:\n"
       << "        frame: " << frame << "\n"
       << "        u: {value: " << u << ", unit: m/s}\n"
       << "        v: {value: " << v << ", unit: m/s}\n"
       << "        w: {value: " << w << ", unit: m/s}\n"
       << "        p: {value: " << p << ", unit: rad/s}\n"
       << "        q: {value: " << q << ", unit: rad/s}\n"
       << "        r: {value: " << r << ", unit: rad/s}\n";
    return ss.str();
}

std::string initial_velocity_kt(const std::string& frame, const double u, const double v, const double w, const double p, const double q, const double r);
std::string initial_velocity_kt(const std::string& frame, const double u, const double v, const double w, const double p, const double q, const double r)
{
    std::stringstream ss;
    ss << "    initial velocity of body frame relative to NED:\n"
       << "        frame: " << frame << "\n"
       << "        u: {value: " << u << ", unit: kt}\n"
       << "        v: {value: " << v << ", unit: kt}\n"
       << "        w: {value: " << w << ", unit: kt}\n"
       << "        p: {value: " << p << ", unit: rad/s}\n"
       << "        q: {value: " << q << ", unit: rad/s}\n"
       << "        r: {value: " << r << ", unit: rad/s}\n";
    return ss.str();
}

std::string dynamics();
std::string dynamics()
{
    return "    dynamics:\n"
           "        hydrodynamic forces calculation point in body frame:\n"
           "            x: {value: 0.696, unit: m}\n"
           "            y: {value: 0, unit: m}\n"
           "            z: {value: 1.418, unit: m}\n"
           "        centre of inertia:\n"
           "            frame: TestShip\n"
           "            x: {value: 0.258, unit: m}\n"
           "            y: {value: 0, unit: m}\n"
           "            z: {value: 0.432, unit: m}\n"
           "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
           "            row 1: [253310,0,0,0,0,0]\n"
           "            row 2: [0,253310,0,0,0,0]\n"
           "            row 3: [0,0,253310,0,0,0]\n"
           "            row 4: [0,0,0,1.522e6,0,0]\n"
           "            row 5: [0,0,0,0,8.279e6,0]\n"
           "            row 6: [0,0,0,0,0,7.676e6]\n"
           "        added mass matrix at the center of gravity and projected in the body frame:\n"
           "            row 1: [3.519e4,0,0,0,0,0]\n"
           "            row 2: [0,3.023e5,0,0,0,0]\n"
           "            row 3: [0,0,1.980e5,0,0,0]\n"
           "            row 4: [0,0,0,3.189e5,0,0]\n"
           "            row 5: [0,0,0,0,8.866e6,0]\n"
           "            row 6: [0,0,0,0,0,6.676e6]\n";
}

std::string linear_damping();
std::string linear_damping()
{
    return "      - model: linear damping\n"
           "        damping matrix at the center of gravity projected in the body frame:\n"
           "            row 1: [ 0, 0,     0,      0,      0, 0]\n"
           "            row 2: [ 0, 0,     0,      0,      0, 0]\n"
           "            row 3: [ 0, 0, 1.9e5,      0,      0, 0]\n"
           "            row 4: [ 0, 0,     0, 1.74e4,      0, 0]\n"
           "            row 5: [ 0, 0,     0,      0, 4.67e6, 0]\n"
           "            row 6: [ 0, 0,     0,      0,      0, 0]\n";
}

std::string quadratic_damping();
std::string quadratic_damping()
{
    return "      - model: quadratic damping\n"
           "        damping matrix at the center of gravity projected in the body frame:\n"
           "            row 1: [ 0, 0, 0,      0, 0, 0]\n"
           "            row 2: [ 0, 0, 0,      0, 0, 0]\n"
           "            row 3: [ 0, 0, 0,      0, 0, 0]\n"
           "            row 4: [ 0, 0, 0, 1.45e6, 0, 0]\n"
           "            row 5: [ 0, 0, 0,      0, 0, 0]\n"
           "            row 6: [ 0, 0, 0,      0, 0, 0]\n";
}

std::string hydrodynamic_calculation_point();
std::string hydrodynamic_calculation_point()
{
    return "        hydrodynamic forces calculation point in body frame:\n"
           "            x: {value: 0.696, unit: m}\n"
           "            y: {value: 0, unit: m}\n"
           "            z: {value: 1.418, unit: m}\n";
}

std::string damping_matrix();
std::string damping_matrix()
{
    return "        damping matrix at the center of gravity projected in the body frame:\n"
           "            row 1: [ 0, 0,     0,      0,      0, 0]\n"
           "            row 2: [ 0, 0,     0,      0,      0, 0]\n"
           "            row 3: [ 0, 0, 1.9e5,      0,      0, 0]\n"
           "            row 4: [ 0, 0,     0, 1.74e4,      0, 0]\n"
           "            row 5: [ 0, 0,     0,      0, 4.67e6, 0]\n"
           "            row 6: [ 0, 0,     0,      0,      0, 0]\n";
}

std::string rigid_body_inertia_matrix();
std::string rigid_body_inertia_matrix()
{
    return "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
           "            row 1: [253310,0,0,0,0,0]\n"
           "            row 2: [0,253310,0,0,0,0]\n"
           "            row 3: [0,0,253310,0,0,0]\n"
           "            row 4: [0,0,0,1.522e6,0,0]\n"
           "            row 5: [0,0,0,0,8.279e6,0]\n"
           "            row 6: [0,0,0,0,0,7.676e6]\n";
}

std::string centre_of_inertia(const std::string& frame, const double x, const double y, const double z);
std::string centre_of_inertia(const std::string& frame, const double x, const double y, const double z)
{
    std::stringstream ss;
    ss << "        centre of inertia:\n"
       << "            frame: " << frame << "\n"
       << "            x: {value: " << x << ", unit: m}\n"
       << "            y: {value: " << y << ", unit: m}\n"
       << "            z: {value: " << z << ", unit: m}\n";
    return ss.str();
}

std::string test_data::bug_2655()
{
    return rotation_convention()
       +  "\n"
       +  environmental_constants()
       +  "environment models:\n"
       +  "  - model: no wind\n"
       +  "  - model: waves\n"
       +  discretization()
       +  airy_depth_100()
       +  stretching()
       +  directional_spreading()
       +  "        spectral density:\n"
       +  "           type: dirac\n"
       +  "           omega0: {value: 0.8976, unit: rad/s}\n"
       +  "           Hs: {value: 5, unit: m}\n"
       +  "    output:\n"
       +  "        frame of reference: NED\n"
       +  "        mesh:\n"
       +  "            xmin: {value: 1, unit: m}\n"
       +  "            xmax: {value: 5, unit: m}\n"
       +  "            nx: 0\n"
       +  "            ymin: {value: 1, unit: m}\n"
       +  "            ymax: {value: 2, unit: m}\n"
       +  "            ny: 0\n"
       +  "# Fixed frame: NED\n"
       +  "bodies: # All bodies have NED as parent frame\n"
       +  "  - name: TestShip\n"
       +  "    mesh: test_ship.stl\n"
       +  position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       +  initial_position_of_body_frame_deg(0,0,-0.099,0,-0.334,0)
       +  initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       +  dynamics()
       +  "    external forces:\n"
       +  "      - model: gravity\n"
       +  "      - model: non-linear hydrostatic (fast)\n"
       +  "      - model: non-linear Froude-Krylov\n"
       +  linear_damping()
       +  quadratic_damping()
       +  "\n"
       +  "output:\n"
       +  "   - format: csv\n"
       +  "     filename: houle_travers_5m_7s.csv\n"
       +  "     data: [t, x(TestShip), y(TestShip), z(TestShip), u(TestShip), v(TestShip), w(TestShip), p(TestShip), q(TestShip), r(TestShip), phi(TestShip), theta(TestShip), psi(TestShip)]\n";
}

std::string test_data::hydrostatic_test()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1000, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: no wind\n"
       + "  - model: no waves\n"
       + "    frame: NED\n"
       + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       + "    \n"
       + "# Fixed frame: NED\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: body 1\n"
       + "    mesh: test_ship.stl\n"
       + position_relative_to_mesh(10, 0, 0, 0, 0, 0)
       + initial_position_of_body_frame(0, 0, 0, 0, 0, 0)
       + initial_velocity("body", 0, 0, 0, 0, 0, 0)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("body 1", 0, 0, 0)
       + rigid_body_inertia_matrix()
       + "        added mass matrix at the center of gravity and projected in the body frame:\n"
       + "            row 1: [3.519e4,0,0,0,0,0]\n"
       + "            row 2: [0,3.023e5,0,0,0,0]\n"
       + "            row 3: [0,0,1.980e5,0,0,0]\n"
       + "            row 4: [0,0,0,3.189e5,0,0]\n"
       + "            row 5: [0,0,0,0,8.866e6,0]\n"
       + "            row 6: [0,0,0,0,0,6.676e6]\n"
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n"
       + "\n";
}

std::string test_data::added_mass_from_hdb_file()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1000, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: no wind\n"
       + "  - model: no waves\n"
       + "    frame: NED\n"
       + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       + "    \n"
       + "# Fixed frame: NED\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: body 1\n"
       + "    mesh: test_ship.stl\n"
       + position_relative_to_mesh(10, 0, 0, 0, 0, 0)
       + initial_position_of_body_frame(0, 0, 0, 0, 0, 0)
       + initial_velocity("body", 0, 0, 0, 0, 0, 0)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("body 1", 0,0,0)
       + rigid_body_inertia_matrix()
       + "        added mass matrix at the center of gravity and projected in the body frame:\n"
       + "            from hdb: test_ship.hdb\n"
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n"
       + "\n";
}

std::string test_data::full_example()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1000, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "    air rho: {value: 1.225, unit: kg/m^3}\n"
       + "environment models:\n"
       + "  - model: no wind\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 12, unit: m}\n"
       + "    \n"
       + "# Fixed frame: NED\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: body 1\n"
       + "    mesh: test_ship.stl\n"
       + position_relative_to_mesh(10, 0.21, 33000, 1, 3, 2)
       + initial_position_of_body_frame(4, 8, 12, 1.3, 1.4, 1.5)
       + initial_velocity("body 1", -8, -9, 14, 56, 7, 6)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("body 1", 4, 7, -10)
       + "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
       + "            row 1: [11,    11,    11,    11,    11,    11]\n"
       + "            row 2: [11,    12,    13,    14,    15,    16]\n"
       + "            row 3: [11,    13,    16,    20,    25,    31]\n"
       + "            row 4: [11,    14,    20,    30,    45,    66]\n"
       + "            row 5: [11,    15,    25,    45,    80,   136]\n"
       + "            row 6: [11,    16,    31,    66,   136,   262]\n"
       + "        added mass matrix at the center of gravity and projected in the body frame:\n"
       + "            row 1: [1,    1,    1,    1,    1,    1]\n"
       + "            row 2: [1,    2,    3,    4,    5,    6]\n"
       + "            row 3: [1,    3,    6,    10,   15,  21]\n"
       + "            row 4: [1,    4,    10,   20,   35,  56]\n"
       + "            row 5: [1,    5,    15,   35,   70, 126]\n"
       + "            row 6: [1,    6,    21,   56,  126, 252]\n"
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n"
       + "      - model: quadratic damping\n"
       + "        damping matrix at the center of gravity projected in the body frame:\n"
       + "            row 1: [  2,   3,   5,   7,  11,  13]\n"
       + "            row 2: [ 17,  19,  23,  29,  31,  37]\n"
       + "            row 3: [ 41,  43,  47,  53,  59,  61]\n"
       + "            row 4: [ 67,  71,  73,  79,  83,  89]\n"
       + "            row 5: [ 97, 101, 103, 107, 109, 113]\n"
       + "            row 6: [127, 131, 137, 139, 149, 151]\n"
       + "    blocked dof:\n"
       + "       from YAML:\n"
       + "         - state: u\n"
       + "           t: [0,4.2]\n"
       + "           value: [4,5]\n"
       + "           interpolation: linear\n"
       + "output:\n"
       + "   - format: csv\n"
       + "     filename: tutorial_1.csv\n"
       + "     data: [x(body 1)]\n"
       + "   - format: hdf5\n"
       + "     filename: tutorial_1.h5\n"
       + "     data: ['y(body 1)','z(body 1)','Fz(gravity,body 1,body 1)','waves']\n";
}

std::string test_data::full_example_with_diagonal_inertia()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1000, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: no wind\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 12, unit: m}\n"
       + "    \n"
       + "# Fixed frame: NED\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: body 1\n"
       + "    mesh: test_ship.stl\n"
       + position_relative_to_mesh(10, 0.21, 33000, 1, 3, 2)
       + initial_position_of_body_frame(4, 8, 12, 1.3, 1.4, 1.5)
       + initial_velocity("body 1", -8, -9, 14, 56, 7, 6)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("body 1", 4, 7, -10)
       + "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
       + "            row 1: [1, 0, 0, 0, 0, 0]\n"
       + "            row 2: [0, 2, 0, 0, 0, 0]\n"
       + "            row 3: [0, 0, 3, 0, 0, 0]\n"
       + "            row 4: [0, 0, 0, 4, 0, 0]\n"
       + "            row 5: [0, 0, 0, 0, 5, 0]\n"
       + "            row 6: [0, 0, 0, 0, 0, 6]\n"
       + "        added mass matrix at the center of gravity and projected in the body frame:\n"
       + "            row 1: [0, 0, 0, 0, 0, 0]\n"
       + "            row 2: [0, 0, 0, 0, 0, 0]\n"
       + "            row 3: [0, 0, 0, 0, 0, 0]\n"
       + "            row 4: [0, 0, 0, 0, 0, 0]\n"
       + "            row 5: [0, 0, 0, 0, 0, 0]\n"
       + "            row 6: [0, 0, 0, 0, 0, 0]\n"
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n"
       + "      - model: quadratic damping\n"
       + "        damping matrix at the center of gravity projected in the body frame:\n"
       + "            row 1: [  2,   3,   5,   7,  11,  13]\n"
       + "            row 2: [ 17,  19,  23,  29,  31,  37]\n"
       + "            row 3: [ 41,  43,  47,  53,  59,  61]\n"
       + "            row 4: [ 67,  71,  73,  79,  83,  89]\n"
       + "            row 5: [ 97, 101, 103, 107, 109, 113]\n"
       + "            row 6: [127, 131, 137, 139, 149, 151]\n"
       + "    blocked dof:\n"
       + "       from YAML:\n"
       + "         - state: u\n"
       + "           t: [0,4.2]\n"
       + "           value: [4,5]\n"
       + "           interpolation: linear\n"
       + "output:\n"
       + "   - format: csv\n"
       + "     filename: tutorial_1.csv\n"
       + "     data: [x(body 1)]\n"
       + "   - format: hdf5\n"
       + "     filename: tutorial_1.h5\n"
       + "     data: ['y(body 1)','z(body 1)','Fz(gravity,body 1,body 1)','waves']\n";
}

std::string test_data::full_example_with_propulsion()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1000, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "    air rho: {value: 1.225, unit: kg/m^3}\n"
       + "environment models:\n"
       + "  - model: no wind\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 12, unit: m}\n"
       + "    \n"
       + "# Fixed frame: NED\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: body 1\n"
       + "    mesh: test_ship.stl\n"
       + position_relative_to_mesh(10, 0.21, 33000, 1, 3, 2)
       + initial_position_of_body_frame(4, 8, 12, 1.3, 1.4, 1.5)
       + initial_velocity("body 1", -8, -9, 14, 56, 7, 6)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("body 1", 4, 7, -10)
       + "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
       + "            row 1: [1,2,3,4,5,6]\n"
       + "            row 2: [7,2,1,2,11,13]\n"
       + "            row 3: [20,30,3,80,0.9,0.1]\n"
       + "            row 4: [0,0,0,4,0,0]\n"
       + "            row 5: [0,0,0,0,5,0]\n"
       + "            row 6: [0,0,0,0,0,6]\n"
       + "        added mass matrix at the center of gravity and projected in the body frame:\n"
       + "            row 1: [1,2,3,4,5,6]\n"
       + "            row 2: [-7,2,1,2,11,13]\n"
       + "            row 3: [-20,30,3,80,0.9,0.1]\n"
       + "            row 4: [0,0,0,4.1,0,0]\n"
       + "            row 5: [0,0,0,0,5.2,0]\n"
       + "            row 6: [0,0,0,0,0,6.3]\n"
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n"
       + "      - model: quadratic damping\n"
       + "        damping matrix at the center of gravity projected in the body frame:\n"
       + "            row 1: [  2,   3,   5,   7,  11,  13]\n"
       + "            row 2: [ 17,  19,  23,  29,  31,  37]\n"
       + "            row 3: [ 41,  43,  47,  53,  59,  61]\n"
       + "            row 4: [ 67,  71,  73,  79,  83,  89]\n"
       + "            row 5: [ 97, 101, 103, 107, 109, 113]\n"
       + "            row 6: [127, 131, 137, 139, 149, 151]\n"
       + "      - name: port side propeller\n"
       + "        model: wageningen B-series\n"
       + "        position of propeller frame:\n"
       + "            frame: mesh(body 1)\n"
       + "            x: {value: -4, unit: m}\n"
       + "            y: {value: -2, unit: m}\n"
       + "            z: {value: 2, unit: m}\n"
       + "            phi: {value: 0, unit: rad}\n"
       + "            theta: {value: -10, unit: deg}\n"
       + "            psi: {value: -1, unit: deg}\n"
       + "        wake coefficient w: 0.9\n"
       + "        relative rotative efficiency etaR: 1\n"
       + "        thrust deduction factor t: 0.7\n"
       + "        rotation: clockwise\n"
       + "        number of blades: 3\n"
       + "        blade area ratio AE/A0: 0.5\n"
       + "        diameter: {value: 2, unit: m}\n"
       + "      - name: starboard propeller\n"
       + "        model: wageningen B-series\n"
       + "        position of propeller frame:\n"
       + "            frame: mesh(body 1)\n"
       + "            relative to: mesh(body 1)\n"
       + "            x: {value: -4, unit: m}\n"
       + "            y: {value: 2, unit: m}\n"
       + "            z: {value: 2, unit: m}\n"
       + "            phi: {value: 0, unit: rad}\n"
       + "            theta: {value: -10, unit: deg}\n"
       + "            psi: {value: 1, unit: deg}\n"
       + "        wake coefficient w: 0.9\n"
       + "        relative rotative efficiency etaR: 1\n"
       + "        thrust deduction factor t: 0.7\n"
       + "        rotation: anti-clockwise\n"
       + "        number of blades: 3\n"
       + "        blade area ratio AE/A0: 0.5\n"
       + "        diameter: {value: 2, unit: m}\n";
}

std::string test_data::full_example_with_propulsion_and_old_key_name()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1000, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: no wind\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 12, unit: m}\n"
       + "    \n"
       + "# Fixed frame: NED\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: body 1\n"
       + "    mesh: test_ship.stl\n"
       + position_relative_to_mesh(10, 0.21, 33000, 1, 3, 2)
       + initial_position_of_body_frame(4, 8, 12, 1.3, 1.4, 1.5)
       + initial_velocity("body 1", -8, -9, 14, 56, 7, 6)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("body 1", 4, 7, -10)
       + "        rigid body inertia matrix at the center of buoyancy projected in the body frame:\n"
       + "            row 1: [1,2,3,4,5,6]\n"
       + "            row 2: [7,2,1,2,11,13]\n"
       + "            row 3: [20,30,3,80,0.9,0.1]\n"
       + "            row 4: [0,0,0,4,0,0]\n"
       + "            row 5: [0,0,0,0,5,0]\n"
       + "            row 6: [0,0,0,0,0,6]\n"
       + "        added mass matrix at the center of buoyancy projected in the body frame:\n"
       + "            row 1: [1,2,3,4,5,6]\n"
       + "            row 2: [-7,2,1,2,11,13]\n"
       + "            row 3: [-20,30,3,80,0.9,0.1]\n"
       + "            row 4: [0,0,0,4.1,0,0]\n"
       + "            row 5: [0,0,0,0,5.2,0]\n"
       + "            row 6: [0,0,0,0,0,6.3]\n"
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n"
       + "      - model: quadratic damping\n"
       + "        damping matrix at the center of gravity projected in the body frame:\n"
       + "            row 1: [  2,   3,   5,   7,  11,  13]\n"
       + "            row 2: [ 17,  19,  23,  29,  31,  37]\n"
       + "            row 3: [ 41,  43,  47,  53,  59,  61]\n"
       + "            row 4: [ 67,  71,  73,  79,  83,  89]\n"
       + "            row 5: [ 97, 101, 103, 107, 109, 113]\n"
       + "            row 6: [127, 131, 137, 139, 149, 151]\n"
       + "      - name: port side propeller\n"
       + "        model: wageningen B-series\n"
       + "        position of propeller frame:\n"
       + "            frame: mesh(body 1)\n"
       + "            x: {value: -4, unit: m}\n"
       + "            y: {value: -2, unit: m}\n"
       + "            z: {value: 2, unit: m}\n"
       + "            phi: {value: 0, unit: rad}\n"
       + "            theta: {value: -10, unit: deg}\n"
       + "            psi: {value: -1, unit: deg}\n"
       + "        wake coefficient w: 0.9\n"
       + "        relative rotative efficiency etaR: 1\n"
       + "        thrust deduction factor t: 0.7\n"
       + "        rotation: clockwise\n"
       + "        number of blades: 3\n"
       + "        blade area ratio AE/A0: 0.5\n"
       + "        diameter: {value: 2, unit: m}\n"
       + "      - name: starboard propeller\n"
       + "        model: wageningen B-series\n"
       + "        position of propeller frame:\n"
       + "            frame: mesh(body 1)\n"
       + "            relative to: mesh(body 1)\n"
       + "            x: {value: -4, unit: m}\n"
       + "            y: {value: 2, unit: m}\n"
       + "            z: {value: 2, unit: m}\n"
       + "            phi: {value: 0, unit: rad}\n"
       + "            theta: {value: -10, unit: deg}\n"
       + "            psi: {value: 1, unit: deg}\n"
       + "        wake coefficient w: 0.9\n"
       + "        relative rotative efficiency etaR: 1\n"
       + "        thrust deduction factor t: 0.7\n"
       + "        rotation: anti-clockwise\n"
       + "        number of blades: 3\n"
       + "        blade area ratio AE/A0: 0.5\n"
       + "        diameter: {value: 2, unit: m}\n";
}

std::string one_ton_rigid_inertia_matrix();
std::string one_ton_rigid_inertia_matrix()
{
    return "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
           "            row 1: [1E3,0,0,0,0,0]\n"
           "            row 2: [0,1E3,0,0,0,0]\n"
           "            row 3: [0,0,1E3,0,0,0]\n"
           "            row 4: [0,0,0,1E3,0,0]\n"
           "            row 5: [0,0,0,0,1E3,0]\n"
           "            row 6: [0,0,0,0,0,1E3]\n";
}

std::string no_added_mass();
std::string no_added_mass()
{
    return "        added mass matrix at the center of gravity and projected in the body frame:\n"
           "            row 1: [0,0,0,0,0,0]\n"
           "            row 2: [0,0,0,0,0,0]\n"
           "            row 3: [0,0,0,0,0,0]\n"
           "            row 4: [0,0,0,0,0,0]\n"
           "            row 5: [0,0,0,0,0,0]\n"
           "            row 6: [0,0,0,0,0,0]\n";
}

std::string test_data::falling_ball_example()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1000, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models: []\n"
       + "\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: ball\n"
       + position_relative_to_mesh(0, 0, -10, 1, 3, 2)
       + initial_position_of_body_frame(4, 8, 12, 0, 0, 0)
       + initial_velocity("ball", 1, 0, 0, 0, 0, 0)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("ball", 0, 0, 0.5)
       + "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
       + "            row 1: [1E6,0,0,0,0,0]\n"
       + "            row 2: [0,1E6,0,0,0,0]\n"
       + "            row 3: [0,0,1E6,0,0,0]\n"
       + "            row 4: [0,0,0,1E6,0,0]\n"
       + "            row 5: [0,0,0,0,1E6,0]\n"
       + "            row 6: [0,0,0,0,0,1E6]\n"
       + no_added_mass()
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "output:\n"
       + "   - format: csv\n"
       + "     filename: falling_ball.csv\n"
       + "     data: [x(ball)]\n"
       + "   - format: hdf5\n"
       + "     filename: falling_ball.h5\n"
       + "     data: ['x(ball)','y(ball)','z(ball)','Fz(gravity,ball,ball)']\n"
       + "   - format: json\n"
       + "     filename: falling_ball.json\n"
       + "     data: ['x(ball)','y(ball)','z(ball)','qr(ball)','qi(ball)','qj(ball)','qk(ball)']\n";
}

std::string test_data::simserver_test_with_commands_and_delay()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1000, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models: []\n"
       + "\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: ball\n"
       + position_relative_to_mesh(0, 0, 0, 0, 0, 0)
       + initial_position_of_body_frame(0, 0, 0, 0, 0, 0)
       + initial_velocity("ball", 0, 0, 0, 0, 0, 0)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("ball", 0, 0, 0)
       + "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
       + "            row 1: [1,0,0,0,0,0]\n"
       + "            row 2: [0,1,0,0,0,0]\n"
       + "            row 3: [0,0,1,0,0,0]\n"
       + "            row 4: [0,0,0,1,0,0]\n"
       + "            row 5: [0,0,0,0,1,0]\n"
       + "            row 6: [0,0,0,0,0,1]\n"
       + no_added_mass()
       + "    external forces:\n"
       + "      - model: maneuvering\n"
       + "        name: F1\n"
       + "        reference frame:\n"
       + "            frame: ball\n"
       + "            x: {value: 0, unit: m}\n"
       + "            y: {value: 0, unit: m}\n"
       + "            z: {value: 0, unit: m}\n"
       + "            phi: {value: 0, unit: deg}\n"
       + "            theta: {value: 0, unit: deg}\n"
       + "            psi: {value: 0, unit: deg}\n"
       + "        commands: [command1, b, a]\n"
       + "        X: x(t)\n"
       + "        Y: y(t-10)\n"
       + "        Z: command1*z(t)\n"
       + "        K: b*u(t-6)\n"
       + "        M: v(t-6) + command1*w(t-5) + 2*b*p(t-4) + q(t-3)/a\n"
       + "        N: 0";
}

std::string test_data::falling_cube()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1026, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models: []\n"
       + "\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: cube\n"
       + "    mesh: cube.stl\n"
       + position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
       + initial_position_of_body_frame(0, 0, 0, 0, 0, 0)
       + initial_velocity("cube", 0, 0, 0, 0, 0, 0)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("cube", 0, 0, 0.5)
       + one_ton_rigid_inertia_matrix()
       + no_added_mass()
       + "    external forces:\n"
       + "      - model: gravity\n";
}

std::string test_data::rolling_cube()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1026, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       + "\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: cube\n"
       + "    mesh: cube.stl\n"
       + position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
       + initial_position_of_body_frame_deg(0, 0, 0, 0, 10, 0)
       + initial_velocity("cube", 0, 0, 0, 0, 0, 0)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("cube", 0, 0, 0.5)
       + one_ton_rigid_inertia_matrix()
       + no_added_mass()
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n";
}

std::string test_data::oscillating_cube_example()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1026, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       + "\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: cube\n"
       + "    mesh: cube.stl\n"
       + position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
       + initial_position_of_body_frame(0, 0, 0.5, 0, 0, 0)
       + initial_velocity("cube", 0, 0, 0, 0, 0, 0)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("cube", 0, 0, 0)
       + one_ton_rigid_inertia_matrix()
       + no_added_mass()
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n";
}

std::string test_data::new_oscillating_cube_example()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1000, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       + "\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: cube\n"
       + "    mesh: big_cube.stl\n"
       + position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
       + initial_position_of_body_frame_deg(0, 0, 0, 25, 0, 0)
       + initial_velocity("cube", 0, 0, 0, 0, 0, 0)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("cube", 0, 0, 0)
       + "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
       + "            row 1: [5E2,0,0,0,0,0]\n"
       + "            row 2: [0,5E2,0,0,0,0]\n"
       + "            row 3: [0,0,5E2,0,0,0]\n"
       + "            row 4: [0,0,0,5E2,0,0]\n"
       + "            row 5: [0,0,0,0,5E2,0]\n"
       + "            row 6: [0,0,0,0,0,5E2]\n"
       + no_added_mass()
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n";
}


std::string test_data::stable_cube_example()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1026, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       + "\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: cube\n"
       + "    mesh: cube.stl\n"
       + position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
       + initial_position_of_body_frame(0, 0, 0.4746588693957115, 0, 0, 0)
       + initial_velocity("cube", 0, 0, 0, 0, 0, 0)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("cube", 0, 0, 0)
       + one_ton_rigid_inertia_matrix()
       + no_added_mass()
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n";
}

std::string test_data::stable_rolling_cube_test()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1000, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       + "    output:\n"
       + "        frame of reference: cube\n"
       + "        mesh:\n"
       + "            xmin: {value: -10, unit: m}\n"
       + "            xmax: {value: 1, unit: km}\n"
       + "            nx: 10\n"
       + "            ymin: {value: -20, unit: m}\n"
       + "            ymax: {value: 3, unit: km}\n"
       + "            ny: 20\n"
       + "\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: cube\n"
       + "    mesh: cube.stl\n"
       + position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
       + initial_position_of_body_frame_deg(0, 0, 0.25, 0, 2, 0)
       + initial_velocity("cube", 0, 0, 0, 0, 0, 0)
       + "    dynamics:\n"
       + hydrodynamic_calculation_point()
       + centre_of_inertia("cube", 0, 0, 0.4)
       + "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
       + "            row 1: [83.33,0,0,0,0,0]\n"
       + "            row 2: [0,83.33,0,0,0,0]\n"
       + "            row 3: [0,0,83.33,0,0,0]\n"
       + "            row 4: [0,0,0,83.33,0,0]\n"
       + "            row 5: [0,0,0,0,83.33,0]\n"
       + "            row 6: [0,0,0,0,0,83.33]\n"
       + no_added_mass()
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n"
       + "\n";
}

std::string test_data::test_ship_hydrostatic_test(const std::string& type)
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, -5, 0, -0.0058, 0)
       << initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: " << type << "\n"
       << "    \n";
    return ss.str();
}


std::string test_data::test_ship_gm_test()
{
    return  rotation_convention() +
       + "\n"
       + environmental_constants()
       + "environment models:\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       + "    \n"
       + "# Fixed frame: NED\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: TestShip\n"
       + "    mesh: test_ship.stl\n"
       + position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       + initial_position_of_body_frame(0, 0, -5, 0, -0.0058, 0)
       + initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       + dynamics()
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: GM\n"
       + "        name of hydrostatic force model: non-linear hydrostatic (fast)\n"
       + "        roll step: {value: 10, unit: deg}\n";
}

std::string test_data::test_ship_propulsion()
{
    return rotation_convention()
       + "\n"
       + environmental_constants()
       + "environment models:\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       + "    \n"
       + "# Fixed frame: NED\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: TestShip\n"
       + "    mesh: test_ship.stl\n"
       + position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       + initial_position_of_body_frame(0, 0, 1, 0, -0.0058, 0)
       + initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       + dynamics()
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n"
       + "      - model: quadratic damping\n"
       + damping_matrix()
       + "      - model: resistance curve\n"
       + "        speed: {unit: m/s, values: [-1,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8]}\n"
       + "        resistance: {unit: N, values: [-7.728E+02,0.000E+00,2.102E+02,7.728E+02,1.647E+03,2.803E+03,4.230E+03,5.999E+03,8.498E+03,1.273E+04,2.084E+04,2.789E+04,4.238E+04,7.737E+04,1.449E+05,2.439E+05,3.590E+05,4.741E+05]}\n"
       + "      - name: propeller\n"
       + "        model: wageningen B-series\n"
       + "        position of propeller frame:\n"
       + "            frame: TestShip\n"
       + "            x: {value: -8.4, unit: m}\n"
       + "            y: {value: 0, unit: m}\n"
       + "            z: {value: 2.5, unit: m}\n"
       + "            phi: {value: 0, unit: rad}\n"
       + "            theta: {value: 3, unit: deg}\n"
       + "            psi: {value: 0, unit: deg}\n"
       + "        wake coefficient w: 0\n"
       + "        relative rotative efficiency etaR: 1\n"
       + "        thrust deduction factor t: 0\n"
       + "        rotation: clockwise\n"
       + "        number of blades: 4\n"
       + "        blade area ratio AE/A0: 0.55\n"
       + "        diameter: {value: 1.925, unit: m}\n"
       + "    \n";
}

std::string test_data::test_ship_fast_hydrostatic_test()
{
    return test_ship_hydrostatic_test("non-linear hydrostatic (fast)");
}

std::string test_data::test_ship_exact_hydrostatic_test()
{
    return test_ship_hydrostatic_test("non-linear hydrostatic (exact)");
}

std::string test_data::test_ship_waves_test()
{
    return rotation_convention()
       + "\n"
       + environmental_constants()
       + "environment models:\n"
       + "  - model: waves\n"
       + discretization()
       + airy_depth_100()
       + stretching()
       + directional_spreading()
       + "        spectral density:\n"
       + "           type: jonswap\n"
       + "           Hs: {value: 5, unit: m}\n"
       + "           Tp: {value: 15, unit: s}\n"
       + "           gamma: 1.2\n"
       + "      - model: airy\n"
       + "        depth: {value: 100, unit: m}\n"
       + "        seed of the random data generator: 10\n"
       + stretching()
       + "        directional spreading:\n"
       + "           type: cos2s\n"
       + "           s: 2\n"
       + "           waves propagating to: {value: 90, unit: deg}\n"
       + "        spectral density:\n"
       + "           type: dirac\n"
       + "           omega0: {value: 0.05, unit: rad/s}\n"
       + "           Hs: {value: 15, unit: m}\n"
       + "    output:\n"
       + "        frame of reference: NED\n"
       + "        mesh:\n"
       + "            xmin: {value: 1, unit: m}\n"
       + "            xmax: {value: 5, unit: m}\n"
       + "            nx: 2\n"
       + "            ymin: {value: 1, unit: m}\n"
       + "            ymax: {value: 2, unit: m}\n"
       + "            ny: 2\n"
       + "# Fixed frame: NED\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: TestShip\n"
       + "    mesh: test_ship.stl\n"
       + position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       + initial_position_of_body_frame(0, 0, -5, 0, -0.0058, 0)
       + initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       + dynamics()
       + "    external forces:\n"
       + "      - model: gravity\n"
       + "      - model: non-linear hydrostatic (fast)\n"
       + "output:\n"
       + "- format: hdf5\n"
       + "  filename: test.h5\n"
       + "  data: [mesh, command line, yaml, python scripts, matlab scripts, spectra, waves]\n";
}

std::string test_data::waves()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1026, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: waves\n"
       + discretization_with_ndir_nfreq()
       + airy_depth_100()
       + stretching()
       + directional_spreading()
       + "        spectral density:\n"
       + "           type: jonswap\n"
       + "           Hs: {value: 5, unit: m}\n"
       + "           Tp: {value: 15, unit: s}\n"
       + "           gamma: 1.2\n"
       + "      - model: airy\n"
       + "        depth: {value: 100, unit: m}\n"
       + "        seed of the random data generator: 10\n"
       + stretching()
       + "        directional spreading:\n"
       + "           type: cos2s\n"
       + "           s: 2\n"
       + "           waves propagating to: {value: 90, unit: deg}\n"
       + "        spectral density:\n"
       + "           type: dirac\n"
       + "           omega0: {value: 0.05, unit: rad/s}\n"
       + "           Hs: {value: 15, unit: m}\n"
       + "    output:\n"
       + "        frame of reference: NED\n"
       + "        mesh:\n"
       + "            xmin: {value: 1, unit: m}\n"
       + "            xmax: {value: 5, unit: m}\n"
       + "            nx: 5\n"
       + "            ymin: {value: 1, unit: m}\n"
       + "            ymax: {value: 2, unit: m}\n"
       + "            ny: 2\n"
       + "output:\n"
       + "   - format: hdf5\n"
       + "     filename: waves.h5\n"
       + "     data: [command line, yaml, spectra, waves]\n";
}

std::string test_data::simple_waves()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1026, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: waves\n"
       + "    discretization:\n"
       + "       n: 128\n"
       + "       omega min: {value: 0.1, unit: rad/s}\n"
       + "       omega max: {value: 6, unit: rad/s}\n"
       + "       energy fraction: 0.999\n"
       + airy_depth_100()
       + stretching()
       + directional_spreading()
       + "        spectral density:\n"
       + "           type: dirac\n"
       + "           omega0: {value: 0.05, unit: rad/s}\n"
       + "           Hs: {value: 15, unit: m}\n"
       + "    output:\n"
       + "        frame of reference: NED\n"
       + "        mesh:\n"
       + "            xmin: {value: -10, unit: m}\n"
       + "            xmax: {value: 10, unit: m}\n"
       + "            nx: 21\n"
       + "            ymin: {value: -20, unit: m}\n"
       + "            ymax: {value: +20, unit: m}\n"
       + "            ny: 41\n"
       + "output:\n"
       + "   - format: hdf5\n"
       + "     filename: simple_waves.h5\n"
       + "     data: ['waves']\n";
}

std::string test_data::waves_for_parser_validation_only()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "discretization:\n"
       << "   n: 128\n"
       << "   omega min: {value: 0.1, unit: rad/s}\n"
       << "   omega max: {value: 6, unit: deg/s}\n"
       << "   energy fraction: 0.123\n"
       << "   equal energy bins: true\n"
       << "spectra:\n"
       << "  - model: airy\n"
       << "    seed of the random data generator: 0\n"
       << "    stretching:\n"
       << "       delta: 0.5\n"
       << "       h: {unit: m, value: 100}\n"
       << "    depth: {value: 1.7, unit: km}\n"
       << "    directional spreading:\n"
       << "       type: dirac\n"
       << "       waves propagating to: {value: 90, unit: deg}\n"
       << "    spectral density:\n"
       << "       type: jonswap\n"
       << "       Hs: {value: 5, unit: m}\n"
       << "       Tp: {value: 15, unit: s}\n"
       << "       gamma: 1.2\n"
       << "  - model: airy\n"
       << "    depth: {value: 12, unit: m}\n"
       << "    seed of the random data generator: 1872\n"
       << "    stretching:\n"
       << "         delta: 0.6\n"
       << "         h: {unit: m, value: 101}\n"
       << "    directional spreading:\n"
       << "       type: cos2s\n"
       << "       s: 2\n"
       << "       waves propagating to: {value: 90, unit: deg}\n"
       << "    spectral density:\n"
       << "       type: dirac\n"
       << "       Hs: {value: 5, unit: m}\n"
       << "       Tp: {value: 15, unit: s}\n"
       << "output:\n"
       << "    frame of reference: NED\n"
       << "    mesh:\n"
       << "       xmin: {value: -10, unit: m}\n"
       << "       xmax: {value: 1, unit: km}\n"
       << "       nx: 10\n"
       << "       ymin: {value: -20, unit: m}\n"
       << "       ymax: {value: 3, unit: km}\n"
       << "       ny: 20\n";
    return ss.str();
}

std::string test_data::waves_from_an_external_list_of_rays_for_parser_validation_only()
{
    std::stringstream ss;
    ss << "spectra from a list of rays:\n"
       << "  - model: airy\n"
       << "    stretching:\n"
       << "       delta: 0.5\n"
       << "       h: {unit: m, value: 100}\n"
       << "    depth: {value: 1.7, unit: km}\n"
       << "    rays from file: rays.csv\n"
       << "  - model: airy\n"
       << "    rays:\n"
       << "      # Two rays with T=10s and T=12s\n"
       << "      a: {values: [0.1, 0.5], unit: m}\n"
       << "      psi: {values: [180.0, 180.0], unit: deg}\n"
       << "      omega: {values: [0.523598, 0.628318], unit: rad/s}\n"
       << "      k: {values: [2.689467, 3.872832], unit: 1/m}\n"
       << "      phase: {values: [110, 130], unit: deg}\n"
       << "    depth: {value: 1000, unit: m}\n"
       << "    stretching:\n"
       << "       delta: 0.6\n"
       << "       h: {unit: m, value: 101}\n"
       << "output:\n"
       << "    frame of reference: NED\n"
       << "    mesh:\n"
       << "       xmin: {value: -10, unit: m}\n"
       << "       xmax: {value: 1, unit: km}\n"
       << "       nx: 10\n"
       << "       ymin: {value: -20, unit: m}\n"
       << "       ymax: {value: 3, unit: km}\n"
       << "       ny: 20\n";
    return ss.str();
}

std::string test_data::waves_from_an_external_list_of_rays_for_parser_validation_only_csv()
{
    std::stringstream ss;
    ss << "a,psi,omega,k,phase\n";
    ss << "0.1,3.1415926535897931,0.628318,3.872832,0.8726646259971648\n";
    ss << "0.5,3.1415926535897931,0.785398,6.051301,1.5707963267948966\n";
    return ss.str();
}

std::string test_data::waves_from_a_list_of_rays_for_parser_validation_only()
{
    std::stringstream ss;
    ss << "spectra from a list of rays:\n"
       << "  - model: airy\n"
       << "    stretching:\n"
       << "       delta: 0.5\n"
       << "       h: {unit: m, value: 100}\n"
       << "    depth: {value: 1.7, unit: km}\n"
       << "    rays:\n"
       << "      # Two rays with T=8s and T=10s\n"
       << "      a: {values: [0.1, 0.5], unit: m}\n"
       << "      psi: {values: [180.0, 180.0], unit: deg}\n"
       << "      omega: {values: [0.628318, 0.785398], unit: rad/s}\n"
       << "      k: {values: [3.872832, 6.051301], unit: 1/m}\n"
       << "      phase: {values: [50, 90], unit: deg}\n"
       << "  - model: airy\n"
       << "    rays:\n"
       << "      # Two rays with T=10s and T=12s\n"
       << "      a: {values: [0.1, 0.5], unit: m}\n"
       << "      psi: {values: [180.0, 180.0], unit: deg}\n"
       << "      omega: {values: [0.523598, 0.628318], unit: rad/s}\n"
       << "      k: {values: [2.689467, 3.872832], unit: 1/m}\n"
       << "      phase: {values: [110, 130], unit: deg}\n"
       << "    depth: {value: 1000, unit: m}\n"
       << "    stretching:\n"
       << "       delta: 0.6\n"
       << "       h: {unit: m, value: 101}\n"
       << "output:\n"
       << "    frame of reference: NED\n"
       << "    mesh:\n"
       << "       xmin: {value: -10, unit: m}\n"
       << "       xmax: {value: 1, unit: km}\n"
       << "       nx: 10\n"
       << "       ymin: {value: -20, unit: m}\n"
       << "       ymax: {value: 3, unit: km}\n"
       << "       ny: 20\n";
    return ss.str();
}

std::string test_data::waves_from_a_list_of_rays()
{
    return rotation_convention()
       + "\n"
       + "environmental constants:\n"
       + "    g: {value: 9.81, unit: m/s^2}\n"
       + "    rho: {value: 1026, unit: kg/m^3}\n"
       + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       + "environment models:\n"
       + "  - model: waves\n"
       + "    spectra from a list of rays:\n"
       + "      - model: airy\n"
       + "        stretching:\n"
       + "           delta: 0.5\n"
       + "           h: {unit: m, value: 100}\n"
       + "        depth: {value: 1.7, unit: km}\n"
       + "        rays:\n"
       + "          # Two rays with T=8s and T=10s\n"
       + "          a: {values: [0.1, 0.5], unit: m}\n"
       + "          psi: {values: [180.0, 180.0], unit: deg}\n"
       + "          omega: {values: [0.628318, 0.785398], unit: rad/s}\n"
       + "          k: {values: [3.872832, 6.051301], unit: 1/m}\n"
       + "          phase: {values: [50, 90], unit: deg}\n"
       + "      - model: airy\n"
       + "        rays:\n"
       + "          # Two rays with T=10s and T=12s\n"
       + "          a: {values: [0.1, 0.5], unit: m}\n"
       + "          psi: {values: [180.0, 180.0], unit: deg}\n"
       + "          omega: {values: [0.523598, 0.628318], unit: rad/s}\n"
       + "          k: {values: [2.689467, 3.872832], unit: 1/m}\n"
       + "          phase: {values: [110, 130], unit: deg}\n"
       + "        depth: {value: 1000, unit: m}\n"
       + "        stretching:\n"
       + "           delta: 0.6\n"
       + "           h: {unit: m, value: 101}\n"
       + "    output:\n"
       + "        frame of reference: NED\n"
       + "        mesh:\n"
       + "           xmin: {value: -10, unit: m}\n"
       + "           xmax: {value: 1, unit: km}\n"
       + "           nx: 10\n"
       + "           ymin: {value: -20, unit: m}\n"
       + "           ymax: {value: 3, unit: km}\n"
       + "           ny: 20\n";
}

std::string test_data::cube_in_waves()
{
    std::stringstream ss;
        ss << rotation_convention()
           << "\n"
           << "environmental constants:\n"
           << "    g: {value: 9.81, unit: m/s^2}\n"
           << "    rho: {value: 1026, unit: kg/m^3}\n"
           << "    nu: {value: 1.18e-6, unit: m^2/s}\n"
           << "environment models:\n"
           << "  - model: waves\n"
           << "    discretization:\n"
           << "       n: 128\n"
           << "       omega min: {value: 0.1, unit: rad/s}\n"
           << "       omega max: {value: 6, unit: rad/s}\n"
           << "       energy fraction: 0.999\n"
           << "    spectra:\n"
           << "      - model: airy\n"
           << "        depth: {value: 100, unit: m}\n"
           << "        seed of the random data generator: 0\n"
           << "        stretching:\n"
           << "          delta: 1\n"
           << "          h: {unit: m, value: 0}\n"
           << "        directional spreading:\n"
           << "           type: dirac\n"
           << "           waves propagating to: {value: 90, unit: deg}\n"
           << "        spectral density:\n"
           << "           type: jonswap\n"
           << "           Hs: {value: 5, unit: m}\n"
           << "           Tp: {value: 15, unit: s}\n"
           << "           gamma: 1.2\n"
           << "      - model: airy\n"
           << "        depth: {value: 100, unit: m}\n"
           << "        seed of the random data generator: 10\n"
           << "        stretching:\n"
           << "          delta: 1\n"
           << "          h: {unit: m, value: 0}\n"
           << "        directional spreading:\n"
           << "           type: cos2s\n"
           << "           s: 2\n"
           << "           waves propagating to: {value: 90, unit: deg}\n"
           << "        spectral density:\n"
           << "           type: dirac\n"
           << "           Hs: {value: 5, unit: m}\n"
           << "           omega0: {value: 0.05, unit: rad/s}\n"
           << "    output:\n"
           << "        frame of reference: cube\n"
           << "        mesh:\n"
           << "            xmin: {value: 1, unit: m}\n"
           << "            xmax: {value: 5, unit: m}\n"
           << "            nx: 5\n"
           << "            ymin: {value: 1, unit: m}\n"
           << "            ymax: {value: 2, unit: m}\n"
           << "            ny: 2\n"
           << "\n"
           << "bodies: # All bodies have NED as parent frame\n"
           << "  - name: cube\n"
           << "    mesh: cube.stl\n"
           << position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
           << initial_position_of_body_frame(0, 0, 0.4746588693957115, 0, 0, 0)
           << initial_velocity("cube", 0, 0, 0, 0, 0, 0)
           << "    dynamics:\n"
           << "        hydrodynamic forces calculation point in body frame:\n"
           << "            x: {value: 0.696, unit: m}\n"
           << "            y: {value: 0, unit: m}\n"
           << "            z: {value: 1.418, unit: m}\n"
           << centre_of_inertia("cube", 0, 0, 0.5)
           << one_ton_rigid_inertia_matrix()
           << no_added_mass()
           << "    external forces:\n"
           << "      - model: gravity\n"
           << "      - model: non-linear hydrostatic (fast)\n";
        return ss.str();
}

std::string test_data::test_ship_froude_krylov()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: waves\n"
       << discretization()
       << airy_depth_100()
       << stretching()
       << directional_spreading()
       << "        spectral density:\n"
       << "           type: jonswap\n"
       << "           Hs: {value: 5, unit: m}\n"
       << "           Tp: {value: 15, unit: s}\n"
       << "           gamma: 1.2\n"
       << "      - model: airy\n"
       << "        depth: {value: 100, unit: m}\n"
       << "        seed of the random data generator: 10\n"
       << stretching()
       << "        directional spreading:\n"
       << "           type: cos2s\n"
       << "           s: 2\n"
       << "           waves propagating to: {value: 90, unit: deg}\n"
       << "        spectral density:\n"
       << "           type: dirac\n"
       << "           omega0: {value: 0.05, unit: rad/s}\n"
       << "           Hs: {value: 15, unit: m}\n"
       << "    output:\n"
       << "        frame of reference: NED\n"
       << "        mesh:\n"
       << "            xmin: {value: 1, unit: m}\n"
       << "            xmax: {value: 5, unit: m}\n"
       << "            nx: 2\n"
       << "            ymin: {value: 1, unit: m}\n"
       << "            ymax: {value: 2, unit: m}\n"
       << "            ny: 2\n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, -5, 0, -0.0058, 0)
       << initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: non-linear hydrostatic (fast)\n"
       << "      - model: non-linear Froude-Krylov\n"
       << "    \n";
    return ss.str();
}

std::string test_data::test_ship_diffraction()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: waves\n"
       << "    discretization:\n"
       << "       n: 10\n"
       << "       omega min: {value: 0.05026548, unit: rad/s}\n"
       << "       omega max: {value: 0.6366198, unit: rad/s}\n"
       << "       energy fraction: 0.999\n"
       << airy_depth_100()
       << stretching()
       << directional_spreading()
       << "        spectral density:\n"
       << "           type: jonswap\n"
       << "           Hs: {value: 5, unit: m}\n"
       << "           Tp: {value: 15, unit: s}\n"
       << "           gamma: 1.2\n"
       << "      - model: airy\n"
       << "        depth: {value: 100, unit: m}\n"
       << "        seed of the random data generator: 10\n"
       << stretching()
       << "        directional spreading:\n"
       << "           type: cos2s\n"
       << "           s: 2\n"
       << "           waves propagating to: {value: 90, unit: deg}\n"
       << "        spectral density:\n"
       << "           type: dirac\n"
       << "           omega0: {value: 0.6366198, unit: rad/s}\n"
       << "           Hs: {value: 15, unit: m}\n"
       << "    output:\n"
       << "        frame of reference: NED\n"
       << "        mesh:\n"
       << "            xmin: {value: 1, unit: m}\n"
       << "            xmax: {value: 5, unit: m}\n"
       << "            nx: 2\n"
       << "            ymin: {value: 1, unit: m}\n"
       << "            ymax: {value: 2, unit: m}\n"
       << "            ny: 2\n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, -5, 0, -0.0058, 0)
       << initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: non-linear hydrostatic (fast)\n"
       << "      - model: non-linear Froude-Krylov\n"
       << "      - model: diffraction\n"
       << "        hdb: test_ship.hdb\n"
       << "        calculation point in body frame:\n"
       << "            x: {value: 0.696, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 1.418, unit: m}\n"
       << "        mirror for 180 to 360: true\n"
       << "    \n";
    return ss.str();
}

std::string test_data::test_ship_damping()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    frame: NED\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "\n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, -5, 0, -0.0058, 0)
       << initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: non-linear hydrostatic (fast)\n"
       << "      - model: quadratic damping\n"
       << damping_matrix()
       << "    \n";
    return ss.str();
}

std::string test_data::controlled_forces()
{
    std::stringstream ss;
    ss << "commands:\n"
          "  - name: propeller\n"
          "    t: [0,1,3,10]\n"
          "    rpm: {unit: rad/s, values: [3, 30, 30, 40]}\n"
          "    P/D: {unit: 1, values: [1.064935,1.064935,1.064935,1.064935]}\n";

    return ss.str();
}

std::string test_data::bug_2961()
{
    std::stringstream ss;
    ss << "commands:\n"
          "  - name: controller\n"
          "    t: [0]\n"
          "    psi_co: {unit: deg, values: [30]}\n";

    return ss.str();
}

std::string test_data::setpoints()
{
    std::stringstream ss;
    ss << "setpoints:\n"
          "  - t: [0,1,3,10]\n"
          "    rpm_co: {unit: rad/s, values: [3, 30, 30, 40]}\n"
          "    P/D: {unit: 1, values: [1.064935,1.064935,1.064935,1.064935]}\n"
          "  - t: [4.2]\n"
          "    psi_co: {unit: rad, values: [2.5]}\n";

    return ss.str();
}

std::string test_data::grpc_setpoints()
{
    std::stringstream ss;
    ss << "setpoints:\n"
          "  - t: [0,1,3,10]\n"
          "    rpm_co: {unit: rad/s, values: [3, 30, 30, 40]}\n"
          "    propeller(P/D): {unit: 1, values: [1.064935,1.064935,1.064935,1.064935]}\n"
          "  - t: [4.2]\n"
          "    psi_co: {unit: rad, values: [2.5]}\n";

    return ss.str();
}

std::string test_data::unknown_controller()
{
    return "  - type: unknown\n"
           "    name: my awesome controller\n"
           "    dt: 0.01\n"
           "    url: some-url\n";
}

std::string test_data::grpc_controller()
{
    return "  - type: gRPC\n"
           "    name: my awesome controller\n"
           "    dt: 0.01\n"
           "    url: pid:9002\n"
           "    gains:\n"
           "        Kp: -1\n"
           "        Ki: 0\n"
           "        Kd: -1\n"
           "    setpoint: rpm_co\n"
           "    command: propeller(rpm)\n"
           "    state weights:\n"
           "        x: 1\n"
           "        y: -1\n";
}

std::string test_data::controllers()
{
    std::stringstream ss;
    ss << "controllers:\n"
          "  - type: pId\n"
          "    dt: 0.1\n"
          "    state weights:\n"
          "        x: 1\n"
          "        y: -1\n"
          "    setpoint: rpm_co\n"
          "    command: propeller(rpm)\n"
          "    gains:\n"
          "        Kp: 4.2\n"
          "        Ki: 0.25\n"
          "        Kd: 1\n"
          "    name: foo\n"
          "  - type: PID\n"
          "    dt: 0.5\n"
          "    state weights:\n"
          "        psi: 1\n"
          "    setpoint: psi_co\n"
          "    command: controller(psi)\n"
          "    name: bar\n"
          "    gains:\n"
          "        Kp: 0.52\n"
          "        Ki: 21.5\n"
          "        Kd: 2.2\n";
    return ss.str();
}

std::string test_data::wageningen()
{
    std::stringstream ss;
    ss << "name: port side propeller\n"
          "model: wageningen B-series\n"
          "position of propeller frame:\n"
          "    frame: mesh(body 1)\n"
          "    x: {value: -4, unit: m}\n"
          "    y: {value: -2, unit: m}\n"
          "    z: {value: 2, unit: m}\n"
          "    phi: {value: 0, unit: rad}\n"
          "    theta: {value: -10, unit: deg}\n"
          "    psi: {value: -1, unit: deg}\n"
          "wake coefficient w: 0.9\n"
          "relative rotative efficiency etaR: 1\n"
          "thrust deduction factor t: 0.7\n"
          "rotation: clockwise\n"
          "number of blades: 3\n"
          "blade area ratio AE/A0: 0.5\n"
          "diameter: {value: 2, unit: m}";
    return ss.str();
}

std::string test_data::resistance_curve()
{
    std::stringstream ss;
    ss << "speed: {unit: knot, values: [0,1,2,3,4,5,15,20]}\n"
       << "resistance: {unit: MN, values: [0,1,4,9,16,25,225,400]}";
    return ss.str();
}

std::string test_data::propulsion_and_resistance()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 1, 0, 0, 0)
       << initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       << "    dynamics:\n"
       << "        hydrodynamic forces calculation point in body frame:\n"
       << "            x: {value: 0.696, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 0.432, unit: m}\n"
       << centre_of_inertia("TestShip", 0.258, 0, 0.432)
       << rigid_body_inertia_matrix()
       << "        added mass matrix at the center of gravity and projected in the body frame:\n"
       << "            row 1: [3.519e4,0,0,0,0,0]\n"
       << "            row 2: [0,3.023e5,0,0,0,0]\n"
       << "            row 3: [0,0,1.980e5,0,0,0]\n"
       << "            row 4: [0,0,0,3.189e5,0,0]\n"
       << "            row 5: [0,0,0,0,8.866e6,0]\n"
       << "            row 6: [0,0,0,0,0,6.676e6]\n"
       << "    external forces:\n"
       << "      - model: resistance curve\n"
       << "        speed: {unit: m/s, values: [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8]}\n"
       << "        resistance: {unit: N, values: [0.000E+00,2.102E+02,7.728E+02,1.647E+03,2.803E+03,4.230E+03,5.999E+03,8.498E+03,1.273E+04,2.084E+04,2.789E+04,4.238E+04,7.737E+04,1.449E+05,2.439E+05,3.590E+05,4.741E+05]}\n"
       << "      - name: propeller\n"
       << "        model: wageningen B-series\n"
       << "        position of propeller frame:\n"
       << "            frame: TestShip\n"
       << "            x: {value: 0, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 0.432, unit: m}\n"
       << "            phi: {value: 0, unit: rad}\n"
       << "            theta: {value: 0, unit: deg}\n"
       << "            psi: {value: 0, unit: deg}\n"
       << "        wake coefficient w: 0\n"
       << "        relative rotative efficiency etaR: 1\n"
       << "        thrust deduction factor t: 0\n"
       << "        rotation: clockwise\n"
       << "        number of blades: 4\n"
       << "        blade area ratio AE/A0: 0.55\n"
       << "        diameter: {value: 1.925, unit: m}\n"
       << "    \n";
    return ss.str();
}

std::string test_data::dummy_controllers_and_commands_for_propulsion_and_resistance()
{
    /*
    Get this result in the datasource for t≥0:
    ```cpp
    commands.set<double>("propeller(rpm)", 100*(2*PI)/60.);
    commands.set<double>("propeller(P/D)", 1.064935);
    ```
    using both controllers and commands.
    */
    return "setpoints:\n"
           "  - t: [0]\n"
           "    rpm_co: {unit: rad/s, values: [10.4719755]}\n"
           "controllers:\n"
           "  - type: PID\n"
           "    name: foo\n"
           "    dt: 10\n"
           "    state weights:\n"
           "        x: 0\n"
           "    setpoint: rpm_co\n"
           "    command: propeller(rpm)\n"
           "    gains:\n"
           "        Kp: 1\n"
           "        Ki: 0\n"
           "        Kd: 0\n"
           "commands:\n"
           "  - name: propeller\n"
           "    t: [0]\n"
           "    P/D: {unit: 1, values: [1.064935]}\n";
}

std::string test_data::heading_keeping_base()
{
    return rotation_convention()
         + "\n"
         + "environmental constants:\n"
         + "    g: {value: 9.81, unit: m/s^2}\n"
         + "    rho: {value: 1026, unit: kg/m^3}\n"
         + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
         + "# Fixed frame: NED\n"
         + "bodies: # All bodies have NED as parent frame\n"
         + "  - name: dtmb\n"
         + position_relative_to_mesh(70.27, 0, -7.55, 0, 0, 0)
         + "    dynamics:\n"
         + "        hydrodynamic forces calculation point in body frame:\n"
         + "            x: {value: 0, unit: m}\n"
         + "            y: {value: 0, unit: m}\n"
         + "            z: {value: 3.45, unit: m}\n"
         + centre_of_inertia("dtmb", 0, 0, 0)
         + "        mass: {value: 8569., unit: tonne} # Caution: 'ton' is the british ton which is 907.185 kg\n"
         + "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
         + "            frame: dtmb\n"
         + "            row 1: [8.569e6,0,0,0,0,0]\n"
         + "            row 2: [0,8.569e6,0,0,0,0]\n"
         + "            row 3: [0,0,8.569e6,0,0,0]\n"
         + "            row 4: [0,0,0,4.271e8,0,0]\n"
         + "            row 5: [0,0,0,0,1.080e10,0]\n"
         + "            row 6: [0,0,0,0,0,1.080e10]\n"
         + "        added mass matrix at the center of gravity and projected in the body frame:\n"
         + "            frame: dtmb\n"
         + "            # T min Aqua+ (3.5s)\n"
         + "            row 1: [ 8.979E+04, 0.000E+00, -1.981E+05,  0.000E+00, -1.962E+07,  0.000E+00]\n"
         + "            row 2: [ 0.000E+00, 2.172E+06,  0.000E+00,  1.135E+06,  0.000E+00,  2.523E+07]\n"
         + "            row 3: [-1.981E+05, 0.000E+00,  9.680E+06,  0.000E+00,  6.716E+07,  0.000E+00]\n"
         + "            row 4: [ 0.000E+00, 1.135E+06,  0.000E+00,  3.865E+07,  0.000E+00, -2.228E+08]\n"
         + "            row 5: [-1.962E+07, 0.000E+00,  6.716E+07,  0.000E+00,  9.117E+09,  0.000E+00]\n"
         + "            row 6: [ 0.000E+00, 2.523E+07,  0.000E+00, -2.228E+08,  0.000E+00,  3.338E+09]\n"
         + initial_position_of_body_frame_deg(0, 0, -1.41, -0.06, 0, 0.81)
         + initial_velocity("dtmb", 8.901, 0.020, 0, -0.007, 0, -0.0006)
         + "    external forces:\n"
         + "      - model: linear damping\n"
         + "        damping matrix at the center of gravity projected in the body frame:\n"
         + "            frame: dtmb\n"
         + "            row 1: [ 0, 0,      0,      0,       0, 0]\n"
         + "            row 2: [ 0, 0,      0,      0,       0, 0]\n"
         + "            row 3: [ 0, 0, 8.86e6,      0,       0, 0]\n"
         + "            row 4: [ 0, 0,      0, 3.18e7,       0, 0]\n"
         + "            row 5: [ 0, 0,      0,      0, 1.01e10, 0]\n"
         + "            row 6: [ 0, 0,      0,      0,       0, 0]\n"
         + "      - model: quadratic damping\n"
         + "        damping matrix at the center of gravity projected in the body frame:\n"
         + "            frame: dtmb\n"
         + "            row 1: [ 0, 0, 0,      0, 0, 0]\n"
         + "            row 2: [ 0, 0, 0,      0, 0, 0]\n"
         + "            row 3: [ 0, 0, 0,      0, 0, 0]\n"
         + "            row 4: [ 0, 0, 0, 1.16e8, 0, 0]\n"
         + "            row 5: [ 0, 0, 0,      0, 0, 0]\n"
         + "            row 6: [ 0, 0, 0,      0, 0, 0]\n"
         + "      - model: resistance curve\n"
         + "        speed: {unit: m/s, values: [-1.858, 0.000, 1.858, 2.212, 2.616, 2.785, 2.979, 3.353, 3.732, 4.100, 4.474, 4.843, 5.212, 5.600, 5.964, 6.338, 6.711, 7.090, 7.479, 7.842, 8.211, 8.580, 8.938, 9.332, 9.696, 10.069, 10.448, 10.817, 11.195, 11.554, 11.933, 12.311, 12.685, 13.059, 13.432, 13.811, 14.185, 14.369, 14.544, 14.743, 14.927, 15.301, 15.665, 16.053, 16.427, 16.791]}\n"
         + "        resistance: {unit: N, values: [-1.890E+04, 0.000E+00, 1.890E+04, 2.557E+04, 3.185E+04, 3.683E+04, 3.853E+04, 4.778E+04, 5.996E+04, 6.849E+04, 8.565E+04, 9.835E+04, 1.145E+05, 1.357E+05, 1.613E+05, 1.789E+05, 1.991E+05, 2.253E+05, 2.465E+05, 2.720E+05, 3.029E+05, 3.327E+05, 3.708E+05, 4.061E+05, 4.415E+05, 4.762E+05, 5.392E+05, 6.029E+05, 6.597E+05, 7.331E+05, 7.862E+05, 8.579E+05, 9.303E+05, 1.011E+06, 1.117E+06, 1.254E+06, 1.414E+06, 1.526E+06, 1.611E+06, 1.723E+06, 1.846E+06, 2.056E+06, 2.283E+06, 2.530E+06, 2.708E+06, 2.992E+06]}\n"
         + "      - name: PSPropRudd\n"
         + "        model: propeller+rudder\n"
         + "        position of propeller frame:\n"
         + "            frame: dtmb\n"
         + "            x: {value: -60.695, unit: m}\n"
         + "            y: {value: -4.650, unit: m}\n"
         + "            z: {value: 6.574, unit: m}\n"
         + "            phi: {value: 0, unit: rad}\n"
         + "            theta: {value: 2.95, unit: deg}\n"
         + "            psi: {value: 0, unit: deg}\n"
         + "        wake coefficient w: 0.15\n"
         + "        relative rotative efficiency etaR: 1\n"
         + "        thrust deduction factor t: 0.12\n"
         + "        rotation: clockwise\n"
         + "        number of blades: 5\n"
         + "        blade area ratio AE/A0: 0.58\n"
         + "        diameter: {value: 6.15, unit: m}\n"
         + "        rudder area: {value: 15.4, unit: m^2}\n"
         + "        rudder height: {value: 4.4, unit: m}\n"
         + "        effective aspect ratio factor: 1.7\n"
         + "        lift tuning coefficient: 1.\n"
         + "        drag tuning coefficient: 1.\n"
         + "        position of rudder in body frame:\n"
         + "            x: {value: -66.27, unit: m}\n"
         + "            y: {value: -4.75, unit: m}\n"
         + "            z: {value: 4.60, unit: m}\n"
         + "      - name: SBPropRudd\n"
         + "        model: propeller+rudder\n"
         + "        position of propeller frame:\n"
         + "            frame: dtmb\n"
         + "            x: {value: -60.695, unit: m}\n"
         + "            y: {value: 4.650, unit: m}\n"
         + "            z: {value: 6.574, unit: m}\n"
         + "            phi: {value: 0, unit: rad}\n"
         + "            theta: {value: 2.95, unit: deg}\n"
         + "            psi: {value: 0, unit: deg}\n"
         + "        wake coefficient w: 0.15\n"
         + "        relative rotative efficiency etaR: 1\n"
         + "        thrust deduction factor t: 0.12\n"
         + "        rotation: anti-clockwise\n"
         + "        number of blades: 5\n"
         + "        blade area ratio AE/A0: 0.58\n"
         + "        diameter: {value: 6.15, unit: m}\n"
         + "        rudder area: {value: 15.4, unit: m^2}\n"
         + "        rudder height: {value: 4.4, unit: m}\n"
         + "        effective aspect ratio factor: 1.7\n"
         + "        lift tuning coefficient: 1.\n"
         + "        drag tuning coefficient: 1.\n"
         + "        position of rudder in body frame:\n"
         + "            x: {value: -66.27, unit: m}\n"
         + "            y: {value: 4.75, unit: m}\n"
         + "            z: {value: 4.60, unit: m}\n"
         + "      - model: maneuvering\n"
         + "        name: man\n"
         + "        reference frame:\n"
         + "            frame: dtmb\n"
         + "            x: {value: 0.731, unit: m}\n"
         + "            y: {value: 0, unit: m}\n"
         + "            z: {value: 1.397, unit: m}\n"
         + "            phi: {value: 0, unit: deg}\n"
         + "            theta: {value: 0, unit: deg}\n"
         + "            psi: {value: 0, unit: deg}\n"
         + "        X: X_\n"
         + "        Y: Y_\n"
         + "        Z: 0.\n"
         + "        K: 0.\n"
         + "        M: 0.\n"
         + "        N: N_\n"
         + "        X_: (Xu0*u(t) + 1/2.*Xuu0*(u(t))^2 + 1/6.*Xuuu0*(u(t))^3) + (Xv*abs(v(t)) + 1/2.*Xvv*(v(t))^2) + Xuv*u(t)*v(t) + Xr*r(t)\n"
         + "        Y_: (Yv*v(t) + 1/2.*Yvv*v(t)*abs(v(t)) + 1/6.*Yvvv*(v(t))^3) + Yuv*u(t)*v(t) + Yr*r(t)\n"
         + "        N_: (Nv*v(t) + 1/2.*Nvv*v(t)*abs(v(t))) + Nuv*u(t)*v(t) + Nr*r(t)\n"
         + "        Xu0:    0.\n"
         + "        Xuu0:   0.\n"
         + "        Xuuu0:  0.\n"
         + "        Xv:    -5.528E+03\n"
         + "        Xvv:   -1.301E+05\n"
         + "        Xuv:   -9.539E+02\n"
         + "        Xr:    -1.462E+06\n"
         + "        Xrp:    1.246E+07\n"
         + "        Yv:     2.925E+05\n"
         + "        Yvv:   -4.548E+05\n"
         + "        Yvvv:  -2.292E+05\n"
         + "        Yuv:   -1.504E+05\n"
         + "        Yr:    -8.542E+07\n"
         + "        Yvp:   -1.056E+07\n"
         + "        Yrp:    1.958E+08\n"
         + "        Nv:     3.979E+07\n"
         + "        Nvv:   -2.511E+07\n"
         + "        Nuv:   -1.392E+07\n"
         + "        Nr:    -9.677E+09\n"
         + "        Nvp:   -8.913E+08\n"
         + "        Nrp:    2.609E+10\n"
         + "    blocked dof:\n"
         + "       from YAML:\n"
         + "         - state: w\n"
         + "           t: [0, 1000]\n"
         + "           value: [0, 0]\n"
         + "           interpolation: piecewise constant\n"
         + "         - state: p\n"
         + "           t: [0, 1000]\n"
         + "           value: [0, 0]\n"
         + "           interpolation: piecewise constant\n"
         + "         - state: q\n"
         + "           t: [0, 1000]\n"
         + "           value: [0, 0]\n"
         + "           interpolation: piecewise constant\n"
         + "environment models:\n"
         + "  - model: no waves\n"
         + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
         + "commands:\n"
         + "    - name: PSPropRudd\n"
         + "      t: [0]\n"
         + "      rpm: {unit: rpm, values: [101.]}\n"
         + "      P/D: {unit: 1, values: [0.79]}\n"
         + "    - name: SBPropRudd\n"
         + "      t: [0]\n"
         + "      rpm: {unit: rpm, values: [101.]}\n"
         + "      P/D: {unit: 1, values: [0.79]}\n";
}

std::string test_data::heading_keeping_controllers()
{
    return "setpoints:\n"
           "    - t: [0]\n"
           "      psi_co: {unit: deg, values: [60]}\n"
           "controllers:\n"
           "    - type: PID\n"
           "      dt: 1\n"
           "      name: foo\n"
           "      state weights:\n"
           "        psi: 1\n"
           "      setpoint: psi_co\n"
           "      command: PSPropRudd(beta)\n"
           "      gains:\n"
           "        Kp: -1\n"
           "        Ki: 0\n"
           "        Kd: -1\n"
           "    - type: PID\n"
           "      dt: 1\n"
           "      name: bar\n"
           "      state weights:\n"
           "        psi: 1\n"
           "      setpoint: psi_co\n"
           "      command: SBPropRudd(beta)\n"
           "      gains:\n"
           "        Kp: -1\n"
           "        Ki: 0\n"
           "        Kd: -1\n"
           "    \n";
}

std::string test_data::radiation_damping()
{
    std::stringstream ss;
    ss << "model: radiation damping\n"
       << "hdb: test_ship.hdb\n"
       << "type of quadrature for cos transform: simpson\n"
       << "type of quadrature for convolution: clenshaw-curtis\n"
       << "nb of points for retardation function discretization: 50\n"
       << "omega min: {value: 0, unit: rad/s}\n"
       << "omega max: {value: 30, unit: rad/s}\n"
       << "tau min: {value: 0.2094395, unit: s}\n"
       << "tau max: {value: 10, unit: s}\n"
       << "output Br and K: true\n"
       << "forward speed correction: true\n"
       << "calculation point in body frame:\n"
       << "    x: {value: 0.696, unit: m}\n"
       << "    y: {value: 0, unit: m}\n"
       << "    z: {value: 1.418, unit: m}\n";
    return ss.str();
}

std::string test_data::diffraction()
{
    return "model: diffraction\n"
           "hdb: test_ship.hdb\n"
           "calculation point in body frame:\n"
           "    x: {value: 0.696, unit: m}\n"
           "    y: {value: 0, unit: m}\n"
           "    z: {value: 1.418, unit: m}\n"
           "mirror for 180 to 360: true\n"
           "use encounter period: true\n";
}

std::string test_data::diffraction_precalr()
{
    return "model: diffraction\n"
           "raodb: test_ship.raodb.ini\n"
           "mirror for 180 to 360: true\n"
           "use encounter period: true\n";
}

std::string test_data::test_ship_radiation_damping()
{
    return rotation_convention()
         + "\n"
         + environmental_constants()
         + "environment models:\n"
         + "  - model: no waves\n"
         + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
         + "    \n"
         + "# Fixed frame: NED\n"
         + "bodies: # All bodies have NED as parent frame\n"
         + "  - name: TestShip\n"
         + "    mesh: test_ship.stl\n"
         + position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
         + initial_position_of_body_frame(0, 0, 1, 0, 0, 0)
         + initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
         + dynamics()
         + "    external forces:\n"
         + "      - model: resistance curve\n"
         + "        speed: {unit: m/s, values: [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8]}\n"
         + "        resistance: {unit: N, values: [0.000E+00,2.102E+02,7.728E+02,1.647E+03,2.803E+03,4.230E+03,5.999E+03,8.498E+03,1.273E+04,2.084E+04,2.789E+04,4.238E+04,7.737E+04,1.449E+05,2.439E+05,3.590E+05,4.741E+05]}\n"
         + "      - model: radiation damping\n"
         + "        hdb: test_ship.hdb\n"
         + "        type of quadrature for cos transform: simpson\n"
         + "        type of quadrature for convolution: simpson\n"
         + "        nb of points for retardation function discretization: 50\n"
         + "        omega min: {value: 0, unit: rad/s}\n"
         + "        omega max: {value: 30, unit: rad/s}\n"
         + "        tau min: {value: 0.2094395, unit: s}\n"
         + "        tau max: {value: 10, unit: s}\n"
         + "        output Br and K: false\n"
         + "        calculation point in body frame:\n"
         + "            x: {value: 0.696, unit: m}\n"
         + "            y: {value: 0, unit: m}\n"
         + "            z: {value: 1.418, unit: m}\n"
         + "      - name: propeller\n"
         + "        model: wageningen B-series\n"
         + "        position of propeller frame:\n"
         + "            frame: TestShip\n"
         + "            x: {value: -8.4, unit: m}\n"
         + "            y: {value: 0, unit: m}\n"
         + "            z: {value: 0.432, unit: m}\n"
         + "            phi: {value: 0, unit: rad}\n"
         + "            theta: {value: 0, unit: deg}\n"
         + "            psi: {value: 0, unit: deg}\n"
         + "        wake coefficient w: 0\n"
         + "        relative rotative efficiency etaR: 1\n"
         + "        thrust deduction factor t: 0\n"
         + "        rotation: clockwise\n"
         + "        number of blades: 4\n"
         + "        blade area ratio AE/A0: 0.55\n"
         + "        diameter: {value: 1.925, unit: m}\n"
         + "    \n";
}

std::string test_data::simple_track_keeping()
{
    return "name: controller\n"
           "model: simple heading controller\n"
           "ksi: 0.9\n"
           "Tp: {value: 4, unit: s}\n";
}

std::string test_data::maneuvering_commands()
{
    return "commands: [a,b,c]\n"
           "X: 0.5*rho*Vs^2*L^2*X_\n"
           "Y: 0.5*rho*Vs^2*L^2*Y_\n"
           "Z: 0\n"
           "K: 0\n"
           "M: 0\n"
           "N: 0.5*rho*Vs^2*L^3*N_\n"
           "Vs: sqrt(u(t)^2+v(t)^2)\n"
           "L: 21.569\n"
           "X_: Xu*u_ + Xuu*u_^2 + Xuuu*u_^3 + Xvv*v_^2 + Xrr*r_^2 + Xvr*abs(v_)*abs(r_)\n"
           "Y_: Yv*v_ + Yvv*v_*abs(v_) + Yvvv*v_^3 + Yvrr*v_*r_^2 + Yr*r_ + Yrr*r_*abs(r_) + Yrrr*r_^3 + Yrvv*r_*v_^2\n"
           "N_: Nv*v_ + Nvv*v_*abs(v_) + Nvvv*v_^3 + Nvrr*v_*r_^2 + Nr*r_ + Nrr*r_*abs(r_) + Nrrr*r_^3 + Nrvv*r_*v_^2\n"
           "u_: u(t)/Vs\n"
           "v_: v(t)/Vs\n"
           "r_: r(t)/Vs*L\n"
           "Xu: 0\n"
           "Xuu: 0\n"
           "Xuuu: 0\n"
           "Xvv: -0.041\n"
           "Xrr: -0.01\n"
           "Xvr: -0.015\n"
           "Yv: -0.13\n"
           "Yvv: -0.18\n"
           "Yvvv: 0\n"
           "Yvrr: 0\n"
           "Yr: 0.015\n"
           "Yrr: 0.021\n"
           "Yrrr: 0\n"
           "Yrvv: 0\n"
           "Nv: -0.37\n"
           "Nvv: -0.12\n"
           "Nvvv: 0\n"
           "Nvrr: 0\n"
           "Nr: -0.1\n"
           "Nrr: 0.005\n"
           "Nrrr: 0\n"
           "Nrvv: 0\n"
           "\n";
}

std::string test_data::maneuvering_with_same_frame_of_reference()
{
    std::stringstream ss;
    ss << "reference frame:\n"
       << "    frame: TestShip\n"
       << "    x: {value: 0, unit: m}\n"
       << "    y: {value: 0, unit: m}\n"
       << "    z: {value: 0, unit: m}\n"
       << "    phi: {value: 0, unit: rad}\n"
       << "    theta: {value: 0, unit: deg}\n"
       << "    psi: {value: 0, unit: deg}\n"
       << "name: test\n"
       << maneuvering_commands();
    return ss.str();
}

std::string test_data::maneuvering()
{
    std::stringstream ss;
    ss << "reference frame:\n"
       << "    frame: TestShip\n"
       << "    x: {value: 0.696, unit: m}\n"
       << "    y: {value: 0, unit: m}\n"
       << "    z: {value: 1.418, unit: m}\n"
       << "    phi: {value: 0.7, unit: rad}\n"
       << "    theta: {value: -166, unit: deg}\n"
       << "    psi: {value: 125, unit: deg}\n"
       << "name: test\n"
       << maneuvering_commands();
    return ss.str();
}

std::string test_data::bug_2641()
{
    return rotation_convention()
         + "\n"
         + environmental_constants()
         + "environment models:\n"
         + "  - model: no waves\n"
         + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
         + "\n"
         + "# Fixed frame: NED\n"
         + "bodies: # All bodies have NED as parent frame\n"
         + "  - name: TestShip\n"
         + "    mesh: test_ship.stl\n"
         + position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
         + initial_position_of_body_frame_deg(0, 0, -0.099, 0, -0.334, 0)
         + initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
         + dynamics()
         + "    external forces:\n"
         + "      - model: gravity\n"
         + "      - model: non-linear hydrostatic (exact)\n"
         + linear_damping()
         + quadratic_damping()
         + "      - model: resistance curve\n"
         + "        speed: {unit: m/s, values: [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8]}\n"
         + "        resistance: {unit: N, values: [0.000E+00,2.102E+02,7.728E+02,1.647E+03,2.803E+03,4.230E+03,5.999E+03,8.498E+03,1.273E+04,2.084E+04,2.789E+04,4.238E+04,7.737E+04,1.449E+05,2.439E+05,3.590E+05,4.741E+05]}\n"
         + "      - name: propeller\n"
         + "        model: wageningen B-series\n"
         + "        position of propeller frame:\n"
         + "            frame: TestShip\n"
         + "            x: {value: -8.4, unit: m}\n"
         + "            y: {value: 0, unit: m}\n"
         + "            z: {value: 2.5, unit: m}\n"
         + "            phi: {value: 0, unit: rad}\n"
         + "            theta: {value: 3, unit: deg}\n"
         + "            psi: {value: 0, unit: deg}\n"
         + "        wake coefficient w: 0\n"
         + "        relative rotative efficiency etaR: 1\n"
         + "        thrust deduction factor t: 0\n"
         + "        rotation: clockwise\n"
         + "        number of blades: 4\n"
         + "        blade area ratio AE/A0: 0.55\n"
         + "        diameter: {value: 1.925, unit: m}\n"
         + "\n"
         + "output:\n"
         + "   - format: csv\n"
         + "     filename: acceleration.csv\n"
         + "     data: [t, x(TestShip), y(TestShip), z(TestShip), u(TestShip), v(TestShip), w(TestShip), p(TestShip), q(TestShip), r(TestShip), phi(TestShip), theta(TestShip), psi(TestShip)]\n";
}

std::string test_data::simple_station_keeping()
{
    return "name: controller\n"
           "model: simple station-keeping controller\n"
           "ksi_x: 0.9\n"
           "T_x: {value: 2, unit: s}\n"
           "ksi_y: 0.85\n"
           "T_y: {value: 3, unit: s}\n"
           "ksi_psi: 0.8\n"
           "T_psi: {value: 4, unit: s}\n";
}

std::string test_data::rudder()
{
    return "name: port side propeller\n"
           "model: wageningen B-series\n"
           "position of propeller frame:\n"
           "    frame: mesh(body 1)\n"
           "    x: {value: -4, unit: m}\n"
           "    y: {value: -2, unit: m}\n"
           "    z: {value: 2, unit: m}\n"
           "    phi: {value: 0, unit: rad}\n"
           "    theta: {value: -10, unit: deg}\n"
           "    psi: {value: -1, unit: deg}\n"
           "wake coefficient w: 0.9\n"
           "relative rotative efficiency etaR: 1\n"
           "thrust deduction factor t: 0.7\n"
           "rotation: clockwise\n"
           "number of blades: 3\n"
           "blade area ratio AE/A0: 0.5\n"
           "diameter: {value: 2, unit: m}\n"
           "rudder area: {value: 2.2, unit: m^2}\n"
           "rudder height: {value: 2, unit: m^2}\n"
           "effective aspect ratio factor: 1.7\n"
           "lift tuning coefficient: 2.1\n"
           "drag tuning coefficient: 1\n"
           "position of rudder in body frame:\n"
           "    x: {value: -5.1, unit: m}\n"
           "    y: {value: -2, unit: m}\n"
           "    z: {value: 2, unit: m}\n";
}

std::string test_data::bug_in_exact_hydrostatic()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << "environmental constants:\n"
       << "    g: {value: 9.81, unit: m/s^2}\n"
       << "    rho: {value: 1026, unit: kg/m^3}\n"
       << "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: cube\n"
       << "    mesh: cube.stl\n"
       << position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 0.4746588693957115, 0, 0, 0)
       << initial_velocity("cube", 0, 0, 0, 0, 0, 0)
       << "    dynamics:\n"
       << hydrodynamic_calculation_point()
       << centre_of_inertia("cube", 0, 0, 0.5)
       << one_ton_rigid_inertia_matrix()
       << no_added_mass()
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: non-linear hydrostatic (exact)\n";
    return ss.str();
}
std::string test_data::bug_2714_heading_keeping()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame_deg(0, 0, -0.099, 10, -0.334, 0)
       << initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: non-linear hydrostatic (fast)\n"
       << linear_damping()
       << quadratic_damping()
       << "      - name: controller\n"
       << "        model: simple heading controller\n"
       << "        ksi: 0.9\n"
       << "        Tp: {value: 4, unit: s}\n"
       << "\n"
       << "output:\n"
       << "   - format: csv\n"
       << "     filename: extinctionRoulis.csv\n"
       << "     data: [t, x(TestShip), y(TestShip), z(TestShip), u(TestShip), v(TestShip), w(TestShip), p(TestShip), q(TestShip), r(TestShip), phi(TestShip), theta(TestShip), psi(TestShip)]\n";
    return ss.str();
}

std::string test_data::bug_2714_station_keeping()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame_deg(0, 0, -0.099, 10, -0.334, 0)
       << initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: non-linear hydrostatic (fast)\n"
       << linear_damping()
       << quadratic_damping()
       << "      - name: controller\n"
       << "        model: simple station-keeping controller\n"
       << "        ksi_x: 0.9\n"
       << "        T_x: {value: 4, unit: s}\n"
       << "        ksi_y: 0.9\n"
       << "        T_y: {value: 4, unit: s}\n"
       << "        ksi_psi: 0.9\n"
       << "        T_psi: {value: 4, unit: s}\n"
       << "\n"
       << "output:\n"
       << "   - format: csv\n"
       << "     filename: extinctionRoulis.csv\n"
       << "     data: [t, x(TestShip), y(TestShip), z(TestShip), u(TestShip), v(TestShip), w(TestShip), p(TestShip), q(TestShip), r(TestShip), phi(TestShip), theta(TestShip), psi(TestShip)]\n";
    return ss.str();
}

std::string test_data::bug_2732()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame_deg(0, 0, -0.099, 10, -0.334, 0)
       << initial_velocity("TestShip", 10, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: non-linear hydrostatic (fast)\n"
       << linear_damping()
       << quadratic_damping()
       << "      - model: resistance curve\n"
       << "        speed: {unit: m/s, values: [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8]}\n"
       << "        resistance: {unit: N, values: [0.000E+00,2.102E+02,7.728E+02,1.647E+03,2.803E+03,4.230E+03,5.999E+03,8.498E+03,1.273E+04,2.084E+04,2.789E+04,4.238E+04,7.737E+04,1.449E+05,2.439E+05,3.590E+05,4.741E+05]}\n"
       << "      - model: maneuvering\n"
       << "        name: maneuvering\n"
       << "        reference frame:\n"
       << "            frame: TestShip\n"
       << "            x: {value: 0.696, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 1.418, unit: m}\n"
       << "            phi: {value: 0, unit: deg}\n"
       << "            theta: {value: 0, unit: deg}\n"
       << "            psi: {value: 0, unit: deg}\n"
       << "        X: 0.5*rho*Vs^2*L^2*X_\n"
       << "        Y: 0.5*rho*Vs^2*L^2*Y_\n"
       << "        Z: 0\n"
       << "        K: 0\n"
       << "        M: 0\n"
       << "        N: 0.5*rho*Vs^2*L^3*N_\n"
       << "        Vs: sqrt(u(t)^2+v(t)^2)\n"
       << "        L: 21.569\n"
       << "        X_: Xu*u_ + Xuu*u_^2 + Xuuu*u_^3 + Xvv*v_^2 + Xrr*r_^2 + Xvr*abs(v_)*abs(r_)\n"
       << "        Y_: Yv*v_ + Yvv*v_*abs(v_) + Yvvv*v_^3 + Yvrr*v_*r_^2 + Yr*r_ + Yrr*r_*abs(r_) + Yrrr*r_^3 + Yrvv*r_*v_^2\n"
       << "        N_: Nv*v_ + Nvv*v_*abs(v_) + Nvvv*v_^3 + Nvrr*v_*r_^2 + Nr*r_ + Nrr*r_*abs(r_) + Nrrr*r_^3 + Nrvv*r_*v_^2\n"
       << "        u_: u(t)/Vs\n"
       << "        v_: v(t)/Vs\n"
       << "        r_: r(t)/Vs*L\n"
       << "        Xu: 0\n"
       << "        Xuu: 0\n"
       << "        Xuuu: 0\n"
       << "        Xvv: -0.041\n"
       << "        Xrr: -0.01\n"
       << "        Xvr: -0.015\n"
       << "        Yv: -0.13\n"
       << "        Yvv: -0.18\n"
       << "        Yvvv: 0\n"
       << "        Yvrr: 0\n"
       << "        Yr: 0.015\n"
       << "        Yrr: 0.021\n"
       << "        Yrrr: 0\n"
       << "        Yrvv: 0\n"
       << "        Nv: -0.37\n"
       << "        Nvv: -0.12\n"
       << "        Nvvv: 0\n"
       << "        Nvrr: 0\n"
       << "        Nr: -0.1\n"
       << "        Nrr: 0.005\n"
       << "        Nrrr: 0\n"
       << "        Nrvv: 0\n"
       << "      - name: Prop. & rudder\n"
       << "        model: propeller+rudder\n"
       << "        position of propeller frame:\n"
       << "            frame: TestShip\n"
       << "            x: {value: -8.4, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 2.5, unit: m}\n"
       << "            phi: {value: 0, unit: rad}\n"
       << "            theta: {value: 3, unit: deg}\n"
       << "            psi: {value: 0, unit: deg}\n"
       << "        wake coefficient w: 0\n"
       << "        relative rotative efficiency etaR: 1\n"
       << "        thrust deduction factor t: 0\n"
       << "        rotation: clockwise\n"
       << "        number of blades: 4\n"
       << "        blade area ratio AE/A0: 0.55\n"
       << "        diameter: {value: 1.925, unit: m}\n"
       << "        rudder area: {value: 2.2, unit: m^2}\n"
       << "        rudder height: {value: 2, unit: m^2}\n"
       << "        effective aspect ratio factor: 1.7\n"
       << "        lift tuning coefficient: 2.1\n"
       << "        drag tuning coefficient: 1\n"
       << "        position of rudder in body frame:\n"
       << "            x: {value: -9.5, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 2.5, unit: m}\n"
       << "\n"
       << "output:\n"
       << "   - format: csv\n"
       << "     filename: propRudd.csv\n"
       << "     data: [t, 'Fx(Prop. & rudder,TestShip,TestShip)', 'Fx(maneuvering,TestShip,NED)']\n";
    return ss.str();
}

std::string test_data::L_config()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << "environmental constants:\n"
       << "    g: {value: 9.81, unit: m/s^2}\n"
       << "    rho: {value: 1026, unit: kg/m^3}\n"
       << "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       << "environment models: []\n"
       << "\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: L\n"
       << "    mesh: L.stl\n"
       << position_relative_to_mesh(0.5, 0.8333333333, -0.8333333333, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 0, 0, 0, 0)
       << initial_velocity("cube", 0, 0, 0, 0, 0, 0)
       << "    dynamics:\n"
       << hydrodynamic_calculation_point()
       << centre_of_inertia("L", 0, 0, 0.5)
       << one_ton_rigid_inertia_matrix()
       << no_added_mass()
       << "    external forces:\n"
       << "      - model: gravity\n";
    return ss.str();
}

std::string test_data::GM_cube()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << "environmental constants:\n"
       << "    g: {value: 9.81, unit: m/s^2}\n"
       << "    rho: {value: 1000, unit: kg/m^3}\n"
       << "    nu: {value: 1.18e-6, unit: m^2/s}\n"
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: cube\n"
       << "    mesh: cube.stl\n"
       << position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
       << initial_position_of_body_frame_deg(0, 0, 0, 0, 0, 0)
       << initial_velocity("cube", 0, 0, 0, 0, 0, 0)
       << "    dynamics:\n"
       << hydrodynamic_calculation_point()
       << centre_of_inertia("cube", 0, 0, 0)
       << one_ton_rigid_inertia_matrix()
       << no_added_mass()
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: GM\n"
       << "        name of hydrostatic force model: non-linear hydrostatic (exact)\n"
       << "        roll step: {value: 0.1, unit: degree}\n";
    return ss.str();
}

std::string test_data::bug_2838()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame_deg(0, 0, -0.099, 10, -0.334, 0)
       << initial_velocity_kt("TestShip", 10, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: non-linear hydrostatic (fast)\n"
       << linear_damping()
       << quadratic_damping()
       << "      - model: resistance curve\n"
       << "        speed: {unit: m/s, values: [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8]}\n"
       << "        resistance: {unit: N, values: [0.000E+00,2.102E+02,7.728E+02,1.647E+03,2.803E+03,4.230E+03,5.999E+03,8.498E+03,1.273E+04,2.084E+04,2.789E+04,4.238E+04,7.737E+04,1.449E+05,2.439E+05,3.590E+05,4.741E+05]}\n"
       << "      - model: maneuvering\n"
       << "        name: maneuvering\n"
       << "        reference frame:\n"
       << "            frame: TestShip\n"
       << "            x: {value: 0.696, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 1.418, unit: m}\n"
       << "            phi: {value: 0, unit: deg}\n"
       << "            theta: {value: 0, unit: deg}\n"
       << "            psi: {value: 0, unit: deg}\n"
       << "        X: 0.5*rho*Vs^2*L^2*X_\n"
       << "        Y: 0.5*rho*Vs^2*L^2*Y_\n"
       << "        Z: 0\n"
       << "        K: 0\n"
       << "        M: 0\n"
       << "        N: 0.5*rho*Vs^2*L^3*N_\n"
       << "        Vs: sqrt(u(t)^2+v(t)^2)\n"
       << "        L: 21.569\n"
       << "        X_: Xu*u_ + Xuu*u_^2 + Xuuu*u_^3 + Xvv*v_^2 + Xrr*r_^2 + Xvr*abs(v_)*abs(r_)\n"
       << "        Y_: Yv*v_ + Yvv*v_*abs(v_) + Yvvv*v_^3 + Yvrr*v_*r_^2 + Yr*r_ + Yrr*r_*abs(r_) + Yrrr*r_^3 + Yrvv*r_*v_^2\n"
       << "        N_: Nv*v_ + Nvv*v_*abs(v_) + Nvvv*v_^3 + Nvrr*v_*r_^2 + Nr*r_ + Nrr*r_*abs(r_) + Nrrr*r_^3 + Nrvv*r_*v_^2\n"
       << "        u_: u(t)/Vs\n"
       << "        v_: v(t)/Vs\n"
       << "        r_: r(t)/Vs*L\n"
       << "        Xu: 0\n"
       << "        Xuu: 0\n"
       << "        Xuuu: 0\n"
       << "        Xvv: -0.041\n"
       << "        Xrr: -0.01\n"
       << "        Xvr: -0.015\n"
       << "        Yv: -0.13\n"
       << "        Yvv: -0.18\n"
       << "        Yvvv: 0\n"
       << "        Yvrr: 0\n"
       << "        Yr: 0.015\n"
       << "        Yrr: 0.021\n"
       << "        Yrrr: 0\n"
       << "        Yrvv: 0\n"
       << "        Nv: -0.37\n"
       << "        Nvv: -0.12\n"
       << "        Nvvv: 0\n"
       << "        Nvrr: 0\n"
       << "        Nr: -0.1\n"
       << "        Nrr: 0.005\n"
       << "        Nrrr: 0\n"
       << "        Nrvv: 0\n"
       << "      - name: PropRudd\n"
       << "        model: propeller+rudder\n"
       << "        position of propeller frame:\n"
       << "            frame: TestShip\n"
       << "            x: {value: -8.4, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 2.5, unit: m}\n"
       << "            phi: {value: 0, unit: rad}\n"
       << "            theta: {value: 3, unit: deg}\n"
       << "            psi: {value: 0, unit: deg}\n"
       << "        wake coefficient w: 0\n"
       << "        relative rotative efficiency etaR: 1\n"
       << "        thrust deduction factor t: 0\n"
       << "        rotation: clockwise\n"
       << "        number of blades: 4\n"
       << "        blade area ratio AE/A0: 0.55\n"
       << "        diameter: {value: 1.925, unit: m}\n"
       << "        rudder area: {value: 2.2, unit: m^2}\n"
       << "        rudder height: {value: 2, unit: m^2}\n"
       << "        effective aspect ratio factor: 1.7\n"
       << "        lift tuning coefficient: 2.1\n"
       << "        drag tuning coefficient: 1\n"
       << "        position of rudder in body frame:\n"
       << "            x: {value: -9.5, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 2.5, unit: m}\n"
       << "\n"
       << "output:\n"
       << "   - format: map\n"
       << "     filename: efforts.h5\n"
       << "     data: [t,  'Mz(PropRudd,TestShip,TestShip)']\n";
    return ss.str();
}

std::string test_data::bug_2845()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame_deg(0, 0, -0.099, 10, -0.334, 0)
       << initial_velocity_kt("TestShip", 10, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - name: PropRudd\n"
       << "        model: propeller+rudder\n"
       << "        position of propeller frame:\n"
       << "            frame: TestShip\n"
       << "            x: {value: 0.258, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 2.5, unit: m}\n"
       << "            phi: {value: 0, unit: rad}\n"
       << "            theta: {value: 0, unit: deg}\n"
       << "            psi: {value: 0, unit: deg}\n"
       << "        wake coefficient w: 0\n"
       << "        relative rotative efficiency etaR: 0\n"
       << "        thrust deduction factor t: 1\n"
       << "        rotation: clockwise\n"
       << "        number of blades: 4\n"
       << "        blade area ratio AE/A0: 0.55\n"
       << "        diameter: {value: 1.925, unit: m}\n"
       << "        rudder area: {value: 2.2, unit: m^2}\n"
       << "        rudder height: {value: 2, unit: m^2}\n"
       << "        effective aspect ratio factor: 1.7\n"
       << "        lift tuning coefficient: 2.1\n"
       << "        drag tuning coefficient: 1\n"
       << "        position of rudder in body frame:\n"
       << "            x: {value: 0.258, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 0.432, unit: m}\n"
       << "      - model: maneuvering\n"
       << "        name: Fman\n"
       << "        reference frame:\n"
       << "            frame: TestShip\n"
       << "            x: {value: 0.696, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 1.418, unit: m}\n"
       << "            phi: {value: 0, unit: deg}\n"
       << "            theta: {value: 0, unit: deg}\n"
       << "            psi: {value: 0, unit: deg}\n"
       << "        X: 0\n"
       << "        Y: 0\n"
       << "        Z: 0\n"
       << "        K: 0\n"
       << "        M: 0\n"
       << "        N: 0\n"
       << "\n"
       << "output:\n"
       << "   - format: map\n"
       << "     data: ['Fx(Fman,TestShip,TestShip)',\n"
       << "            'Fx(PropRudd,TestShip,PropRudd)',\n"
       << "            'Fy(PropRudd,TestShip,PropRudd)',\n"
       << "            'Fz(PropRudd,TestShip,PropRudd)',\n"
       << "            'Mx(PropRudd,TestShip,PropRudd)',\n"
       << "            'My(PropRudd,TestShip,PropRudd)',\n"
       << "            'Mz(PropRudd,TestShip,PropRudd)',\n"
       << "            'Mx(PropRudd,TestShip,TestShip)',\n"
       << "            'My(PropRudd,TestShip,TestShip)',\n"
       << "            'Mz(PropRudd,TestShip,TestShip)']\n";
    return ss.str();
}

std::string test_data::maneuvering_with_commands()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame_deg(0, 0, -0.099, 10, -0.334, 0)
       << initial_velocity_kt("TestShip", 10, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: maneuvering\n"
       << "        name: F1\n"
       << "        reference frame:\n"
       << "            frame: TestShip\n"
       << "            x: {value: 0, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 1.418, unit: m}\n"
       << "            phi: {value: 0, unit: deg}\n"
       << "            theta: {value: 0, unit: deg}\n"
       << "            psi: {value: 0, unit: deg}\n"
       << "        X: PropRudd(rpm)\n"
       << "        Y: 1\n"
       << "        Z: 2\n"
       << "        K: 3\n"
       << "        M: 4\n"
       << "        N: 5\n"
       << "      - name: PropRudd\n"
       << "        model: propeller+rudder\n"
       << "        position of propeller frame:\n"
       << "            frame: TestShip\n"
       << "            x: {value: -8.4, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 2.5, unit: m}\n"
       << "            phi: {value: 0, unit: rad}\n"
       << "            theta: {value: 3, unit: deg}\n"
       << "            psi: {value: 0, unit: deg}\n"
       << "        wake coefficient w: 0\n"
       << "        relative rotative efficiency etaR: 1\n"
       << "        thrust deduction factor t: 0\n"
       << "        rotation: clockwise\n"
       << "        number of blades: 4\n"
       << "        blade area ratio AE/A0: 0.55\n"
       << "        diameter: {value: 1.925, unit: m}\n"
       << "        rudder area: {value: 2.2, unit: m^2}\n"
       << "        rudder height: {value: 2, unit: m^2}\n"
       << "        effective aspect ratio factor: 1.7\n"
       << "        lift tuning coefficient: 2.1\n"
       << "        drag tuning coefficient: 1\n"
       << "        position of rudder in body frame:\n"
       << "            x: {value: 0.258, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 0.432, unit: m}\n"
       << "\n"
       << "output:\n"
       << "   - format: map\n"
       << "     filename: efforts.h5\n"
       << "     data: [t,  'Fx(F1,TestShip,TestShip)',  'Fy(F1,TestShip,TestShip)',  'Fz(F1,TestShip,TestShip)']\n";
    return ss.str();
}

std::string test_data::kt_kq()
{
    std::stringstream ss;
    ss << "name: port side propeller\n"
       << "model: Kt(J) & Kq(J)\n"
       << "position of propeller frame:\n"
       << "    frame: mesh(body 1)\n"
       << "    x: {value: -4, unit: m}\n"
       << "    y: {value: -2, unit: m}\n"
       << "    z: {value: 2, unit: m}\n"
       << "    phi: {value: 0, unit: rad}\n"
       << "    theta: {value: -10, unit: deg}\n"
       << "    psi: {value: -1, unit: deg}\n"
       << "wake coefficient w: 0.9\n"
       << "relative rotative efficiency etaR: 1\n"
       << "thrust deduction factor t: 0.7\n"
       << "rotation: clockwise\n"
       << "diameter: {value: 2, unit: m}\n"
       << "J: [-1.00000E+00,-8.00000E-01,-5.00000E-01,-2.50000E-01,-1.00000E-03,1.00000E-03, 2.00000E-01, 4.00000E-01, 6.00000E-01, 7.00000E-01, 8.00000E-01,1.00000E+00]\n"
       << "Kt: [-4.50000E-01,-2.50000E-01,-1.90000E-01,-2.00000E-01,-2.00000E-01,3.25000E-01, 2.80000E-01, 2.33000E-01, 1.85000E-01, 1.62000E-01,1.36000E-01,8.50000E-02]\n"
       << "Kq: [-4.80000E-02,-3.30000E-02,-2.20000E-02,-2.50000E-02,-2.80000E-02,3.40000E-02, 3.26000E-02, 2.97000E-02, 2.55000E-02, 2.30000E-02, 2.040000E-02,1.50000E-02]\n";
    return ss.str();
}

std::string test_data::linear_hydrostatics()
{
    std::stringstream ss;
    ss << "model: linear hydrostatics\n"
       << "z eq: {value: -2, unit: m}\n"
       << "theta eq: {value: 1, unit: deg}\n"
       << "phi eq: {value: -3, unit: deg}\n"
       << "K row 1: [1, 0 , 0]\n"
       << "K row 2: [0, 1 , 0]\n"
       << "K row 3: [0, 0 , 1]\n";
    return ss.str();
}

std::string test_data::test_ship_linear_hydrostatics_without_waves()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 1, 0, 0, 0)
       << initial_velocity("TestShip", 0, 0, 1, 0, 0, 0)
       << "    dynamics:\n"
       << hydrodynamic_calculation_point()
       << centre_of_inertia("TestShip", 0.258, 0, 0.432)
       << rigid_body_inertia_matrix()
       << no_added_mass()
       << "    external forces:\n"
       << "      - model: linear hydrostatics\n"
       << "        z eq: {value: -0.099, unit: m}\n"
       << "        theta eq: {value: 0, unit: deg}\n"
       << "        phi eq: {value: 0, unit: deg}\n"
       << "        K row 1: [100002.8, 0 , 0]\n"
       << "        K row 2: [0, 1E6 , 0]\n"
       << "        K row 3: [0, 0 , 1E6]\n";
    return ss.str();
}

std::string test_data::test_ship_linear_hydrostatics_with_waves()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: waves\n"
       << "    discretization:\n"
       << "       n: 128\n"
       << "       omega min: {value: 0.1, unit: rad/s}\n"
       << "       omega max: {value: 6, unit: rad/s}\n"
       << "       energy fraction: 0.999\n"
       << airy_depth_100()
       << stretching()
       << "        directional spreading:\n"
       << "           type: dirac\n"
       << "           waves propagating to: {value: 0, unit: deg}\n"
       << "        spectral density:\n"
       << "           type: dirac\n"
       << "           omega0: {value: 2.482701, unit: rad/s}\n"
       << "           Hs: {value: 15, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 1, 0, 0, 0)
       << initial_velocity("TestShip", 0, 0, 1, 0, 0, 0)
       << "    dynamics:\n"
       << hydrodynamic_calculation_point()
       << centre_of_inertia("TestShip", 0.258, 0, 0.432)
       << rigid_body_inertia_matrix()
       << no_added_mass()
       << "    external forces:\n"
       << "      - model: linear hydrostatics\n"
       << "        z eq: {value: -0.099, unit: m}\n"
       << "        theta eq: {value: 0, unit: deg}\n"
       << "        phi eq: {value: 0, unit: deg}\n"
       << "        K row 1: [100002.8, 0 , 0]\n"
       << "        K row 2: [0, 1E6 , 0]\n"
       << "        K row 3: [0, 0 , 1E6]\n";
    return ss.str();
}

std::string test_data::bug_2963_hs_fast()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 1, 0, 0, 0)
       << initial_velocity("TestShip", 0, 0, 1, 0, 0, 0)
       << "    dynamics:\n"
       << hydrodynamic_calculation_point()
       << centre_of_inertia("TestShip", 0.258, 0, 0.432)
       << rigid_body_inertia_matrix()
       << no_added_mass()
       << "    external forces:\n"
       << "      - model: non-linear hydrostatic (fast)\n";
    return ss.str();
}

std::string test_data::bug_2963_hs_exact()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 1, 0, 0, 0)
       << initial_velocity("TestShip", 0, 0, 1, 0, 0, 0)
       << "    dynamics:\n"
       << hydrodynamic_calculation_point()
       << centre_of_inertia("TestShip", 0.258, 0, 0.432)
       << rigid_body_inertia_matrix()
       << no_added_mass()
       << "    external forces:\n"
       << "      - model: non-linear hydrostatic (exact)\n";
    return ss.str();
}

std::string test_data::bug_2963_fk()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 1, 0, 0, 0)
       << initial_velocity("TestShip", 0, 0, 1, 0, 0, 0)
       << "    dynamics:\n"
       << hydrodynamic_calculation_point()
       << centre_of_inertia("TestShip", 0.258, 0, 0.432)
       << rigid_body_inertia_matrix()
       << no_added_mass()
       << "    external forces:\n"
       << "      - model: non-linear Froude-Krylov\n";
    return ss.str();
}

std::string test_data::bug_2963_diff()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 1, 0, 0, 0)
       << initial_velocity("TestShip", 0, 0, 1, 0, 0, 0)
       << "    dynamics:\n"
       << hydrodynamic_calculation_point()
       << centre_of_inertia("TestShip", 0.258, 0, 0.432)
       << rigid_body_inertia_matrix()
       << no_added_mass()
       << "    external forces:\n"
       << "      - model: diffraction\n"
       << "        hdb: test_ship.hdb\n"
       << "        calculation point in body frame:\n"
       << "            x: {value: 0.696, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 1.418, unit: m}\n"
       << "        mirror for 180 to 360: true\n";
    return ss.str();
}

std::string test_data::bug_2963_gm()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 1, 0, 0, 0)
       << initial_velocity("TestShip", 0, 0, 1, 0, 0, 0)
       << "    dynamics:\n"
       << hydrodynamic_calculation_point()
       << centre_of_inertia("TestShip", 0.258, 0, 0.432)
       << rigid_body_inertia_matrix()
       << no_added_mass()
       << "    external forces:\n"
       << "      - model: GM\n"
       << "        name of hydrostatic force model: non-linear hydrostatic (fast)\n"
       << "        roll step: {value: 60, unit: degree}\n";
    return ss.str();
}

std::string test_data::man_with_delay()
{
    std::stringstream ss;
    ss << "name: F\n"
       << "reference frame:\n"
       << "    frame: ball\n"
       << "    x: {value: 0, unit: m}\n"
       << "    y: {value: 0, unit: m}\n"
       << "    z: {value: 0, unit: m}\n"
       << "    phi: {value: 0, unit: deg}\n"
       << "    theta: {value: 0, unit: deg}\n"
       << "    psi: {value: 0, unit: deg}\n"
       << "commands: [command1, b, a]\n"
//       << "X: 1E6*(x(t-10) + command1*x(t-9) + 2*b*y(t-8) + z(t-7)/a)\n"
       << "X: 1E6*(x(t))\n"
       << "Y: 1E6*(x(t-10))\n"
       << "Z: 1E6*(command1*x(t))\n"
       << "K: 1E6*(b*x(t-6)\n"
       << "M: 1E6*(x(t-6) + command1*x(t-5) + 2*b*y(t-4) + z(t-3)/a)\n"
       << "N: 0";
    return ss.str();
}

std::string test_data::dummy_history()
{
    std::stringstream ss;
    ss << "{\"Dt\": 1234.5,\n"
       << "\"states\":\n"
       << "[ {\"t\": 0,   \"x\": 90, \"y\": 91, \"z\": 92, \"u\": 93, \"v\": 94, \"w\": 95, \"p\": 96, \"q\": 97, \"r\": 98, \"qr\": 99, \"qi\": 100, \"qj\": 101, \"qk\": 102}\n"
       << ", {\"t\": 0.1, \"x\": 70, \"y\": 71, \"z\": 72, \"u\": 73, \"v\": 74, \"w\": 75, \"p\": 76, \"q\": 77, \"r\": 78, \"qr\": 79, \"qi\": 80,  \"qj\": 81,  \"qk\": 82}\n"
       << ", {\"t\": 0.2, \"x\": 50, \"y\": 51, \"z\": 52, \"u\": 53, \"v\": 54, \"w\": 55, \"p\": 56, \"q\": 57, \"r\": 58, \"qr\": 59, \"qi\": 60,  \"qj\": 61,  \"qk\": 62}\n"
       << ", {\"t\": 0.3, \"x\": 30, \"y\": 31, \"z\": 32, \"u\": 33, \"v\": 34, \"w\": 35, \"p\": 36, \"q\": 37, \"r\": 38, \"qr\": 39, \"qi\": 40,  \"qj\": 41,  \"qk\": 42}\n"
       << ", {\"t\": 0.4, \"x\": 10, \"y\": 11, \"z\": 12, \"u\": 13, \"v\": 14, \"w\": 15, \"p\": 16, \"q\": 17, \"r\": 18, \"qr\": 19, \"qi\": 20,  \"qj\": 21,  \"qk\": 22}\n"
       << "], \"commands\": null}";
    return ss.str();
}

std::string test_data::complete_yaml_message_from_gui()
{
    std::stringstream ss;
    ss << "{\"Dt\": 2.0, " << std::endl
       << "\"states\":[" << std::endl
       << "{\"t\": 1234.5, "
       << "\"x\": 90, "
       << "\"y\": 91, "
       << "\"z\": 92, "
       << "\"u\": 93, "
       << "\"v\": 94, "
       << "\"w\": 95, "
       << "\"p\": 96, "
       << "\"q\": 97, "
       << "\"r\": 98, "
       << "\"qr\": 99, "
       << "\"qi\": 100, "
       << "\"qj\": 101, "
       << "\"qk\": 102}]" << std::endl
       <<",\"commands\":" << std::endl
       <<"  {\"RPM\": 1.2," << std::endl
       <<"  \"B1\": 0.1}}";
    return ss.str();
}

std::string test_data::JSON_message_with_requested_output()
{
    std::stringstream ss;
    ss << "{\"Dt\": 2.0, " << std::endl
       << "\"states\":[" << std::endl
       << "{\"t\": 1234.5, "
       << "\"x\": 90, "
       << "\"y\": 91, "
       << "\"z\": 92, "
       << "\"u\": 93, "
       << "\"v\": 94, "
       << "\"w\": 95, "
       << "\"p\": 96, "
       << "\"q\": 97, "
       << "\"r\": 98, "
       << "\"qr\": 99, "
       << "\"qi\": 100, "
       << "\"qj\": 101, "
       << "\"qk\": 102}]" << std::endl
       <<",\"commands\":" << std::endl
       <<"  {\"RPM\": 1.2," << std::endl
       <<"  \"B1\": 0.1}"
       <<",\"requested_output\":" << std::endl
       <<"  [\"Fx(force,body,NED)\","
       <<"  \"By(body)\"]}";
    return ss.str();
}

std::string test_data::JSON_server_request_GM_cube_with_output()
{
    std::stringstream ss;
    ss << "{\"Dt\": 10.0, " << std::endl
       << "\"states\":" << std::endl
       << "[ {\"t\": 1.87, "
       << "\"x\": 4.0,"
       << "\"y\": 8.0, "
       << "\"z\": 12.0, "
       << "\"u\": 1.0, "
       << "\"v\": 0.0, "
       << "\"w\": 0.0, "
       << "\"p\": 0.0, "
       << "\"q\": 0.0, "
       << "\"r\": 0.0, "
       << "\"qr\": 1.0, "
       << "\"qi\": 0.0, "
       << "\"qj\": 0.0, "
       << "\"qk\": 0.0}]" << std::endl
       <<",\"commands\": null," << std::endl
       <<"\"requested_output\":" << std::endl
       <<"  [\"GM(cube)\","
       <<"  \"GZ(cube)\"]}";

    return ss.str();
}



std::string test_data::simserver_message_without_Dt()
{
    std::stringstream ss;
    ss << "{\"states\":[" << std::endl
       << "{\"t\": 1234.5, "
       << "\"x\": 90, "
       << "\"y\": 91, "
       << "\"z\": 92, "
       << "\"u\": 93, "
       << "\"v\": 94, "
       << "\"w\": 95, "
       << "\"p\": 96, "
       << "\"q\": 97, "
       << "\"r\": 98, "
      << "\"qr\": 99, "
      << "\"qi\": 100, "
      << "\"qj\": 101, "
      << "\"qk\": 102}]" << std::endl
      <<",\"commands\":" << std::endl
      <<"  {\"RPM\": 1.2," << std::endl
      <<"  \"B1\": 0.1}}";
    return ss.str();
}

std::string test_data::complete_yaml_message_for_falling_ball()
{
    std::stringstream ss;
    ss << "{\"Dt\": 10.0, " << std::endl
       << "\"states\":" << std::endl
       << "[ {\"t\": 1.87, "
       << "\"x\": 4.0,"
       << "\"y\": 8.0, "
       << "\"z\": 12.0, "
       << "\"u\": 1.0, "
       << "\"v\": 0.0, "
       << "\"w\": 0.0, "
       << "\"p\": 0.0, "
       << "\"q\": 0.0, "
       << "\"r\": 0.0, "
      << "\"qr\": 1.0, "
      << "\"qi\": 0.0, "
      << "\"qj\": 0.0, "
      << "\"qk\": 0.0}]" << std::endl
      <<",\"commands\": null}";

    return ss.str();
}

std::string test_data::invalid_json_for_cs()
{
    std::stringstream ss;
    ss << "{\"Dt\": 0, " << std::endl
       << "\"states\":" << std::endl
       << "[ {\"t\": 1.87, "
       << "\"x\": 4.0,"
       << "\"y\": 8.0, "
       << "\"z\": 12.0, "
       << "\"u\": 1.0, "
       << "\"v\": 0.0, "
       << "\"w\": 0.0, "
       << "\"p\": 0.0, "
       << "\"q\": 0.0, "
       << "\"r\": 0.0, "
      << "\"qr\": 1.0, "
      << "\"qi\": 0.0, "
      << "\"qj\": 0.0, "
      << "\"qk\": 0.0}]" << std::endl
      <<",\"commands\": null}";

    return ss.str();
}

std::string test_data::bug_3004()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: Ponton\n"
       << "    mesh: Ponton.stl\n"
       << position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, -2.167, 0, 0, 0)
       << initial_velocity("Ponton", 0, 0, 0, 0, 0, 0)
       << "    dynamics:\n"
       << "        hydrodynamic forces calculation point in body frame:\n"
       << "            x: {value: 0, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 0, unit: m}\n"
       << centre_of_inertia("Ponton", 0, 0, 0)
       << "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
       << "            row 1: [1.025e7,0,0,0,0,0]\n"
       << "            row 2: [0,1.025e7,0,0,0,0]\n"
       << "            row 3: [0,0,1.025e7,0,0,0]\n"
       << "            row 4: [0,0,0,4.271e8,0,0]\n"
       << "            row 5: [0,0,0,0,8.627e9,0]\n"
       << "            row 6: [0,0,0,0,0,8.883e9]\n"
       << "        added mass matrix at the center of gravity and projected in the body frame:\n"
       << "            row 1: [1.025e7,0,0,0,0,0]\n"
       << "            row 2: [0,1.025e7,0,0,0,0]\n"
       << "            row 3: [0,0,1.025e7,0,0,0]\n"
       << "            row 4: [0,0,0,4.271e8,0,0]\n"
       << "            row 5: [0,0,0,0,8.627e9,0]\n"
       << "            row 6: [0,0,0,0,0,8.883e9]\n"
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: GM \n"
       << "        name of hydrostatic force model: non-linear hydrostatic (fast)\n"
       << "        roll step: {value: 1, unit: degree}\n"
       << "output:\n"
       << "   - format: csv\n"
       << "     filename: GM.csv\n"
       << "     data: ['t','GM(TestShip)']\n";
    return ss.str();
}

std::string test_data::bug_3003()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "    \n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << position_relative_to_mesh(0, 0, 0, 0, 0, 0)
       << initial_position_of_body_frame_deg(0, 0, -0.099, 0, -0.334, 0)
       << initial_velocity_kt("TestShip", 10, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: maneuvering\n"
       << "        name: F1\n"
       << "        reference frame:\n"
       << "            frame: TestShip\n"
       << "            x: {value: 0.696, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 1.418, unit: m}\n"
       << "            phi: {value: 1.418, unit: deg}\n"
       << "            theta: {value: 1.418, unit: deg}\n"
       << "            psi: {value: 1.418, unit: deg}\n"
       << "        X: x(t-3)\n"
       << "        Y: 1\n"
       << "        Z: 2\n"
       << "        K: 3\n"
       << "        M: 4\n"
       << "        N: 5\n";
    return ss.str();
}
std::string test_data::bug_2984()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << "environmental constants:\n"
       << "    g: {value: 9.81, unit: m/s^2}\n"
       << "    rho: {value: 1025, unit: kg/m^3}\n"
       << "    nu: {value: 1.19e-6, unit: m^2/s}\n"
       << "environment models:\n"
       << "  - model: no waves\n"
       << "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       << "\n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: ship\n"
       << position_relative_to_mesh(0, 0, 0, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 0, 0, 0, 0)
       << initial_velocity("ship", 0, 0, 0, 0, 0, 0)
       << "    dynamics:\n"
       << "        hydrodynamic forces calculation point in body frame:\n"
       << "            x: {value: 0, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 4.15, unit: m}\n"
       << centre_of_inertia("ship", 0, 0, 0)
       << "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
       << "            row 1: [6.09e6,0,0,0,0,0]\n"
       << "            row 2: [0,6.09e6,0,0,0,0]\n"
       << "            row 3: [0,0,6.09e6,0,0,0]\n"
       << "            row 4: [0,0,0,3.09e8,0,0]\n"
       << "            row 5: [0,0,0,0,6.44e9,0]\n"
       << "            row 6: [0,0,0,0,0,6.44e9]\n"
       << "        added mass matrix at the center of gravity and projected in the body frame:\n"
       << "            # T min Aqua+ (3.5s)\n"
       << "            row 1: [5.815E+04, 0.000E+00, 2.027E+05, 0.000E+00, 1.465E+07, 0.000E+00]\n"
       << "            row 2: [0.000E+00, 1.519E+06, 0.000E+00, 1.824E+05, 0.000E+00, 1.783E+07]\n"
       << "            row 3: [2.027E+05, 0.000E+00, 7.822E+06, 0.000E+00, 7.591E+07, 0.000E+00]\n"
       << "            row 4: [0.000E+00, 1.824E+05, 0.000E+00, 3.134E+07, 0.000E+00, 1.408E+08]\n"
       << "            row 5: [1.465E+07, 0.000E+00, 7.591E+07, 0.000E+00, 6.399E+09, 0.000E+00]\n"
       << "            row 6: [0.000E+00, 1.783E+07, 0.000E+00, 1.408E+08, 0.000E+00, 2.047E+09]\n"
       << "    external forces:\n"
       << "      - name: propeller\n"
       << "        model: wageningen B-series\n"
       << "        position of propeller frame:\n"
       << "            frame: ship\n"
       << "            x: {value: -53.319, unit: m}\n"
       << "            y: {value: -3.750, unit: m}\n"
       << "            z: {value: 6.799, unit: m}\n"
       << "            phi: {value: 0, unit: rad}\n"
       << "            theta: {value: 4., unit: deg}\n"
       << "            psi: {value: 0, unit: deg}\n"
       << "        wake coefficient w: 0.037\n"
       << "        relative rotative efficiency etaR: 1.\n"
       << "        thrust deduction factor t: 0.144\n"
       << "        rotation: clockwise\n"
       << "        number of blades: 4\n"
       << "        blade area ratio AE/A0: 0.77\n"
       << "        diameter: {value: 4.65, unit: m}\n"
       << "        rudder area: {value: 10.766, unit: m^2}\n"
       << "        rudder height: {value: 4.164, unit: m}\n"
       << "        effective aspect ratio factor: 1.7\n"
       << "output:\n"
       << "   - format: map\n"
       << "     data: ['Fx(propeller,ship,ship)',  'Fy(propeller,ship,ship)',  'Fz(propeller,ship,ship)',  'Mx(propeller,ship,ship)',  'My(propeller,ship,ship)',  'Mz(propeller,ship,ship)']\n";
    return ss.str();
}

std::string test_data::bug_3217()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: waves\n"
       << "    discretization:\n"
       << "       n: 1\n"
       << "       omega min: {value: 0.1, unit: rad/s}\n"
       << "       omega max: {value: 6.2, unit: rad/s}\n"
       << "       energy fraction: 0.999\n"
       << "    spectra:\n"
       << "      - model: airy\n"
       << "        depth: {value: 100000, unit: m}\n"
       << "        seed of the random data generator: 0\n"
       << "        stretching:\n"
       << "           delta: 0\n"
       << "           h: {unit: m, value: 100000}\n"
       << "        directional spreading:\n"
       << "           type: dirac\n"
       << "           waves propagating to: {value: 0.001, unit: deg}\n"
       << "        spectral density:\n"
       << "           type: dirac\n"
       << "           omega0: {value: 0.0982, unit: rad/s}\n"
       << "           Hs: {value: 2., unit: m}\n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, -5, 0, -0.0058, 0)
       << initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: diffraction\n"
       << "        hdb: v00_complet_FK_diffr.hdb\n"
       << "        calculation point in body frame:\n"
       << "            x: {value: 0.696, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 1.418, unit: m}\n"
       << "        mirror for 180 to 360: true\n"
       << "    blocked dof:\n"
       << "        from YAML:\n"
       << "            - state: u\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant\n"
       << "            - state: v\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant\n"
       << "            - state: w\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant\n"
       << "            - state: p\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant\n"
       << "            - state: q\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant\n"
       << "            - state: r\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant\n"
       << "output:\n"
       << "   - format: map\n"
       << "     data: ['Fx(diffraction,TestShip,TestShip)', 'Fy(diffraction,TestShip,TestShip)', 'Fz(diffraction,TestShip,TestShip)', 'Mx(diffraction,TestShip,TestShip)', 'My(diffraction,TestShip,TestShip)', 'Mz(diffraction,TestShip,TestShip)']\n";
    return ss.str();
}
std::string test_data::bug_3227()
{
    std::stringstream ss;
    ss << rotation_convention()
       << "\n"
       << environmental_constants()
       << "environment models:\n"
       << "  - model: waves\n"
       << "    discretization:\n"
       << "       n: 1\n"
       << "       omega min: {value: 0.1, unit: rad/s}\n"
       << "       omega max: {value: 6.2, unit: rad/s}\n"
       << "       energy fraction: 0.999\n"
       << "    spectra:\n"
       << "      - model: airy\n"
       << "        depth: {value: 100000, unit: m}\n"
       << "        seed of the random data generator: 0\n"
       << "        stretching:\n"
       << "           delta: 0\n"
       << "           h: {unit: m, value: 100000}\n"
       << "        directional spreading:\n"
       << "           type: dirac\n"
       << "           waves propagating to: {value: 30, unit: deg}\n"
       << "        spectral density:\n"
       << "           type: dirac\n"
       << "           omega0: {value: 0.0982, unit: rad/s}\n"
       << "           Hs: {value: 2., unit: m}\n"
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: TestShip\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, -5, 0, -0.0058, 0)
       << initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       << dynamics()
       << "    external forces:\n"
       << "      - model: diffraction\n"
       << "        hdb: v00_complet_FK_diffr.hdb\n"
       << "        calculation point in body frame:\n"
       << "            x: {value: 0.696, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 1.418, unit: m}\n"
       << "        mirror for 180 to 360: true\n"
       << "    blocked dof:\n"
       << "        from YAML:\n"
       << "            - state: u\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant\n"
       << "            - state: v\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant\n"
       << "            - state: w\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant\n"
       << "            - state: p\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant\n"
       << "            - state: q\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant\n"
       << "            - state: r\n"
       << "              t: [0, 10000]\n"
       << "              value: [0, 0]\n"
       << "              interpolation: piecewise constant   \n"
       << "output:\n"
       << "   - format: map\n"
       << "     data: ['Fx(diffraction,TestShip,TestShip)', 'Fy(diffraction,TestShip,TestShip)', 'Fz(diffraction,TestShip,TestShip)', 'Mx(diffraction,TestShip,TestShip)', 'My(diffraction,TestShip,TestShip)', 'Mz(diffraction,TestShip,TestShip)']\n";
    return ss.str();
}

std::string test_data::constant_force()
{
    return "model: constant force\n"
           "frame: NED\n"
           "x: {value: 0.5, unit: m}\n"
           "y: {value: -0.2, unit: m}\n"
           "z: {value: -440, unit: m}\n"
           "X: {value: 10, unit: kN}\n"
           "Y: {value: 20, unit: kN}\n"
           "Z: {value: 30, unit: kN}\n"
           "K: {value: 100, unit: kN*m}\n"
           "M: {value: 200, unit: kN*m}\n"
           "N: {value: 300, unit: kN*m}\n";
}

std::string test_data::tutorial_09_gRPC_wave_model()
{
    return rotation_convention()
         + "\n"
         + "environmental constants:\n"
         + "    g: {value: 9.81, unit: m/s^2}\n"
         + "    rho: {value: 1026, unit: kg/m^3}\n"
         + "    nu: {value: 1.18e-6, unit: m^2/s}\n"
         + "environment models:\n"
         + "  - model: grpc\n"
         + "    url: waves-server:50051\n"
         + "    Hs: 1.5\n"
         + "    Tp: 10\n"
         + "    gamma: 1.2\n"
         + "    waves propagating to: 180\n"
         + "    omega: [0.448, 0.483, 0.523, 0.571, 0.628, 0.698, 0.785, 0.897, 1.047]\n"
         + "bodies: # All bodies have NED as parent frame\n"
         + "  - name: cube\n"
         + "    mesh: cube.stl\n"
         + position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
         + initial_position_of_body_frame_deg(0, 0, 0.25, 0, 2, 0)
         + initial_velocity("cube", 0, 0, 0, 0, 0, 0)
         + "    dynamics:\n"
         + hydrodynamic_calculation_point()
         + centre_of_inertia("cube", 0, 0, 0.4)
         + "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
         + "            row 1: [83.33,0,0,0,0,0]\n"
         + "            row 2: [0,83.33,0,0,0,0]\n"
         + "            row 3: [0,0,83.33,0,0,0]\n"
         + "            row 4: [0,0,0,83.33,0,0]\n"
         + "            row 5: [0,0,0,0,83.33,0]\n"
         + "            row 6: [0,0,0,0,0,83.33]\n"
         + no_added_mass()
         + "    external forces:\n"
         + "      - model: gravity\n"
         + "      - model: non-linear hydrostatic (fast)\n"
         + "      - model: diffraction\n"
         + "        hdb: test_ship.hdb\n"
         + "        calculation point in body frame:\n"
         + "            x: {value: 0.696, unit: m}\n"
         + "            y: {value: 0, unit: m}\n"
         + "            z: {value: 1.418, unit: m}\n"
         + "        mirror for 180 to 360: true\n" + "\n";
}

std::string test_data::tutorial_10_gRPC_force_model()
{
    return rotation_convention()
         + "\n"
         + environmental_constants()
         + "environment models:\n"
         + "  - model: no waves\n"
         + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
         + "    \n"
         + "# Fixed frame: NED\n"
         + "bodies: # All bodies have NED as parent frame\n"
         + "  - name: TestShip\n"
         + "    filtered states:\n"
         + "      u:\n"
         + "          type of filter: moving average\n"
         + "          duration in seconds : 2.3\n"
         + "      v:\n"
         + "          type of filter: moving average\n"
         + "          duration in seconds : 3\n"
         + position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
         + initial_position_of_body_frame(5, 0, 1, 0, 0, 0)
         + initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
         + dynamics()
         + "    external forces:\n"
         + "      - name: parametric oscillator\n"
         + "        model: grpc\n"
         + "        url: force-model:9002\n"
         + "        k: 60\n"
         + "        c: 1\n";
}

std::string test_data::tutorial_14_filtered_states()
{
    return rotation_convention()
         + "\n"
         + environmental_constants()
         + "environment models:\n"
         + "  - model: no waves\n"
         + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
         + "    \n"
         + "# Fixed frame: NED\n"
         + "bodies: # All bodies have NED as parent frame\n"
         + "  - name: TestShip\n"
         + "    filtered states:\n"
         + "      z:\n"
         + "          type of filter: moving average\n"
         + "          duration in seconds : 5\n"
         + position_relative_to_mesh(0, 0, 0, 0, 0, 0)
         + initial_position_of_body_frame(0, 0, 0, 0, 0, 0)
         + initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
         + dynamics()
         + "    external forces:\n"
         + "      - model: grpc\n"
         + "        name: filtered force model\n"
         + "        url: force-model:9002\n"
         +  "output:\n"
         +  "   - format: csv\n"
         +  "     filename: filtered_states.csv\n"
         +  "     data: [t, 'x(TestShip)',  'x_filtered(TestShip)', 'z(TestShip)',  'z_filtered(TestShip)', 'xf(TestShip)', 'x_(TestShip)']\n";
}

std::string test_data::tutorial_10_gRPC_force_model_commands()
{
    return "commands:\n"
           "  - name: parametric oscillator\n"
           "    t: [0,1,3,10]\n"
           "    omega: {unit: rad/s, values: [3, 30, 30, 40]}\n";
}

std::string test_data::gRPC_force_model()
{
    return "name: parametric oscillator\n"
           "model: grpc\n"
           "url: force-model:9002\n"
           "k: 60\n"
           "c: 1\n";
}

std::string test_data::gRPC_controller()
{
    return "command: port side propeller(beta)\n"
           "dt: 1\n"
           "gains:\n"
           "  Kd: -1\n"
           "  Ki: 0\n"
           "  Kp: -1\n"
           "name: PID\n"
           "setpoint: psi_co\n"
           "state weights:\n"
           "  psi: 1\n"
           "type: grpc\n"
           "url: pid:9002";
}

std::string test_data::fmi()
{
    return rotation_convention()
        +  environmental_constants()
        +  environmental_models_no_waves_no_wind()
        +  "# Fixed frame: NED\n"
        +  "bodies: # All bodies have NED as parent frame\n"
        +  "  - name: TestShip\n"
        +  "    # mesh: test_ship.stl\n"
        +  position_relative_to_mesh(9.355, 0, -3.21, 0, 0, 0)
        +  initial_position_of_body_frame_deg(0, 0, -0.099, 0, -0.334, 0)
        +  initial_velocity_kt("TestShip", 10, 0, 0, 0, 0, 0)
        +  "    dynamics:\n"
        +  hydrodynamic_calculation_point()
        +  centre_of_inertia("TestShip", 0.258, 0, 0.432)
        +  "        mass: {value: 253.31, unit: tonne} # Caution: 'ton' is the british ton which is 907.185 kg\n"
        +  "        rigid body inertia matrix at the center of gravity and projected in the body frame:\n"
        +  "            frame: TestShip\n"
        +  "            row 1: [253310,0,0,0,0,0]\n"
        +  "            row 2: [0,253310,0,0,0,0]\n"
        +  "            row 3: [0,0,253310,0,0,0]\n"
        +  "            row 4: [0,0,0,1.522e6,0,0]\n"
        +  "            row 5: [0,0,0,0,8.279e6,0]\n"
        +  "            row 6: [0,0,0,0,0,7.676e6]\n"
        +  "        added mass matrix at the center of gravity and projected in the body frame:\n"
        +  "            frame: TestShip\n"
        +  "            row 1: [3.519e4,0,0,0,0,0]\n"
        +  "            row 2: [0,3.023e5,0,0,0,0]\n"
        +  "            row 3: [0,0,1.980e5,0,0,0]\n"
        +  "            row 4: [0,0,0,3.189e5,0,0]\n"
        +  "            row 5: [0,0,0,0,8.866e6,0]\n"
        +  "            row 6: [0,0,0,0,0,6.676e6]\n"
        +  "    external forces:\n"
        +  "      - model: gravity\n"
        +  "      # - model: non-linear hydrostatic (fast)\n"
        +  "      - model: linear hydrostatics\n"
        +  "        z eq: {value: 0.0, unit: m}\n"
        +  "        theta eq: {value: 0, unit: deg}\n"
        +  "        phi eq: {value: 0, unit: deg}\n"
        +  "        K row 1: [1e5, 0 , 0]\n"
        +  "        K row 2: [0, 1e6 , 0]\n"
        +  "        K row 3: [0, 0 , 1e6]\n"
        +  "      - model: linear damping\n"
        +  damping_matrix()
        +  "      - model: quadratic damping\n"
        +  "        damping matrix at the center of gravity projected in the body frame:\n"
        +  "            row 1: [ 0, 0, 0,      0, 0, 0]\n"
        +  "            row 2: [ 0, 0, 0,      0, 0, 0]\n"
        +  "            row 3: [ 0, 0, 0,      0, 0, 0]\n"
        +  "            row 4: [ 0, 0, 0, 1.45e6, 0, 0]\n"
        +  "            row 5: [ 0, 0, 0,      0, 0, 0]\n"
        +  "            row 6: [ 0, 0, 0,      0, 0, 0]\n"
        +  "      - model: resistance curve\n"
        +  "        speed: {unit: m/s, values: [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8]}\n"
        +  "        resistance: {unit: N, values: [0.000E+00,2.102E+02,7.728E+02,1.647E+03,2.803E+03,4.230E+03,5.999E+03,8.498E+03,1.273E+04,2.084E+04,2.789E+04,4.238E+04,7.737E+04,1.449E+05,2.439E+05,3.590E+05,4.741E+05]}\n"
        +  "      - model: maneuvering\n"
        +  "        name: F1\n"
        +  "        reference frame:\n"
        +  "            frame: TestShip\n"
        +  "            x: {value: 0.696, unit: m}\n"
        +  "            y: {value: 0, unit: m}\n"
        +  "            z: {value: 1.418, unit: m}\n"
        +  "            phi: {value: 1.418, unit: deg}\n"
        +  "            theta: {value: 1.418, unit: deg}\n"
        +  "            psi: {value: 1.418, unit: deg}\n"
        +  "        X: 0.5*rho*Vs^2*L^2*X_\n"
        +  "        Y: 0.5*rho*Vs^2*L^2*Y_\n"
        +  "        Z: 0\n"
        +  "        K: 0\n"
        +  "        M: 0\n"
        +  "        N: 0.5*rho*Vs^2*L^3*N_\n"
        +  "        rho: 1025\n"
        +  "        Vs: sqrt(u(t)^2+v(t)^2)\n"
        +  "        L: 21.569\n"
        +  "        X_: Xu*u_ + Xuu*u_^2 + Xuuu*u_^3 + Xvv*v_^2 + Xrr*r_^2 + Xvr*abs(v_)*abs(r_)\n"
        +  "        Y_: Yv*v_ + Yvv*v_*abs(v_) + Yvvv*v_^3 + Yvrr*v_*r_^2 + Yr*r_ + Yrr*r_*abs(r_) + Yrrr*r_^3 + Yrvv*r_*v_^2\n"
        +  "        N_: Nv*v_ + Nvv*v_*abs(v_) + Nvvv*v_^3 + Nvrr*v_*r_^2 + Nr*r_ + Nrr*r_*abs(r_) + Nrrr*r_^3 + Nrvv*r_*v_^2\n"
        +  "        u_: u(t)/Vs\n"
        +  "        v_: v(t)/Vs\n"
        +  "        r_: r(t)/Vs*L\n"
        +  "        Xu: 0\n"
        +  "        Xuu: 0\n"
        +  "        Xuuu: 0\n"
        +  "        Xvv: -0.041\n"
        +  "        Xrr: -0.01\n"
        +  "        Xvr: -0.015\n"
        +  "        Yv: -0.13\n"
        +  "        Yvv: -0.18\n"
        +  "        Yvvv: 0\n"
        +  "        Yvrr: 0\n"
        +  "        Yr: 0.015\n"
        +  "        Yrr: 0.021\n"
        +  "        Yrrr: 0\n"
        +  "        Yrvv: 0\n"
        +  "        Nv: -0.37\n"
        +  "        Nvv: -0.12\n"
        +  "        Nvvv: 0\n"
        +  "        Nvrr: 0\n"
        +  "        Nr: -0.1\n"
        +  "        Nrr: 0.005\n"
        +  "        Nrrr: 0\n"
        +  "        Nrvv: 0\n"
        +  "      - name: PropRudd\n"
        +  "        model: propeller+rudder\n"
        +  "        position of propeller frame:\n"
        +  "            frame: TestShip\n"
        +  "            x: {value: -8.4, unit: m}\n"
        +  "            y: {value: 0, unit: m}\n"
        +  "            z: {value: 2.5, unit: m}\n"
        +  "            phi: {value: 0, unit: rad}\n"
        +  "            theta: {value: 3, unit: deg}\n"
        +  "            psi: {value: 0, unit: deg}\n"
        +  "        wake coefficient w: 0\n"
        +  "        relative rotative efficiency etaR: 1\n"
        +  "        thrust deduction factor t: 0\n"
        +  "        rotation: clockwise\n"
        +  "        number of blades: 4\n"
        +  "        blade area ratio AE/A0: 0.55\n"
        +  "        diameter: {value: 1.925, unit: m}\n"
        +  "        rudder area: {value: 2.2, unit: m^2}\n"
        +  "        rudder height: {value: 2, unit: m^2}\n"
        +  "        effective aspect ratio factor: 1.7\n"
        +  "        lift tuning coefficient: 2.1\n"
        +  "        drag tuning coefficient: 1\n"
        +  "        position of rudder in body frame:\n"
        +  "            x: {value: 0.258, unit: m}\n"
        +  "            y: {value: 0, unit: m}\n"
        +  "            z: {value: 0.432, unit: m}\n"
        +  "\n"
        +  "output:\n"
        +  "   - format: map\n"
        +  "     filename: efforts.h5\n"
        +  "     data: [t,  'Mz(PropRudd,TestShip,TestShip)']\n";
}

std::string test_data::tutorial_11_gRPC_controller()
{
    return heading_keeping_base()
                + "setpoints:\n"
                + "    - t: [0,250,250.0001,500]\n"
                + "      psi_co: {unit: deg, values: [30,30,45,45]}\n"
                + "controllers:\n"
                + "    - type: grpc\n"
                + "      name: portside controller\n"
                + "      url: pid:9002\n"
                + "      dt: 0.3\n"
                + "      state weights:\n"
                + "        psi: 1\n"
                + "      setpoint: psi_co\n"
                + "      command: PSPropRudd(beta)\n"
                + "      gains:\n"
                + "        Kp: -1\n"
                + "        Ki: 0\n"
                + "        Kd: -1\n"
                + "    - type: PID\n"
                + "      name: starboard controller\n"
                + "      dt: 0.7\n"
                + "      state weights:\n"
                + "        psi: 1\n"
                + "      setpoint: psi_co\n"
                + "      command: SBPropRudd(beta)\n"
                + "      gains:\n"
                + "        Kp: -1\n"
                + "        Ki: 0\n"
                + "        Kd: -1\n"
                "output:\n"
                "  - format: csv\n"
                "    filename: tutorial_11.csv\n"
                "    data: [t, 'psi(dtmb)', 'PSPropRudd(beta)', 'SBPropRudd(beta)', 'Ten times current time']\n";
}

std::string test_data::tutorial_13_hdb_force_model()
{
    return rotation_convention()
       + "\n"
       + environmental_constants()
       + "environment models:\n"
       + "  - model: no waves\n"
       + "    constant sea elevation in NED frame: {value: 0, unit: m}\n"
       + "    \n"
       + "# Fixed frame: NED\n"
       + "bodies: # All bodies have NED as parent frame\n"
       + "  - name: TestShip\n"
       + position_relative_to_mesh(0, 0, 0.5, 0, 0, 0)
       + initial_position_of_body_frame(5, 0, 1, 0, 0, 0)
       + initial_velocity("TestShip", 0, 0, 0, 0, 0, 0)
       + dynamics()
       + "    external forces:\n"
       + "      - model: grpc\n"
       + "        name: hdb force model\n"
       + "        url: force-model:9002\n"
       + "        hdb: big.hdb\n"
       + "output:\n"
       + "   - format: csv\n"
       + "     filename: hdb_output.csv\n"
       + "     data:\n"
       + "     - 'Ma(0,0)(TestShip)'\n"
       + "     - 'Ma(0,2)(TestShip)'\n"
       + "     - 'Ma(0,4)(TestShip)'\n"
       + "     - 'Ma(1,1)(TestShip)'\n"
       + "     - 'Ma(1,3)(TestShip)'\n"
       + "     - 'Ma(1,5)(TestShip)'\n"
       + "     - 'Ma(2,0)(TestShip)'\n"
       + "     - 'Ma(2,2)(TestShip)'\n"
       + "     - 'Ma(2,4)(TestShip)'\n"
       + "     - 'Ma(3,1)(TestShip)'\n"
       + "     - 'Ma(3,3)(TestShip)'\n"
       + "     - 'Ma(3,5)(TestShip)'\n"
       + "     - 'Ma(4,0)(TestShip)'\n"
       + "     - 'Ma(4,2)(TestShip)'\n"
       + "     - 'Ma(4,4)(TestShip)'\n"
       + "     - 'Ma(5,1)(TestShip)'\n"
       + "     - 'Ma(5,3)(TestShip)'\n"
       + "     - 'Ma(5,5)(TestShip)'\n"
       + "     - 'diffX(0,0)(TestShip)'\n"
       + "     - 'diffX(0,1)(TestShip)'\n"
       + "     - 'diffX(1,0)(TestShip)'\n"
       + "     - 'diffX(1,1)(TestShip)'\n"
       + "     - 'diffX(2,0)(TestShip)'\n"
       + "     - 'diffX(2,1)(TestShip)'\n"
       + "     - 'diffY(0,0)(TestShip)'\n"
       + "     - 'diffY(0,1)(TestShip)'\n"
       + "     - 'diffY(1,0)(TestShip)'\n"
       + "     - 'diffY(1,1)(TestShip)'\n"
       + "     - 'diffY(2,0)(TestShip)'\n"
       + "     - 'diffY(2,1)(TestShip)'\n"
       + "     - 'diffZ(0,0)(TestShip)'\n"
       + "     - 'diffZ(0,1)(TestShip)'\n"
       + "     - 'diffZ(1,0)(TestShip)'\n"
       + "     - 'diffZ(1,1)(TestShip)'\n"
       + "     - 'diffZ(2,0)(TestShip)'\n"
       + "     - 'diffZ(2,1)(TestShip)'\n"
       + "     - 'diffK(0,0)(TestShip)'\n"
       + "     - 'diffK(0,1)(TestShip)'\n"
       + "     - 'diffK(1,0)(TestShip)'\n"
       + "     - 'diffK(1,1)(TestShip)'\n"
       + "     - 'diffK(2,0)(TestShip)'\n"
       + "     - 'diffK(2,1)(TestShip)'\n"
       + "     - 'diffM(0,0)(TestShip)'\n"
       + "     - 'diffM(0,1)(TestShip)'\n"
       + "     - 'diffM(1,0)(TestShip)'\n"
       + "     - 'diffM(1,1)(TestShip)'\n"
       + "     - 'diffM(2,0)(TestShip)'\n"
       + "     - 'diffM(2,1)(TestShip)'\n"
       + "     - 'diffN(0,0)(TestShip)'\n"
       + "     - 'diffN(0,1)(TestShip)'\n"
       + "     - 'diffN(1,0)(TestShip)'\n"
       + "     - 'diffN(1,1)(TestShip)'\n"
       + "     - 'diffN(2,0)(TestShip)'\n"
       + "     - 'diffN(2,1)(TestShip)'\n"
       + "     - 'diffpX(0,0)(TestShip)'\n"
       + "     - 'diffpX(0,1)(TestShip)'\n"
       + "     - 'diffpX(1,0)(TestShip)'\n"
       + "     - 'diffpX(1,1)(TestShip)'\n"
       + "     - 'diffpX(2,0)(TestShip)'\n"
       + "     - 'diffpX(2,1)(TestShip)'\n"
       + "     - 'diffpY(0,0)(TestShip)'\n"
       + "     - 'diffpY(0,1)(TestShip)'\n"
       + "     - 'diffpY(1,0)(TestShip)'\n"
       + "     - 'diffpY(1,1)(TestShip)'\n"
       + "     - 'diffpY(2,0)(TestShip)'\n"
       + "     - 'diffpY(2,1)(TestShip)'\n"
       + "     - 'diffpZ(0,0)(TestShip)'\n"
       + "     - 'diffpZ(0,1)(TestShip)'\n"
       + "     - 'diffpZ(1,0)(TestShip)'\n"
       + "     - 'diffpZ(1,1)(TestShip)'\n"
       + "     - 'diffpZ(2,0)(TestShip)'\n"
       + "     - 'diffpZ(2,1)(TestShip)'\n"
       + "     - 'diffpK(0,0)(TestShip)'\n"
       + "     - 'diffpK(0,1)(TestShip)'\n"
       + "     - 'diffpK(1,0)(TestShip)'\n"
       + "     - 'diffpK(1,1)(TestShip)'\n"
       + "     - 'diffpK(2,0)(TestShip)'\n"
       + "     - 'diffpK(2,1)(TestShip)'\n"
       + "     - 'diffpM(0,0)(TestShip)'\n"
       + "     - 'diffpM(0,1)(TestShip)'\n"
       + "     - 'diffpM(1,0)(TestShip)'\n"
       + "     - 'diffpM(1,1)(TestShip)'\n"
       + "     - 'diffpM(2,0)(TestShip)'\n"
       + "     - 'diffpM(2,1)(TestShip)'\n"
       + "     - 'diffpN(0,0)(TestShip)'\n"
       + "     - 'diffpN(0,1)(TestShip)'\n"
       + "     - 'diffpN(1,0)(TestShip)'\n"
       + "     - 'diffpN(1,1)(TestShip)'\n"
       + "     - 'diffpN(2,0)(TestShip)'\n"
       + "     - 'diffpN(2,1)(TestShip)'\n"
       + "     - 'diffT0(TestShip)'\n"
       + "     - 'diffT1(TestShip)'\n"
       + "     - 'diffT2(TestShip)'\n"
       + "     - 'diffTp0(TestShip)'\n"
       + "     - 'diffTp1(TestShip)'\n"
       + "     - 'diffTp2(TestShip)'\n"
       + "     - 'diffpsi0(TestShip)'\n"
       + "     - 'diffpsi1(TestShip)'\n"
       + "     - 'diffppsi0(TestShip)'\n"
       + "     - 'diffppsi1(TestShip)'\n"
       + "     - 'FKFMX(0,0)(TestShip)'\n"
       + "     - 'FKFMX(0,1)(TestShip)'\n"
       + "     - 'FKFMX(1,0)(TestShip)'\n"
       + "     - 'FKFMX(1,1)(TestShip)'\n"
       + "     - 'FKFMX(2,0)(TestShip)'\n"
       + "     - 'FKFMX(2,1)(TestShip)'\n"
       + "     - 'FKFMY(0,0)(TestShip)'\n"
       + "     - 'FKFMY(0,1)(TestShip)'\n"
       + "     - 'FKFMY(1,0)(TestShip)'\n"
       + "     - 'FKFMY(1,1)(TestShip)'\n"
       + "     - 'FKFMY(2,0)(TestShip)'\n"
       + "     - 'FKFMY(2,1)(TestShip)'\n"
       + "     - 'FKFMZ(0,0)(TestShip)'\n"
       + "     - 'FKFMZ(0,1)(TestShip)'\n"
       + "     - 'FKFMZ(1,0)(TestShip)'\n"
       + "     - 'FKFMZ(1,1)(TestShip)'\n"
       + "     - 'FKFMZ(2,0)(TestShip)'\n"
       + "     - 'FKFMZ(2,1)(TestShip)'\n"
       + "     - 'FKFMK(0,0)(TestShip)'\n"
       + "     - 'FKFMK(0,1)(TestShip)'\n"
       + "     - 'FKFMK(1,0)(TestShip)'\n"
       + "     - 'FKFMK(1,1)(TestShip)'\n"
       + "     - 'FKFMK(2,0)(TestShip)'\n"
       + "     - 'FKFMK(2,1)(TestShip)'\n"
       + "     - 'FKFMM(0,0)(TestShip)'\n"
       + "     - 'FKFMM(0,1)(TestShip)'\n"
       + "     - 'FKFMM(1,0)(TestShip)'\n"
       + "     - 'FKFMM(1,1)(TestShip)'\n"
       + "     - 'FKFMM(2,0)(TestShip)'\n"
       + "     - 'FKFMM(2,1)(TestShip)'\n"
       + "     - 'FKFMN(0,0)(TestShip)'\n"
       + "     - 'FKFMN(0,1)(TestShip)'\n"
       + "     - 'FKFMN(1,0)(TestShip)'\n"
       + "     - 'FKFMN(1,1)(TestShip)'\n"
       + "     - 'FKFMN(2,0)(TestShip)'\n"
       + "     - 'FKFMN(2,1)(TestShip)'\n"
       + "     - 'FKFMpX(0,0)(TestShip)'\n"
       + "     - 'FKFMpY(0,0)(TestShip)'\n"
       + "     - 'FKFMpZ(0,0)(TestShip)'\n"
       + "     - 'FKFMpK(0,0)(TestShip)'\n"
       + "     - 'FKFMpM(0,0)(TestShip)'\n"
       + "     - 'FKFMpN(0,0)(TestShip)'\n"
       + "     - 'FKFMpX(1,0)(TestShip)'\n"
       + "     - 'FKFMpY(1,0)(TestShip)'\n"
       + "     - 'FKFMpZ(1,0)(TestShip)'\n"
       + "     - 'FKFMpK(1,0)(TestShip)'\n"
       + "     - 'FKFMpM(1,0)(TestShip)'\n"
       + "     - 'FKFMpN(1,0)(TestShip)'\n"
       + "     - 'FKT0(TestShip)'\n"
       + "     - 'FKT1(TestShip)'\n"
       + "     - 'FKT2(TestShip)'\n"
       + "     - 'FKTp0(TestShip)'\n"
       + "     - 'FKTp1(TestShip)'\n"
       + "     - 'FKTp2(TestShip)'\n"
       + "     - 'FKTp6(TestShip)'\n"
       + "     - 'FKpsi0(TestShip)'\n"
       + "     - 'FKpsi1(TestShip)'\n"
       + "     - 'FKppsi0(TestShip)'\n"
       + "     - 'FKppsi1(TestShip)'\n"
       + "     - 'omega0(TestShip)'\n"
       + "     - 'omega1(TestShip)'\n"
       + "     - 'omega2(TestShip)'\n"
       + "     - 'forwardSpeed(TestShip)'\n"
       + "     - 'Ma000(TestShip)'\n"
       + "     - 'Ma001(TestShip)'\n"
       + "     - 'Ma002(TestShip)'\n"
       + "     - 'Ma110(TestShip)'\n"
       + "     - 'Ma111(TestShip)'\n"
       + "     - 'Ma112(TestShip)'\n"
       + "     - 'Ma220(TestShip)'\n"
       + "     - 'Ma221(TestShip)'\n"
       + "     - 'Ma222(TestShip)'\n"
       + "     - 'Ma330(TestShip)'\n"
       + "     - 'Ma331(TestShip)'\n"
       + "     - 'Ma332(TestShip)'\n"
       + "     - 'Ma440(TestShip)'\n"
       + "     - 'Ma441(TestShip)'\n"
       + "     - 'Ma442(TestShip)'\n"
       + "     - 'Ma550(TestShip)'\n"
       + "     - 'Ma551(TestShip)'\n"
       + "     - 'Ma552(TestShip)'\n"
       + "     - 'Br_0_0_0(TestShip)'\n"
       + "     - 'Br_0_0_6(TestShip)'\n"
       + "     - 'Br_5_0_0(TestShip)'\n"
       + "     - 'Br_5_0_6(TestShip)'\n"
       + "     - 'Br_0_5_0(TestShip)'\n"
       + "     - 'Br_0_5_6(TestShip)'\n"
       + "     - 'Br_5_5_0(TestShip)'\n"
       + "     - 'Br_5_5_6(TestShip)'\n"
       + "     - 'driftX(0,0)(TestShip)'\n"
       + "     - 'driftX(1,0)(TestShip)'\n"
       + "     - 'driftX(0,1)(TestShip)'\n"
       + "     - 'driftY(0,0)(TestShip)'\n"
       + "     - 'driftY(1,0)(TestShip)'\n"
       + "     - 'driftY(0,1)(TestShip)'\n"
       + "     - 'driftM(0,0)(TestShip)'\n"
       + "     - 'driftM(1,0)(TestShip)'\n"
       + "     - 'driftM(0,1)(TestShip)'\n"
       + "     - 'driftTp(0)(TestShip)'\n"
       + "     - 'driftTp(1)(TestShip)'\n"
       + "     - 'driftTp(2)(TestShip)'\n"
       + "     - 'driftPsi(0)(TestShip)'\n"
       + "     - 'driftPsi(1)(TestShip)'\n"
       ;
}

std::string test_data::tutorial_13_precal_r_force_model()
{
    std::string ret(tutorial_13_hdb_force_model());
    boost::replace_all(ret, "hdb: big.hdb", "raodb: ONRT_SIMMAN.raodb.ini");
    boost::replace_all(ret, "hdb force model", "precal-r force model");
    boost::replace_all(ret, "filename: hdb_output.csv", "filename: precal_r_output.csv");
    return ret;
}

std::string test_data::added_mass_from_precal_file()
{
    std::stringstream ss;
    ss << rotation_convention()
       << environmental_constants()
       << environmental_models_no_waves_no_wind()
       << "# Fixed frame: NED\n"
       << "bodies: # All bodies have NED as parent frame\n"
       << "  - name: body 1\n"
       << "    mesh: test_ship.stl\n"
       << position_relative_to_mesh(10, 0, 0, 0, 0, 0)
       << initial_position_of_body_frame(0, 0, 0, 0, 0, 0)
       << initial_velocity("body", 0, 0, 0, 0, 0, 0)
       << "    dynamics:\n"
       << hydrodynamic_calculation_point()
       << centre_of_inertia("body 1", 0, 0, 0)
       << "        centre of inertia:\n"
       << "            frame: body 1\n"
       << "            x: {value: 0, unit: m}\n"
       << "            y: {value: 0, unit: m}\n"
       << "            z: {value: 0, unit: m}\n"
       << rigid_body_inertia_matrix()
       << "        added mass matrix at the center of gravity and projected in the body frame:\n"
       << "            from raodb: ONRT_SIMMAN.raodb.ini\n"
       << "    external forces:\n"
       << "      - model: gravity\n"
       << "      - model: non-linear hydrostatic (fast)\n"
       << "\n";
    return ss.str();
}
