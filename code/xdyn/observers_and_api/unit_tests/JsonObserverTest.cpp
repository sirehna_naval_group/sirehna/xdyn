#include "JsonObserverTest.hpp"
#include "JsonObserver.hpp"
#include "ListOfObservers.hpp"
#include "xdyn/observers_and_api/simulator_api.hpp"
#include "xdyn/test_data_generator/yaml_data.hpp"
#include "xdyn/yaml_parser/parse_output.hpp"
#include <ssc/solver/steppers.hpp>

JsonObserverTest::JsonObserverTest() : a(ssc::random_data_generator::DataGenerator(666))
{
}

TEST_F(JsonObserverTest, should_be_able_to_create_an_observer)
{
    const double dt = 1E-1;
    const double tend = 10;
    auto sys = get_system(test_data::falling_ball_example(), 0);
    const auto yaml = parse_output(test_data::falling_ball_example());
    {
        ssc::solver::Scheduler scheduler(0, tend, dt);
        ListOfObservers observers(yaml);
        ssc::solver::quicksolve<ssc::solver::EulerStepper>(sys, scheduler, observers);
    }
    for (auto output:yaml)
    {
        if (output.format == "json")
        {
            if (not output.filename.empty())
            {
                //EXPECT_EQ(0,remove(output.filename.c_str()));
            }
        }
    }
}

TEST_F(JsonObserverTest, should_be_able_to_create_an_observer_with_waves)
{
    const double dt = 1E-1;
    const double tend = 10;
    auto sys = get_system(test_data::falling_ball_example(), 0);
    const auto yaml = parse_output(test_data::falling_ball_example());
    {
        ssc::solver::Scheduler scheduler(0, tend, dt);
        ListOfObservers observers(yaml);
        ssc::solver::quicksolve<ssc::solver::EulerStepper>(sys, scheduler, observers);
    }
    for (auto output:yaml)
    {
        if (output.format == "json")
        {
            if (not output.filename.empty())
            {
                //EXPECT_EQ(0,remove(output.filename.c_str()));
            }
        }
    }
}
