#ifndef FORCE_MODELS_UNIT_TESTS_INC_HYDROPOLARFORCEMODELTEST_HPP_
#define FORCE_MODELS_UNIT_TESTS_INC_HYDROPOLARFORCEMODELTEST_HPP_

#include "gtest/gtest.h"

class HydroPolarForceModelTest : public ::testing::Test
{
    public:
        HydroPolarForceModelTest();
        virtual ~HydroPolarForceModelTest() = default;
};

#endif /* FORCE_MODELS_UNIT_TESTS_INC_HYDROPOLARFORCEMODELTEST_HPP_ */
