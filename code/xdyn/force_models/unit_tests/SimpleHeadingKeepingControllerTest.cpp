/*
 * SimpleTrackKeepingControllerTest.cpp
 *
 *  Created on: Jan 15, 2015
 *      Author: cady
 */

#include "SimpleHeadingKeepingControllerTest.hpp"
#include "SimpleHeadingKeepingController.hpp"
#include "xdyn/core/BodyStates.hpp"
#include "xdyn/test_data_generator/yaml_data.hpp"

#define _USE_MATH_DEFINE
#include <cmath>
#define PI M_PI

#define EPS 1E-14

SimpleHeadingKeepingControllerTest::SimpleHeadingKeepingControllerTest() : a(ssc::random_data_generator::DataGenerator(545454))
{
}

SimpleHeadingKeepingControllerTest::~SimpleHeadingKeepingControllerTest()
{
}

void SimpleHeadingKeepingControllerTest::SetUp()
{
}

void SimpleHeadingKeepingControllerTest::TearDown()
{
}

TEST_F(SimpleHeadingKeepingControllerTest, parser)
{
    const auto k = SimpleHeadingKeepingController::parse(test_data::simple_track_keeping());
    ASSERT_DOUBLE_EQ(4, k.Tp);
    ASSERT_DOUBLE_EQ(0.9, k.ksi);
    ASSERT_EQ("controller", k.name);
}

TEST_F(SimpleHeadingKeepingControllerTest, force_and_torque)
{
    auto input = SimpleHeadingKeepingController::parse(test_data::simple_track_keeping());
    input.Tp = 2*PI;
    EnvironmentAndFrames env;
    env.rot = YamlRotation("angle", {"z","y'","x''"});
    const SimpleHeadingKeepingController w(input, "body", env);
    ASSERT_EQ("simple heading controller", w.model_name());
    BodyStates states;
    const double psi = 1.234;
    states.qr.record(0,  cos(psi/2));
    states.qi.record(0,  0);
    states.qj.record(0,  0);
    states.qk.record(0,  sin(psi/2));
    states.r.record(0, 10);
    states.total_inertia = Eigen::Matrix<double,6,6>::Zero();
    states.total_inertia(2,2) = 4;

    std::map<std::string,double> commands;
    commands["psi_co"] = 5;

    const auto F = w.get_force(states, a.random<double>(), env, commands);

    ASSERT_NEAR(0, F.X(), EPS);
    ASSERT_NEAR(0, F.Y(), EPS);
    ASSERT_NEAR(0, F.Z(), EPS);
    ASSERT_NEAR(0, F.K(), EPS);
    ASSERT_NEAR(0, F.M(), EPS);
    ASSERT_NEAR(4*1*(5-psi)-2*0.9*4*1*10, F.N(), EPS);
}
