#ifndef FORCE_MODELS_UNIT_TESTS_INC_FLETTNERROTORFORCEMODELTEST_HPP_
#define FORCE_MODELS_UNIT_TESTS_INC_FLETTNERROTORFORCEMODELTEST_HPP_

#include "gtest/gtest.h"

class FlettnerRotorForceModelTest : public ::testing::Test
{
    public:
        FlettnerRotorForceModelTest();
        virtual ~FlettnerRotorForceModelTest() = default;
};

#endif /* FORCE_MODELS_UNIT_TESTS_INC_FLETTNERROTORFORCEMODELTEST_HPP_ */
