#ifndef FORCE_MODELS_UNIT_TESTS_INC_AEROPOLARFORCEMODELTEST_HPP_
#define FORCE_MODELS_UNIT_TESTS_INC_AEROPOLARFORCEMODELTEST_HPP_

#include "gtest/gtest.h"

class AeroPolarForceModelTest : public ::testing::Test
{
    public:
        AeroPolarForceModelTest();
        virtual ~AeroPolarForceModelTest() = default;
};

#endif /* FORCE_MODELS_UNIT_TESTS_INC_AEROPOLARFORCEMODELTEST_HPP_ */
