/*
 * EnvironmentAndFramesTest.hpp
 *
 *  Created on: 17 déc. 2020
 *      Author: mcharlou2016
 */

#ifndef CORE_UNIT_TESTS_INC_ENVIRONMENTANDFRAMESTEST_HPP_
#define CORE_UNIT_TESTS_INC_ENVIRONMENTANDFRAMESTEST_HPP_

#include "gtest/gtest.h"

class EnvironmentAndFramesTest : public ::testing::Test
{
public:
    EnvironmentAndFramesTest ();
    virtual ~EnvironmentAndFramesTest ();
};

#endif /* CORE_UNIT_TESTS_INC_ENVIRONMENTANDFRAMESTEST_HPP_ */
