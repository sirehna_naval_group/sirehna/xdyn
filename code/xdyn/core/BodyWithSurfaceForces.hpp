/*
 * BodyWithSurfaceForces.hpp
 *
 *  Created on: Jan 9, 2015
 *      Author: cady
 */

#ifndef BODYWITHSURFACEFORCES_HPP_
#define BODYWITHSURFACEFORCES_HPP_

#include "xdyn/core/Body.hpp"

class BodyWithSurfaceForces : public Body
{
    public:
        BodyWithSurfaceForces(const size_t idx, const BlockedDOF& blocked_states, const YamlFilteredStates& filtered_states);
        BodyWithSurfaceForces(const BodyStates& states, const size_t idx, const BlockedDOF& blocked_states, const YamlFilteredStates& filtered_states);
        BodyWithSurfaceForces(const size_t idx, const BlockedDOF& blocked_states, const StatesFilter& states_filter);
        BodyWithSurfaceForces(const BodyStates& states, const size_t idx, const BlockedDOF& blocked_states, const StatesFilter& states_filter);
        void update_intersection_with_free_surface(const EnvironmentAndFrames& env, const double t);
};

#endif /* BODYWITHSURFACEFORCES_HPP_ */
