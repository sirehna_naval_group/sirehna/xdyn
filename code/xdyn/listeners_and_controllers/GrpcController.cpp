#include "GrpcController.hpp"

GrpcController* GrpcController::build(const double tstart, const std::string& name_, const std::string& yaml, ssc::solver::ContinuousSystem& sys)
{
    const auto parsed_yaml = GrpcControllerInterface::parse(yaml);
    const auto grpc = GrpcControllerInterface::build(parsed_yaml, tstart);
    auto ret = new GrpcController(tstart, name_, grpc);
    const auto command_names = grpc->get_command_names();
    const auto setpoint_names = grpc->get_setpoint_names();
    std::vector<double> setpoints(setpoint_names.size(), 0);
    ret->add_controller_outputs_to_data_source(tstart, sys, setpoints);
    return ret;
}

std::vector<std::string> GrpcController::get_command_names() const
{
    return grpc->get_command_names();
}

GrpcController::GrpcController (const double tstart, const std::string& name_, const std::shared_ptr<GrpcControllerInterface>& grpc_) :
                    Controller(tstart, grpc_->get_dt(), name_)
                    , grpc(grpc_), name(name_)
{
    
}

void GrpcController::add_controller_outputs_to_data_source(const double time, ssc::solver::ContinuousSystem& sys, const std::vector<double>& setpoints)
{
    auto dx_dt = sys.get_latest_dx_dt();
    if (dx_dt.empty())
    {
        dx_dt = std::vector<double>(13, 0);
    }
    const auto response = grpc->get_commands(time, sys.state, dx_dt, setpoints);
    const auto commands = response.commands;
    for (auto command : commands)
    {
        Controller::set_discrete_state(sys, command.first, command.second);
    }
}

void GrpcController::update_discrete_states (const double time,
                                 ssc::solver::ContinuousSystem& sys)
{
    const auto setpoint_names = grpc->get_setpoint_names();
    std::vector<double> setpoints(setpoint_names.size(), 0);
    for (size_t i = 0 ; i < setpoint_names.size() ; ++i)
    {
        setpoints[i] = Controller::get_setpoint(sys, setpoint_names[i]);
    }
    add_controller_outputs_to_data_source(time, sys, setpoints);        
}
