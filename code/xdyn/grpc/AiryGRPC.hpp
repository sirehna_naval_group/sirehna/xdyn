/*
 * AiryGRPC.hpp
 */
#ifndef GRPC_INC_AIRYGRPC_HPP_
#define GRPC_INC_AIRYGRPC_HPP_

struct EnvironmentAndFrames;
void run_xdyn_airy_server(const EnvironmentAndFrames& env, const short unsigned int port_number);

#endif /* GRPC_INC_AIRYGRPC_HPP_ */
