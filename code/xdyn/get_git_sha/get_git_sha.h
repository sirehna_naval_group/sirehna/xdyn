#ifndef GET_GIT_SHA_HPP
#define GET_GIT_SHA_HPP

#ifdef __cplusplus
extern "C" {
#endif

const char* get_git_sha();

#ifdef __cplusplus
}
#endif

#endif
