Extinction en roulis
A réaliser : comparaison de l'angle de roulis entre essais maquette à l'échelle 1/11ème (voir fichier Excel "extinctions_TestShipEssaisECN.xlsx", onglet "Roulis", colonnes "Essais") et X-Dyn.

Extinction en tangage
A réaliser : comparaison de l'angle de tangage entre essais maquette à l'échelle 1/11ème (voir fichier Excel "extinctions_TestShipEssaisECN.xlsx", onglet "Tangage", colonnes "Essais") et X-Dyn.
Attention : incertitudes sur les mesures réalisées lors des essais en tangage (conditions, élastiques non-reproduits dans le simulateur).
