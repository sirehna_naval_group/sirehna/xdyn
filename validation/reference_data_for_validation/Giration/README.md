Les girations réalisées en essai ont été utilisées pour extraire les valeurs de
rayon de giration et vitesse Vx et Vy en fonction de l'angle de barre
(3 différents) et du rpm (3 différents).

La mise en données du simulateur est similaire à ce qui avait été fait pour SOS :
concaténation des différents cas en 1 seule simulation -> 9 couples (angle de
barre, rpm) différents.

Dans l'ordre :

- barre à 10°, 5tr/s
- barre à 20°, 5tr/s
- barre à 20°, 5tr/s
- barre à 10°, 3.5tr/s
- barre à 20°, 3.5tr/s
- barre à 30°, 3.5tr/s
- barre à 10°, 2.5tr/s
- barre à 20°, 2.5tr/s
- barre à 30°, 2.5tr/s

À réaliser : comparaison des 6 vitesses et 6 déplacements entre simulateur SOS
voir fichier Excel, onglet "SOS") et X-Dyn.

Pas de résultats d'essais (post-traitement difficile).
