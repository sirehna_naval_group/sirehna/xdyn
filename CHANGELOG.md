# v6.0.3 (2022-07-12)

- #262 Create a wavemodel instance from discretedirectionalwavespectrum with imposed phase
- #256 Parser precal_r seacal units
- #257 Create a docker image to deploy with xdyn python xdyn wheel and also proto file
- #260 Updated documentation on grpc attribute for force models
- #259 Fix python unit test tolerance
- #258 Merge doc directories and add fr suffix to the doc directory
- #255 Fix sphinx documentation build due to automatic upgrade of sphinx


# v6.0.2 (2022-05-31)

- #253 Add python grpc interface code to xdyn container
- #252 Fix ci build due to grpc library update
- #69 Update code so that it compiles with ubuntu 22 04 and gcc 11


# v6.0.1 (2022-05-20)

- #144 Utilisation modele hydrostatic avec houle
- #68 Extra observations output lag in server modes
- #250 Integration test build clean up
- #249 Fix tab linter error
- #247 Update xdyn documentation for efforts hydrostatiques lineaires
- #246 Wrong hydrostatic forces in waves with linear hydrostatics model
- #245 Remove unneded include instructions
- #244 Remove duplicated code for grpc spectrumresponse creation
- #243 Create a xdyn grpc airy server
- #66 Update code so that it compiles with intel compilers
- #241 Hdf5 files generated with version 6 contained twice time values


# v6.0.0 (2022-04-28)

- #235 Controller outputs are not written properly in hdf5 files
- #240 Fix interfaces submodule reference
- #39 Add possibility to use csv file as input to commands
- #238 Error in the documentation for the definition of the environment
- #65 Constant tag for beta version on dockerhub
- #234 Add python wrapper for flettnerrotormodel
- #62 Lift direction does not take angle command into account in polar force models
- #203 Typo error in xdyn doc
- #231 Typo in documentation
- #227 Build does not fail on python version check
- #61 Wrong frame in generic stiffness model
- #233 Fix broken documentation generation
- #229 Clearer error message on parse error for hydro polar model
- #222 Wrong exception when the wrong key is set in yaml file
- #226 Update changelog due to new version 5 0 1 5 1
- #224 Update python code in documentation that does not work
- #60 Flettner rotor force model is not known by xdyn


# v5.1.0 (2022-02-28)

- #224 Update python code in documentation that does not work
- #223 Fix grpc directional_spectra method that contains an indice bug
- #59 Flettner rotor force model
- #221 Fix title section in documentation
- #220 Fix orbital_velocity in integration_tests grpc_tests waves airy py
- #219 Fix broken builds for debian 9 and debian 10
- #218 Fix documentation for method ruddermodel get_vs
- #217 Fix wave direction convention in code documentation
- #215 Fix documentation description for waves propagating to
- #214 Bug while evaluating torque transport
- #26 New feature generic stiffness matrix
- #57 Making lifting profile models controllable


# v5.0.1 (2021-12-02)

- #212 Hdf5 output no longer exports discretised spectra
- #54 Fix broken training documentation generation
- #209 Fix body ned force export that is located at the center of gravity of the body and not the
- #202 Error in the frame for efforts
- #206 Update broken links in main readme md


# v5.0.0 (2021-10-05)

- #53 Xdyn server still stays locked into errors
- #204 Date of first call to external controller does not correspond to tstart
- #92 Irregular waves separate number of frequential and directional rays
- #195 Homogeneisation des cles associees a precal_r dans fichiers yaml
- #200 Mistake in conversion matrix in doc
- #199 Bug cannot parse binary stl
- #52 Fix failed ci'
- #196 Bug can parse precal_r files with windows end of lines
- #51 Bump ssc version
- #138 Rendre l inclusion de maillage optionnelle dans les fichiers hdf5
- #50 Server modes error message is not reset when handling a new request
- #100 Ajouter le calcul a chaque instant des moyennes de tous les etats pour utilisation dans les
- #198 Corriger les typos dans la documentation suite a la relecture de code 118
- #197 Add hdb precal_r wave drift force data to grpc interface
- #40 Automatic detection of mesh panels orientation prevents the use of some geometries e g catamarans
- #41 Default outputting of all forces
- #46 Bad initialization of states for cs modes
- #49 Xdyn server stays locked into errors
- #194 Fire fix build issue on gitlab com
- #187 Add results from precal_r or hdb to grpc force interface
- #45 New feature mmg standard maneuvering force model
- #42 Co simulation json websocket mode returns the time vector for all states
- #193 Fix failing unit test for debian 9 for holtropmennenforcemodeltest numerical_example_1984
- #178 Precal r extraction des informations necessaires au calcul des efforts de froude krylov
- #191 Update current changelog md
- #188 Describe wave model conventions used by precal_r and aqua
- #177 Precal r extraction des matrices d amortissement de radiation
- #183 Slides for remote model training
- #184 Fix pulsation size check in hdb
- #175 Precal r extraction des informations necessaires au calcul des efforts de diffraction


# v4.3.0 (2021-06-29)

- #176 Precal r extraction des masses ajoutees
- #165 Mise en place de l interface precal_r x dyn
- #182 Fixed typo in doc_user interfaces_utilisateur m4 md
- #154 Rendre le developpement de modules modeles d efforts essentiellement possible facile par les
- #172 Should be able to output commands
- #95 Fix wave grpc
- #174 Add a command line flag to display concatenated yaml
- #89 Problem with history class
- #90 Add a word target for user documentation
- #39 Missing parameter documentation for radiation damping force model
- #37 Undocumented changes on diffraction force model
- #32 Diffraction force model does not take encounter frequency into account
- #31 Bug correction and performance improvement in radiationdampingforcemodel


# v4.2.0 (2021-05-04)

- #166 External controllers
- #16 New feature generic quadratic hydrodynamic polar model
- #171 Fix error outputting
- #28 Segfault in unit tests using googletest s testing internal capturestderr


# v4.1.0 (2021-04-16)

- #170 Remove local version of ssc solver
- #164 Pid controller for xdyn
- #5 Radiation damping force model improvement
- #17 New feature generic quadratic aerodynamic polar model
- #25 Holtrop mennen force model was not added to the simulator api
- #151 Integrate fmi generation
- #167 Bug sur le numero de version lors de l installation du paquet debian
- #22 Grpc server modes cs and me do not return the details of errors occurring in xdyn
- #21 Co simulation response always starts at t 0
- #7 Adding air specific mass to environment description
- #8 Outputting forces in server mode
- #4 Force models refactoring
- #6 Fix yaml rendering problem
- #1 Wind force models
- #155 Frequency discretisation of irregular wave spectrum in equal energy bins


# v4.0.4 (2020-11-20)

- #157 Push to gitlab
- #156 Reparer l integration continue de travis
- #153 Integration de la proposition de bug fix radiation damping force model correction
- #152 Add grpc interface to model exchange cosimulation
- #118 Divergence in tutorial_08_diffraction yml
- #120 Negative speed in resistance model in tutorial_05_froude_krylov yml
- #126 Modele propeller wageningen b series pris en compte repere
- #127 Definition des plages de temps et de valeur pour le forcage des degres de liberte
- #132 Bug dans la generation de la doc
- #134 Wave height in body frame does not change regardless of ship speed
- #109 Discontinuities found in euler angles
- #91 Bug on moment computation when efforts are expressed in a rotated frame
- #105 Upgrade gcc


# v4.0.3 (2020-03-16)



# v4.0.2 (2020-01-13)

- #99 Update spectrum images generation for documentation
- #94 Fixed docker compose tests for ci
- #85 Grpc force models do not register their commands
- #83 Update waves_grpc submodule not to depend on other submodules
- #82 Update repository submodule to fix github ci
- #77 Add the possibility to control precision of floating point numbers with ascii export
- #71 Preparer la serialisation de sorties supplementaires dans xdyn_for_cs
- #76 Ask the grpc force model for the reference frame
- #74 Create grpc docker container
- #72 Grpc force models
- #49 Vectorize orbital_velocity
- #55 Xdyn client code rpc wave model
- #50 Vectorize dynamic_pressure
- #69 Use const commands for force models
- #48 Vectorize elevation
- #68 Typo update in all hpp and cpp
- #67 Remove python dependencies from build
- #66 Fixed xdyn build without hos
- #65 Remove hos that will be refactored later
- #63 Build xdyn windows version with posix thread model
- #64 Improve makefile build scripts for eclipse


# v4.0.1 (2019-05-20)

- #62 Updated ci build for gitlab and travis
- #61 Update user documentation on tutorials
- #60 Fix readme travis ci authentication
- #59 Fixes for github
- #43 Add github deployment
- #42 Fixed for github travis
- #29 Fix issues on documentation detected by jjm
- #33 Simplify ssc fetching the ssc artifacts
- #31 Clean up deprecated build construction files
- #30 Websocket server for model exchange
- #30 Websocket server for model exchange
- #24 Get repository ready for github
- #23 Improve error message when yaml is invalid
- #20 Erreur lors de l utilisation du modele d effort constant force
- #14 Ajout module d effort constant
- #18 Les fichiers yaml d entrees ont une erreur d unite pour les tp sans consequence pour les resultats


