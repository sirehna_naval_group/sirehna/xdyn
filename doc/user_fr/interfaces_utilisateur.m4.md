changequote(`{{', `}}')

# Ligne de commande

## Liste des arguments

```python echo=False, results='verbatim', name='xdyn-command-line-arguments'
from subprocess import check_output
import re
r=re.compile(r"This is a.*USAGE:", re.DOTALL)
print(re.sub(r,"", check_output(['xdyn']).decode('utf-8')))
```

## Exemples

Les exemples suivants peuvent être lancés à partir du répertoire `demos`:

### Simulation avec un solveur Runge-Kutta d'ordre 4 en commençant à t=0

```bash
./xdyn tutorial_01_falling_ball.yml --dt 0.1 --tend 1
```

ou si l'on utilise une invite de commande MS-DOS :

```bash
xdyn tutorial_01_falling_ball.yml --dt 0.1 --tend 1
```

En ajoutant l'option -o csv, les sorties se font sur la sortie standard
(le terminal). Ceci permet de chaîner les traitements (pipe UNIX), par exemple :

```bash
./xdyn tutorial_01_falling_ball.yml --dt 0.1 --tend 1 -o csv| python plot.py test 0 3
```

La commande précédente lance la simulation et génère un tracé (format SVG) à
l'aide du
[script python de post-traitement](https://gitlab.sirehna.com/root/xdyn/blob/master/postprocessing/Python/plot.py)
livré avec le simulateur. Des scripts de post-traitement sont livrés avec
l'installation du simulateur, pour
[Matlab](https://gitlab.sirehna.com/root/xdyn/tree/master/postprocessing/MatLab),
[Python](https://gitlab.sirehna.com/root/xdyn/tree/master/postprocessing/Python).

### Simulation avec un solveur Euler en commençant à t=1

```bash
./xdyn tutorial_01_falling_ball.yml -s euler --dt 0.1 --tstart 1 --tend 1.2
```

# Documentations des données d'entrées du simulateur

Les données d'entrées du simulateur se basent sur un format
de fichiers [YAML](http://www.yaml.org/) qui fonctionne par clef-valeur.

Ce format présente l'avantage d'être facilement lisible et éditable. Des
parsers YAML existent pour de nombreux langages de programmation.

## Vue d'ensemble

### Liste des sections

Le fichier YAML comprend quatre sections de haut niveau :

- la section `rotations convention` définit la convention d'angles utilisée,
- `environmental constants` donne les valeurs de la gravité et la densité de
  l'eau,
- les modèles environnementaux figurent dans `environment models`,
- `bodies` décrit les corps simulés.

### Remarques sur les unités

On retrouve fréquemment dans le fichier YAML des lignes du type :

```yaml
clef: {value: 65456, unit: km}
```

Les unités ne sont pas vérifiées par le système : le parser se contente de
convertir toutes les entrées en unité du système international avant simulation,
sans préjuger de l'homogénéité. Ce décodage est fait en convertissant l'unité
en un facteur multiplicatif (en utilisant la liste des unités de l'utilitaire UNIX
[units](http://heirloom.cvs.sourceforge.net/viewvc/heirloom/heirloom/units/)) et
en multipliant la valeur originale par ce facteur multiplicatif. Par exemple:

```yaml
masse: {value: 10, unit: lb}
```

sera vu par xdyn comme:

```cpp
masse = 4.5359237;
```

L'exemple précédent :

```yaml
clef: {value: 65456, unit: km}
```

aurait tout aussi bien pu être écrit :

```yaml
clef: {value: 65456, unit: kW}
```

et on aurait obtenu exactement la même valeur numérique, bien que la grandeur
physique ne soit pas la même : on suppose donc que l'utilisateur renseigne des
données de façon homogène. En interne, tous les calculs sont faits en unité du
système international.

## Sorties

La génération des sorties dans xdyn est paramétrable de deux façons :

- soit directement en ligne de commande en utilisant le flag `-o`
- soit dans le fichier YAML, section `output`

Ces deux modes de fonctionnement peuvent être utilisés de façon conjointe.




### Paramétrage des sorties depuis la ligne de commande

Le flag `-o <format>` permet de générer facilement les sorties, soit dans un fichier,
soit sur la sortie standard (le terminal) (pour chaîner les traitements grâce
aux pipe UNIX "`|`").

Lorsque l'utilisateur ne spécifie que l'extension (par exemple `-o csv`), les sorties
sont écrites sur la sortie standard (affichées à l'écran). Lorsqu'un chemin complet
est donné (par exemple `-o test.csv`) les sorties se font dans un fichier au chemin
spécifié.

Les valeurs de `<format>` possibles sont :


|                      `<format>`                    | Description | Example |
|----------------------------------------------------|-------------| ------- |
| `csv` ou `<filename>.csv`            | Format [CSV (comma-separated values)](https://en.wikipedia.org/wiki/Comma-separated_values)     | `-o csv` (écriture sur la sortie standard) ou `-o test.csv` (écriture dans un fichier) |
| `tsv` ou `<filename>.csv`            | Format [TSV (Tab-Separated Values)](https://en.wikipedia.org/wiki/Tab-separated_values)       | `-o tsv` (écriture sur la sortie standard) ou `-o test.tsv` (écriture dans un fichier) |
| `json` ou `<filename>.json`          | Format [JSON (JavaScript Object Notation)](https://en.wikipedia.org/wiki/JSON) | `-o json` (écriture sur la sortie standard) ou `-o test.json` (écriture dans un fichier) |
| `<filename>.hdf5` ou `<filename>.h5` | Format [HDF5 (Hierarchical Data Format)](https://en.wikipedia.org/wiki/Hierarchical_Data_Format) utilisé par exemple par Matlab | Seule l'écrituredans un fichier est possible : `-o test.h5` ou `-o test.hdf5` |
| `ws <address>`                       | Sérialisation au format JSON sur un websocket. xdyn se connecte alors à l'adresse indiquée pour envoyer ses sorties en cours de simulation. `xdyn` est alors en mode client et non en mode serveur : pour une utilisation en mode serveur voir `xdyn-for-me` et `xdyn-for-cs`. |

Le format CSV a l'allure suivante :

```csv
Fx(blocked states ball ball),Fx(fictitious forces ball NED),Fx(fictitious forces ball ball),Fx(gravity ball NED),Fx(gravity ball ball),Fx(sum of forces ball NED),Fx(sum of forces ball ball),Fy(blocked states ball ball),Fy(fictitious forces ball NED),Fy(fictitious forces ball ball),Fy(gravity ball NED),Fy(gravity ball ball),Fy(sum of forces ball NED),Fy(sum of forces ball ball),Fz(blocked states ball ball),Fz(fictitious forces ball NED),Fz(fictitious forces ball ball),Fz(gravity ball NED),Fz(gravity ball ball),Fz(sum of forces ball NED),Fz(sum of forces ball ball),Mx(blocked states ball ball),Mx(fictitious forces ball NED),Mx(fictitious forces ball ball),Mx(gravity ball NED),Mx(gravity ball ball),Mx(sum of forces ball NED),Mx(sum of forces ball ball),My(blocked states ball ball),My(fictitious forces ball NED),My(fictitious forces ball ball),My(gravity ball NED),My(gravity ball ball),My(sum of forces ball NED),My(sum of forces ball ball),Mz(blocked states ball ball),Mz(fictitious forces ball NED),Mz(fictitious forces ball ball),Mz(gravity ball NED),Mz(gravity ball ball),Mz(sum of forces ball NED),Mz(sum of forces ball ball),p(ball),p_filtered(ball),phi(ball),phi_filtered(ball),psi(ball),psi_filtered(ball),q(ball),q_filtered(ball),qi(ball),qj(ball),qk(ball),qr(ball),r(ball),r_filtered(ball),t,theta(ball),theta_filtered(ball),u(ball),u_filtered(ball),v(ball),v_filtered(ball),w(ball),w_filtered(ball),x(ball),x_filtered(ball),y(ball),y_filtered(ball),z(ball),z_filtered(ball)
0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,9.810000e+06,9.810000e+06,9.810000e+06,9.810000e+06,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,1.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,-0.000000e+00,-0.000000e+00,1.000000e+00,1.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,4.000000e+00,4.000000e+00,8.000000e+00,8.000000e+00,1.200000e+01,1.200000e+01
0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,9.810000e+06,9.810000e+06,9.810000e+06,9.810000e+06,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,0.000000e+00,1.000000e+00,0.000000e+00,0.000000e+00,1.000000e+00,-0.000000e+00,-0.000000e+00,1.000000e+00,1.000000e+00,0.000000e+00,0.000000e+00,9.810000e+00,9.810000e+00,5.000000e+00,5.000000e+00,8.000000e+00,8.000000e+00,1.690500e+01,1.690500e+01
```

Le format TSV est de la forme :

```tsv
Fx(blocked states,ball,ball) Fx(fictitious forces,ball,NED) Fx(fictitious forces,ball,ball) Fx(gravity,ball,NED) Fx(gravity,ball,ball) Fx(sum of forces,ball,NED) Fx(sum of forces,ball,ball) Fy(blocked states,ball,ball) Fy(fictitious forces,ball,NED) Fy(fictitious forces,ball,ball) Fy(gravity,ball,NED) Fy(gravity,ball,ball) Fy(sum of forces,ball,NED) Fy(sum of forces,ball,ball) Fz(blocked states,ball,ball) Fz(fictitious forces,ball,NED) Fz(fictitious forces,ball,ball) Fz(gravity,ball,NED) Fz(gravity,ball,ball) Fz(sum of forces,ball,NED) Fz(sum of forces,ball,ball) Mx(blocked states,ball,ball) Mx(fictitious forces,ball,NED) Mx(fictitious forces,ball,ball) Mx(gravity,ball,NED) Mx(gravity,ball,ball) Mx(sum of forces,ball,NED) Mx(sum of forces,ball,ball) My(blocked states,ball,ball) My(fictitious forces,ball,NED) My(fictitious forces,ball,ball) My(gravity,ball,NED) My(gravity,ball,ball) My(sum of forces,ball,NED) My(sum of forces,ball,ball) Mz(blocked states,ball,ball) Mz(fictitious forces,ball,NED) Mz(fictitious forces,ball,ball) Mz(gravity,ball,NED) Mz(gravity,ball,ball) Mz(sum of forces,ball,NED) Mz(sum of forces,ball,ball)   p(ball) p_filtered(ball) phi(ball) phi_filtered(ball) psi(ball) psi_filtered(ball)   q(ball) q_filtered(ball)  qi(ball)  qj(ball)  qk(ball)  qr(ball)   r(ball) r_filtered(ball)         t theta(ball) theta_filtered(ball)   u(ball) u_filtered(ball)   v(ball) v_filtered(ball)   w(ball) w_filtered(ball)   x(ball) x_filtered(ball)   y(ball) y_filtered(ball)   z(ball) z_filtered(ball)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 9.810e+06 9.810e+06 9.810e+06 9.810e+06 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 1.000e+00 0.000e+00 0.000e+00 0.000e+00 -0.000e+00 -0.000e+00 1.000e+00 1.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 4.000e+00 4.000e+00 8.000e+00 8.000e+00 1.200e+01 1.200e+01
0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 9.810e+06 9.810e+06 9.810e+06 9.810e+06 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 0.000e+00 1.000e+00 0.000e+00 0.000e+00 1.000e+00 -0.000e+00 -0.000e+00 1.000e+00 1.000e+00 0.000e+00 0.000e+00 9.810e+00 9.810e+00 5.000e+00 5.000e+00 8.000e+00 8.000e+00 1.691e+01 1.691e+01
```

Le format JSON se présente comme suit :

```json
{"t":0,"states":{"ball":{"p":0,"p_filtered":0,"phi":0,"phi_filtered":0,"psi":0,"psi_filtered":0,"q":0,"q_filtered":0,"qi":0,"qj":0,"qk":0,"qr":1,"r":0,"r_filtered":0,"theta":-0,"theta_filtered":-0,"u":1,"u_filtered":1,"v":0,"v_filtered":0,"w":0,"w_filtered":0,"x":4,"x_filtered":4,"y":8,"y_filtered":8,"z":12,"z_filtered":12}},"wrenches":{"blocked states,ball,ball":{"Fx":0,"Fy":0,"Fz":0,"Mx":0,"My":0,"Mz":0},"fictitious forces,ball,NED":{"Fx":0,"Fy":0,"Fz":0,"Mx":0,"My":0,"Mz":0},"fictitious forces,ball,ball":{"Fx":0,"Fy":0,"Fz":0,"Mx":0,"My":0,"Mz":0},"gravity,ball,NED":{"Fx":0,"Fy":0,"Fz":9.81e+06,"Mx":0,"My":0,"Mz":0},"gravity,ball,ball":{"Fx":0,"Fy":0,"Fz":9.81e+06,"Mx":0,"My":0,"Mz":0},"sum of forces,ball,NED":{"Fx":0,"Fy":0,"Fz":9.81e+06,"Mx":0,"My":0,"Mz":0},"sum of forces,ball,ball":{"Fx":0,"Fy":0,"Fz":9.81e+06,"Mx":0,"My":0,"Mz":0}}}
{"t":1,"states":{"ball":{"p":0,"p_filtered":0,"phi":0,"phi_filtered":0,"psi":0,"psi_filtered":0,"q":0,"q_filtered":0,"qi":0,"qj":0,"qk":0,"qr":1,"r":0,"r_filtered":0,"theta":-0,"theta_filtered":-0,"u":1,"u_filtered":1,"v":0,"v_filtered":0,"w":9.81,"w_filtered":9.81,"x":5,"x_filtered":5,"y":8,"y_filtered":8,"z":16.905,"z_filtered":16.905}},"wrenches":{"blocked states,ball,ball":{"Fx":0,"Fy":0,"Fz":0,"Mx":0,"My":0,"Mz":0},"fictitious forces,ball,NED":{"Fx":0,"Fy":0,"Fz":0,"Mx":0,"My":0,"Mz":0},"fictitious forces,ball,ball":{"Fx":0,"Fy":0,"Fz":0,"Mx":0,"My":0,"Mz":0},"gravity,ball,NED":{"Fx":0,"Fy":0,"Fz":9.81e+06,"Mx":0,"My":0,"Mz":0},"gravity,ball,ball":{"Fx":0,"Fy":0,"Fz":9.81e+06,"Mx":0,"My":0,"Mz":0},"sum of forces,ball,NED":{"Fx":0,"Fy":0,"Fz":9.81e+06,"Mx":0,"My":0,"Mz":0},"sum of forces,ball,ball":{"Fx":0,"Fy":0,"Fz":9.81e+06,"Mx":0,"My":0,"Mz":0}}}
```

Lorsque l'on utilise la ligne de commande, le contenu des sorties n'est pas
paramétrable et xdyn écrit toutes les sorties disponibles. Pour plus de
contrôle, on peut utiliser le fichier YAML, dont le contenu est paramétrable.

### Paramétrage des sorties via le fichier YAML

La spécification des sorties dans le fichier YAML se fait au moyen de la
section `output`, à la racine du fichier YAML, dont voici un exemple :

```yaml
output:
   - format: csv
     filename: test.csv
     data: [t, x(ball), 'Fx(gravity,ball)']
```

- `format` : `csv` pour un fichier texte dont les colonnes sont séparées par
  une virgule, `tsv` pour un fichier texte dont les colonnes sont séparées par
  des tabulations, ou `hdf5` pour le format des fichiers .mat de MatLab (HDF5)
- `filename` : nom du fichier de sortie
- `data` : liste des colonnes à écrire (cf. ci-après pour la description complète)

### Liste des sorties possibles

#### Temps et états

Si l'on suppose que le nom du corps simulé est `TestShip` (configurable dans le YAML,
section `bodies[0]/name`), le temps et les états sont accessibles de la façon suivante :

| Variable    | Description                                       | Nom à utiliser dans la section `output` du YAML | Unité |
| ----------- | ------------------------------------------------- | ----------------------------------------------- | ----- |
| `x`         | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du [repère body](#rep%C3%A8re-navire-mobile-ou-body-ou-rep%C3%A8re-de-r%C3%A9solution) | `x(TestShip)` | m |
| `y`         | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY                                                                         | `y(TestShip)` | m |
| `z`         | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY                                                                         | `z(TestShip)` | m |
| `u`         | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                                                                                             | `u(TestShip)` | m/s |
| `v`         | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                                                                                             | `v(TestShip)` | m/s |
| `w`         | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                                                                                             | `w(TestShip)` | m/s |
| `p`         | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                                                                                                 | `p(TestShip)` | rad/s |
| `q`         | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                                                                                                 | `q(TestShip)` | rad/s |
| `r`         | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                                                                                                 | `r(TestShip)` | rad/s |
| `qr`        | Partie réelle du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                                                                                                             | `qr(TestShip)` | - |
| `qi`        | Première partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                                                                                                | `qi(TestShip)` | - |
| `qj`        | Seconde partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                                                                                                 | `qj(TestShip)` | - |
| `qk`        | Troisième partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                                                                                               | `qk(TestShip)` | - |
| `phi`       | Angle de roulis/gîte du navire par rapport au sol (BODY/NED)                                                                                                                                                                                                            | `phi(TestShip)` | rad |
| `theta`     | Angle de tangage/assiette par rapport au sol (BODY/NED)                                                                                                                                                                                                                 | `theta(TestShip)` | rad |
| `psi`       | Angle de lacet par rapport au sol (BODY/NED)                                                                                                                                                                                                                            | `psi(TestShip)` | rad |

Une [version filtrée](#États-filtrés) de ces états est également disponible, en
ajoutant le suffixe `_filtered` (par exemple `q_filtered`). Attention : les
quaternions ne sont pas filtrés (mais les angles d'Euler le sont).

Par exemple, pour générer un fichier CSV contenant le temps, l'embardée
(filtrée) et l'assiette pour le navire nommé `TestShip` on utiliserait la
section suivante :

```yaml
output:
   - format: csv
     filename: exemple.csv
     data: [t, y_filtered(TestShip), theta(TestShip)]
```

#### Observations supplémentaires des modèles d'effort

Lorsqu'un modèle d'effort déclare des observations supplémentaires
(`extra_observations`), celles-ci sont accessibles depuis la section `output`
du YAML d'xdyn et peuvent donc être incluses dans les fichiers de sortie.

Les modèles définissant des observations supplémentaires sont :

- Modèle d'effort hydrostatique : fournit `Bx`, `By`, `Bz`.
- Modèle d'effort `GM` : fournit `GM`, `GZ`.
- Modèle d'effort `hydrodynamic polar` : fournit `alpha`, `U`.
- Modèles gRPC : peuvent définir des observations externes

Un modèle gRPC externe Python déclarant la méthode `force` suivante :

```python
def force(self, states, commands, __):
    """Force model."""
    return {'Fx': 0,
            'Fy': 0,
            'Fz': 0,
            'Mx': 0,
            'My': 0,
            'Mz': 0,
            'extra_observations': {'k': 2, 'harmonic_oscillator_time': states.t[0]}}
```

rendra `harmonic_oscillator_time` disponible.

On peut ensuite accéder à ces sorties supplémentaires de la façon suivante :

```yaml
output:
   - format: tsv
     filename: exemple.tsv
     data:
        - 'harmonic_oscillator_time(TestShip)'
```

#### Efforts

Les sorties d'effort sont spécifiées de la façon suivante :

- `Fx(modèle,corps,repère)` en N
- `Fy(modèle,corps,repère)` en N
- `Fz(modèle,corps,repère)` en N
- `Mx(modèle,corps,repère)` en N.m
- `My(modèle,corps,repère)` en N.m
- `Mz(modèle,corps,repère)` en N.m

où `modèle` est le nom du modèle d'effort (renseigné dans la clef `modèle` de
chaque modèle d'effort), `corps` est le nom du corps sur lequel agit l'effort
et `repère` est le repère d'expression (qui ne peut être que `NED`, le nom du
corps ou le nom du modèle d'effort (si sa paramétrisation comporte une clef
`frame`, c'est-à-dire pour `hydrodynamic polar`, `aerodynamic polar`,
`maneuvering`, `wageningen B-series`, `propeller+rudder`, `Kt(J) & Kq(J)` et
`grpc`). Les forces et les moments sont toujours exprimés à l'origine du repère
concerné, y compris pour les repères NED local (centré au centre de gravité
du navire mais avec des axes fixes dans le repère terrestre) et BODY.

La somme des efforts appliqués au solide est accessible via les clefs
suivantes :

- `Fx(sum of forces,corps,repère)`, par exemple `Fx(sum of forces,TestShip,NED)` ou `Fx(sum of forces,TestShip,TestShip)`
- `Fy(sum of forces,corps,repère)`, par exemple `Fy(sum of forces,TestShip,NED)` ou `Fy(sum of forces,TestShip,TestShip)`
- `Fz(sum of forces,corps,repère)`, par exemple `Fz(sum of forces,TestShip,NED)` ou `Fz(sum of forces,TestShip,TestShip)`
- `Mx(sum of forces,corps,repère)`, par exemple `Mx(sum of forces,TestShip,NED)` ou `Mx(sum of forces,TestShip,TestShip)`
- `My(sum of forces,corps,repère)`, par exemple `My(sum of forces,TestShip,NED)` ou `My(sum of forces,TestShip,TestShip)`
- `Mz(sum of forces,corps,repère)`, par exemple `Mz(sum of forces,TestShip,NED)` ou `Mz(sum of forces,TestShip,TestShip)`

Une sortie `blocked states` est également disponible (uniquement dans le repère lié au corps). Elle correspond à l'effort
nécessaire pour maintenir des degrés de liberté en cas de forçage :

- `Fx(blocked states,corps,corps)`, par exemple `Fx(blocked states,TestShip,TestShip)`
- `Fy(blocked states,corps,corps)`, par exemple `Fy(blocked states,TestShip,TestShip)`
- `Fz(blocked states,corps,corps)`, par exemple `Fz(blocked states,TestShip,TestShip)`
- `Mx(blocked states,corps,corps)`, par exemple `Mx(blocked states,TestShip,TestShip)`
- `My(blocked states,corps,corps)`, par exemple `My(blocked states,TestShip,TestShip)`
- `Mz(blocked states,corps,corps)`, par exemple `Mz(blocked states,TestShip,TestShip)`

La somme des efforts de Coriolis et des efforts centripètes est notée `fictitious
forces`. Il s'agit des efforts dûs au caractère non-galliléen du repère lié au
corps (forces d'inertie d'entraînement et de Coriolis). Voir _Guidance And
Control of Ocean Vehicles_, ISBN 0 471 94113 1, Thor I. Fossen, Hydrodynamic
Forces And Moments equation (2.125) chapter 2.4.1 page 36 $`C_A(\nu)`$ pour le
détail du calcul.

- `Fx(fictitious forces,corps,repère)`, par exemple `Fx(fictitious forces,TestShip,NED)` ou `Fx(fictitious forces,TestShip,TestShip)`
- `Fy(fictitious forces,corps,repère)`, par exemple `Fy(fictitious forces,TestShip,NED)` ou `Fy(fictitious forces,TestShip,TestShip)`
- `Fz(fictitious forces,corps,repère)`, par exemple `Fz(fictitious forces,TestShip,NED)` ou `Fz(fictitious forces,TestShip,TestShip)`
- `Mx(fictitious forces,corps,repère)`, par exemple `Mx(fictitious forces,TestShip,NED)` ou `Mx(fictitious forces,TestShip,TestShip)`
- `My(fictitious forces,corps,repère)`, par exemple `My(fictitious forces,TestShip,NED)` ou `My(fictitious forces,TestShip,TestShip)`
- `Mz(fictitious forces,corps,repère)`, par exemple `Mz(fictitious forces,TestShip,NED)` ou `Mz(fictitious forces,TestShip,TestShip)`

### Exemple

Pour générer un fichier HDF5 contenant l'effort dû à la gravité agissant sur
un corps nommé `ball` projeté dans le repère du corps, ainsi que la somme des
moments autour de l'axe X, on utiliserait la section suivante :

```yaml
output:
   - format: hdf5
     filename: test.h5
     data: [Fx(gravity, ball, ball), Fy(gravity, ball, ball), Fz(gravity, ball, ball), Mx(sum of forces,ball,NED)]
```


#### Commandes

Les commandes des efforts qui en possèdent (propeller+rudder, manoeuvring,
etc.) peuvent également figurer dans les sorties. La syntaxe est la suivante :

`name(command)`

Par exemple, supposons que l'on utilise un modèle d'hélice et de safran définis
par le YAML suivant :

```yaml
external forces:
  - name: Prop. & rudder
    model: propeller+rudder
    position of propeller frame:
        frame: TestShip
        x: {value: -4, unit: m}
        y: {value: -2, unit: m}
        z: {value: 2, unit: m}
        phi: {value: 0, unit: rad}
        theta: {value: -10, unit: deg}
        psi: {value: -1, unit: deg}
    wake coefficient w: 0.9
    relative rotative efficiency etaR: 1
    thrust deduction factor t: 0.7
    rotation: clockwise
    number of blades: 3
    blade area ratio AE/A0: 0.5
    diameter: {value: 2, unit: m}
    rudder area: {value: 2.2, unit: m^2}
    rudder height: {value: 2, unit: m^2}
    effective aspect ratio factor: 1.7
    lift tuning coefficient: 2.1
    drag tuning coefficient: 1
    position of rudder in body frame:
        x: {value: -5.1, unit: m}
        y: {value: -2, unit: m}
        z: {value: 2, unit: m}
```

Dans ce cas, on peut récupérer la vitesse de rotation de l'hélice et l'angle du
safran en utilisant la section `output` suivante :

```yaml
output:
   - format: csv
     filename: propRudd.csv
     data: [t, 'Prop. & rudder(rpm)', 'Prop. & rudder(beta)']
```

#### Sorties supplémentaires pour le format HDF5

Lorsque l'on utilise le format HDF5 xdyn permet les sorties supplémentaires
suivantes :

- `mesh` : le maillage (format STL) est écrit dans le fichier de sortie (dans
  l'arborescence `/inputs/meshes`)
- `yaml` : concaténation des fichiers YAML fournis à xdyn (`/inputs/yaml/input`)
- `command line`: ligne de commande utilisée pour lancer la simulation
- `matlab scripts`: les scripts Matlab d'exploitation des fichiers HDF5
  (dans l'aborescence `/scripts/MatLab`)
- `python scripts`: les scripts Python d'exploitation des fichiers HDF5
  (dans l'aborescence `/scripts/Python`)
- `waves`: élévations de surface libre sur la grille définie dans la section
  `outputs` des modèles de houle. (`/outputs/waves/t`, `/outputs/waves/x`,
  `/outputs/waves/y` et `/outputs/waves/z`)
- `spectra`: (arborescence `/outputs/spectra/00/a`,
  `/outputs/spectra/00/gamma`, `/outputs/spectra/00/k`,
  `/outputs/spectra/00/omega`, `/outputs/spectra/00/phase`,
  `/outputs/spectra/01/`, etc. suivant le nombre de spectres)

Par exemple, pour enregistrer le maillage et la ligne de commande (mais pas
le YAML d'entrée) dans un fichier de sortie au format HDF5 on utilisera :

```yaml
output:
   - format: hdf5
     filename: test.h5
     data: [mesh, 'command line']
```

ou, de façon équivalente :

```yaml
output:
   - format: hdf5
     filename: test.h5
     data:
       - mesh
       - command line
```



# Interface MatLab

`xdyn` peut être appelé depuis le logiciel `MatLab`.
Cela présente l'avantage de disposer dans la foulée d'un environnement graphique
pour afficher les résultats de simulation.

Voici les fonctions de base pour travailler avec `xdyn`.

- `xdyn_run` exécute `xdyn` depuis `MatLab`.
- `xdyn_loadResultsFromHdf5File` permet de charger les résultats d'une
  simulation dans `MatLab`.
- `xdyn_postProcess` lance l'ensemble des post-traitements disponibles.
- `xdyn_plotStates` permet de tracer les états, à partir de résultats de
  simulations. Cela comprend les positions et les vitesses de chaque corps.
- `xdyn_plotPositions` permet de tracer les positions et les orientations
   de chaque corps.
- `xdyn_plotVelocities` permet de tracer les vitesses de translation et de
   rotation de chaque corps dans le repère de chaque repère.
- `xdyn_animate3d` permet de lancer une animation 3d d'une simulation avec
  les objets simulés et le champ de vagues lorsque celui-ci est exporté.
- `xdyn_extractMatLabScript` permet d'extraire les scripts `MatLab` de
  dépouillement intégrés dans le fichier de sortie hdf5.

Enfin, le fichier `xdyn_demos.m` permet de lancer les différents tutoriels.

Il est également possible d'utiliser l'environnement `MatLab` pour analyser des
simulations déjà réalisées.
Par exemple, le script `xdyn_demos.m` va lire le fichier
`tutorial_01_falling_ball.h5` qui aura préalablement été généré à partir
de la commande :

```bash
./xdyn tutorial_01_falling_ball.yml --dt 0.1 --tend 1 -o tutorial_01_falling_ball.h5
```

# Interface Docker et génération automatique de rapports

`xdyn` peut être utilisé sous la forme d'un conteneur
[Docker](https://www.docker.com/)
ce qui permet de l'exécuter sur n'importe quelle plateforme.

En outre, c'est actuellement le seul moyen d'utiliser le module de génération
automatique de rapport décrit ci-après.

Pour utiliser l'image `xdyn`, il faut d'abord installer Docker puis récupérer
l'image `sirehna/xdyn` en exécutant :

```bash
docker pull sirehna/xdyn
```

Pour utiliser l'image `xdyn` avec son environnement de post-traitement,
on récupérera l'image `sirehna/xweave` en exécutant :

```bash
docker pull sirehna/xweave
```

## Lancement d'xdyn via l'image docker

Une fois l'image chargée par la commande précédente, on peut lancer :

```bash
docker run -it --rm -v $(pwd):/work -w /work sirehna/xdyn
```

- le paramètre `-v $(pwd):/work` permet de faire correspondre le répertoire courant avec le répertoire `/work` du conteneur,
- le paramètre `-w /work` précise que le répertoire de travail du conteneur (celui depuis lequel sera exécuté xdyn) est `/work`,
- `sirehna/xdyn` correspond au nom de l'image Docker,

Si l'on souhaite lancer une simulation, on ajoute les arguments d'xdyn à la suite :

```bash
docker run -it --rm -v $(pwd):/work -w /work sirehna/xdyn tutorial_01_falling_ball.yml --dt 0.1 --tend 1 -o tsv
```

## Génération automatique de rapports (X-Weave)

Cette fonctionnalité n'est accessible qu'en utilisant le conteneur Docker
`sirehna/xweave`.


### Principe de fonctionnement

On utilise [Pweave](http://mpastell.com/pweave/) pour inclure des balises Python
dans du texte au format [Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown)).
Par exemple, la présente documentation est générée en utilisant
ce système, y compris les tutoriels et leurs courbes de résultats.

Grâce à ce générateur (appelé "x-weave"), on peut obtenir des rapports dans le
format que l'on souhaite (DOCX, PDF, HTML...). Ces rapports peuvent par exemple
lancer des simulations xdyn et inclure des courbes à partir de ces résultats.

L'intérêt est que l'on peut gérer le code de ses rapports en configuration (puisque
c'est du texte), regarder facilement les différences entre deux versions sans
utiliser d'outil propriétaire et regénérer à la demande une nouvelle version
du rapport lorsque l'on change les entrées.

X-Weave n'est ni plus ni moins que Pweave dans un containeur Docker avec xdyn
pré-installé, ce qui permet de lancer xdyn depuis Pweave.

### Ligne de commande

La ligne de commande à utiliser est :

```bash
docker run --rm -it -v $(pwd):/work -w /work -u $(id -u):$(id -g) --entrypoint /usr/bin/xdyn-weave sirehna/xweave -f markdown concatenated_doc.pmd -o concatenated_doc.md
```

- La première partie de la ligne de commande (`docker run --rm -it -v
    $(pwd):/work -w /work -u $(id -u):$(id -g) --entrypoint /usr/bin/xdyn-weave
    sirehna/xweave`) contient les arguments passés à Docker pour partager le
    dossier courant avec le containeur et créer les fichiers de sortie avec les
    permissions de l'utilisateur courant.
- La partie après `sirehna/xweave` contient les arguments passés à pweave. Une
    liste complète de ces arguments peut être obtenue en exécutant `docker run
    --rm -it -v $(pwd):/work -w /work -u $(id -u):$(id -g) --entrypoint
    /usr/bin/xdyn-weave sirehna/xweave -h` ou en consultant [la documentation
    de Pweave](http://mpastell.com/pweave/docs.html).

On obtient alors un fichier Markdown et (le cas échéant) des images. On peut
ensuite utiliser [Pandoc](https://pandoc.org/) pour convertir le Markdown au
format de son choix en utilisant le Pandoc installé dans le containeur X-Weave
:

```bash
docker run --rm -it -v $(shell pwd):/work -w /work -u $(shell id -u):$(shell id -g) --entrypoint /usr/bin/pandoc xweave concatenated_doc_pandoc.md -o doc.html
```

### Commandes Python supplémentaires

Par rapport à Pweave "classique", on a inclus quelques fonctions Python pour
simplifier le travail avec xdyn.

#### Chargement du fichier YAML : `load_yaml`

L'insertion de la section suivante dans un document X-weave ne génèrera pas de
sortie. Par contre, dans les sections `python` ultérieures, on pourra utiliser
les données ainsi chargées.

```python echo=False, results='markdown', name='example-load-yaml'
print('~' * 4 + '{.markdown}')
print('`' * 3 + 'python')
print("yaml_data = load_yaml('tutorial_06_1D_propulsion.yml')")
print('`' * 3)
print('~' * 4)
```

#### Affichage de l'intégrabilité du YAML : `print_yaml`

Suite au chargement du YAML, on peut vouloir l'afficher dans le document.
Pour cela, on utilise une section `python` de la forme :


```python echo=False, results='markdown', name='example-print-yaml'
print('~' * 4 + '{.markdown}')
print('`' * 3 + 'python')
print("yaml_data = load_yaml('tutorial_06_1D_propulsion.yml')")
print("")
print("# Pour afficher l'intégralité du YAML avec de la coloration syntaxique")
print("print_yaml(yaml_data)")
print("")
print("# Pour n'afficher qu'une sous-section du YAML")
print("print_yaml(yaml_data, 'bodies/0/external forces')")
print('`' * 3)
print('~' * 4)
```

#### Exécution d'une simulation

La commande suivante ne produira pas de contenu visible dans le document. En
revanche, il sera possible de récupérer et afficher les données ainsi générées.

```python echo=False, results='markdown', name='example-run-simulation'
print('~' * 4 + '{.markdown}')
print('`' * 3 + 'python')
print("execCommand('xdyn tutorial_01_falling_ball.yml --dt 0.01 --tend 1 -o out.csv')")
print('`' * 3)
print('~' * 4)
```

#### Tracé des résultats

```python echo=False, results='markdown', name='example-plot-results'
print('~' * 4 + '{.markdown}')
print('`' * 3 + 'python')
print('# Récupération des données générées')
print("data = csv('out.csv')")
print('')
print('# Définition de la donnée à tracer')
print("plot = prepare_plot_data(data, x = 't', y = 'z(ball)', name='Résultat')")
print('')
print('# Définition du type de graph')
print("g = cartesian_graph([plot], x='t (s)', y='Élévation (m)')")
print('')
print('# Tracé de la planche')
print("create_layout(g, title='Élévation au cours du temps')")
print('`' * 3)
print('~' * 4)
```

## Utilisation de xdyn en mode serveur

### Description

Il est possible de lancer xdyn en tant que serveur, afin de l'intégrer à un
autre environnement de simulation. L'utilisation d'un serveur plutôt qu'une
[MEX-fonction](https://www.mathworks.com/help/matlab/matlab_external/introducing-mex-files.html) (*MATLAB executable* - fonction) ou un [FMU](https://fmi-standard.org/) (*Functional Mock-up Unit*) permet d'exécuter xdyn sur une autre machine et de
l'interroger par plusieurs clients.

Il s'agit d'une utilisation en "model exchange" (au sens de la [spécification
"Functional Mockup
Interface"](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&cad=rja&uact=8&ved=2ahUKEwimvuWL6tDeAhUC1hoKHWiwALAQFjABegQIBBAC&url=https%3A%2F%2Fsvn.modelica.org%2Ffmi%2Fbranches%2Fpublic%2Fspecifications%2Fv2.0%2FFMI_for_ModelExchange_and_CoSimulation_v2.0.pdf&usg=AOvVaw2ePLxrLtnb42qW1aLIVoov)),
par opposition à "xdyn for co-simulation". La différence se situe dans
l'utilisation du solveur : dans le cas de la co-simulation, on utilise le
solveur interne de xdyn (le serveur renvoie les états intégrés au pas suivant
$`X(t+dt)`$). Dans le cas "model exchange", le serveur renvoie la dérivée
des états $`\frac{dX}{dt}`$.

### Protocole utilisé

Les serveurs "model exchange" et "co-simulation" peuvent être lancés soit en
mode "JSON + websocket" (par défaut), soit en mode "gRPC", en utilisant le
paramètre `--grpc` sur la ligne de commande.

- L'utilisation des websockets permet des temps de réponse plus courts (puisque
  c'est un protocole assez léger, comparé au HTTP par exemple). Les messages
  envoyés sont en JSON, pour offrir un bon compromis entre la verbosité (moins
  que du XML mais plus qu'un format binaire) et une utilisation plus aisée
  qu'un format binaire, quitte à sacrifier un peu de performance. La façon
  choisie de sérialiser les nombres en chaînes de caractère nous assure d'une
  part que deux nombres flottants différents donneront deux chaînes de
  caractères différentes et d'autre part que lorsque l'on relit une telle
  réprésentation textuelle, le nombre flottant ainsi obtenu ne peut être
  confondu avec aucun autre nombre flottant.
- Le format [gRPC](https://grpc.io/) est un format binaire performant pour
  l'appel distant de procédures, ce qui permet d'appeler xdyn depuis une
  [grande variété de langages](https://grpc.io/docs/languages/) et de plateformes.
  Le gRPC utilise le protocole HTTP/2, et non les websockets. Les websockets
  [utilisent un masque](https://tools.ietf.org/html/rfc6455#section-10.3) qui
  leur imposent de parcourir les messages entièrement pour masquer le contenu,
  et le client doit faire l'opération inverse, ce qui prend du temps : le gRPC
  n'a pas ce problème. Le gRPC utilise le format [protocol
  buffers](https://developers.google.com/protocol-buffers) pour encoder les
  messages ce qui est plus efficace et clarifie les interfaces.

### Lancement du serveur "Model Exchange"

Le serveur est lancé grâce à l'exécutable `xdyn-for-me` (xdyn for Model Exchange)
avec un ou plusieurs fichiers YAML en paramètre : une fois lancé, ce
serveur ne simulera donc qu'une seule configuration de l'environnement et du
navire. Concrètement, on lance le serveur comme suit :

```bash
./xdyn-for-me --port 9002 tutorial_01_falling_ball.yml
```

où `--port` sert à définir le port sur lequel écoute le serveur websocket.

Pour lancer ce serveur en mode gRPC, on remplace la ligne précédente par :

```bash
./xdyn-for-me --grpc --port 9002 tutorial_01_falling_ball.yml
```

La liste complète des options avec leur description est obtenue en lançant
l'exécutable avec le flag `-h`.

Ensuite, on peut se connecter à l'adresse du serveur pour l'interroger.

### Lancement du serveur "Co-Simulation"

Le serveur est lancé grâce à l'exécutable `xdyn-for-cs` (xdyn for
Co-Simulation) avec un ou plusieurs fichiers YAML en paramètre, un pas de temps
et un solveur : une fois lancé, ce serveur ne simulera donc qu'une seule
configuration de l'environnement et du navire, en utilisant un solveur et
un pas de temps. Concrètement, on lance le serveur comme suit :

```bash
./xdyn-for-cs --port 9002 tutorial_01_falling_ball.yml --dt 0.1
```

où `--port` sert à définir le port sur lequel écoute le serveur websocket.

La liste complète des options avec leur description est obtenue en lançant
l'exécutable avec le flag `-h`.

Pour lancer ce serveur en mode gRPC, on remplace la ligne précédente par :

```bash
./xdyn-for-cs --grpc --port 9002 tutorial_01_falling_ball.yml --dt 0.1
```

Ensuite, on peut se connecter à l'adresse du serveur pour l'interroger.


### Utilisation avec Chrome

Le navigateur Chrome dispose d'une extension websocket [Simple Websocket
Client](https://chrome.google.com/webstore/detail/simple-websocket-client/pfdhoblngboilpfeibdedpjgfnlcodoo?hl=en)
qui permet de faire quelques tests de bon fonctionnement.

### Utilisation avec MatLab

On initie une connexion websocket via MatLab en utilisant par exemple
[MatLabWebSocket](https://github.com/jebej/MatlabWebSocket).
Il faut également pouvoir encoder et décoder du JSON en MatLab, par exemple en
utilisant les fonctions MatLab [jsondecode et
jsonencode](https://fr.mathworks.com/help/matlab/json-format.html).

### Description des entrées/sorties pour une utilisation en "Model Exchange" (x -> dx/dt)

Dans ce mode, xdyn calcule uniquement la dérivée des 13 états navire mais
n'effectue pas l'intégration numérique, ce qui permet d'utiliser un solveur
externe, par exemple Matlab ou Simulink.

#### Interface JSON

| Entrées    | Type                                                             | Détail                                                                                                                                                                                                                                                                  |
| ---------- | ---------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `states`   | Liste d’éléments de type « État »                                | Historique des états jusqu’au temps courant t. Si les modèles utilisés ne nécessitent pas d'historique, cette liste peut n'avoir qu'un seul élément.                                                                                                                    |
| `commands` | Liste de clefs-valeurs (dictionnaire)                            | État des actionneurs au temps t                                                                 |
| `requested_output` | Liste de chaînes de caractères             | Valeurs calculées par xdyn à renvoyer par le serveur (par exemples, les composantes des forces forces, avec la même syntaxe que pour la section 'output' du fichier YAML d'entrée) |

Chaque élément de type « État » est composé des éléments suivants:

| État  | Type      | Détail                                                                                                                                                                                                                                                                  |
| ----- | --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `t`   | Flottant  | Temps courant de la simulation                                                                                                                                                                                                                                          |
| `x`   | Flottant  | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du [repère body](#rep%C3%A8re-navire-mobile-ou-body-ou-rep%C3%A8re-de-r%C3%A9solution) |
| `y`   | Flottant  | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY                                                                         |
| `z`   | Flottant  | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY                                                                         |
| `u`   | Flottant  | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                                                                                             |
| `v`   | Flottant  | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                                                                                             |
| `w`   | Flottant  | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                                                                                             |
| `p`   | Flottant  | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                                                                                                 |
| `q`   | Flottant  | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                                                                                                 |
| `r`   | Flottant  | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                                                                                                 |
| `qr`  | Flottant  | Partie réelle du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                                                                                                             |
| `qi`  | Flottant  | Première partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                                                                                                |
| `qj`  | Flottant  | Seconde partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                                                                                                 |
| `qk`  | Flottant  | Troisième partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                                                                                               |

Exemple d'entrée:

```json
{
  "states": [
    {
      "t": 0,
      "x": 0,
      "y": 8,
      "z": 12,
      "u": 1,
      "v": 0,
      "w": 0,
      "p": 0,
      "q": 1,
      "r": 0,
      "qr": 1,
      "qi": 0,
      "qj": 0,
      "qk": 0
    },
    {
      "t": 0.1,
      "x": 0.1,
      "y": -0.156,
      "z": 10,
      "u": 0,
      "v": 0,
      "w": 0,
      "p": 0,
      "q": 0,
      "r": 0,
      "qr": 0,
      "qi": 0,
      "qj": 0,
      "qk": 0,
    }
  ],
  "commands": {
    "beta": 0.1
  },
  "requested_output": [
    "Fz(gravity,body,NED)",
    "My(non-linear hydrostatic (exact),body,body)"
  ]
}
```

La sortie du "Model Exchange" correspond à la dérivée des états
à savoir `dx/dt`, `dy/dt`, `dz/dt`, `du/dt`, `dv/dt`, `dw/dt`,
`dp/dt`, `dq/dt`, `dr/dt`, `dqr/dt`, `dqi/dt`, `dqj/dt`, `dqk/dt`, `dphi/dt`, `dtheta/dt`, `dpsi/dt`.

| Sorties   | Type      | Détail                                                                                                                                                                                                                             |
| --------- | --------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `dx/dt`   | Flottant  | Dérivée par rapport au temps de la projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY |
| `dy/dt`   | Flottant  | Dérivée par rapport au temps de la projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY |
| `dz/dt`   | Flottant  | Dérivée par rapport au temps de la projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY |
| `du/dt`   | Flottant  | Dérivée par rapport au temps de la projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                     |
| `dv/dt`   | Flottant  | Dérivée par rapport au temps de la projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                     |
| `dw/dt`   | Flottant  | Dérivée par rapport au temps de la projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                     |
| `dp/dt`   | Flottant  | Dérivée par rapport au temps de la projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                         |
| `dq/dt`   | Flottant  | Dérivée par rapport au temps de la projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                         |
| `dr/dt`   | Flottant  | Dérivée par rapport au temps de la projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                         |
| `dqr/dt`  | Flottant  | Dérivée par rapport au temps de la partie réelle du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                                     |
| `dqi/dt`  | Flottant  | Dérivée par rapport au temps de la première partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                        |
| `dqj/dt`  | Flottant  | Dérivée par rapport au temps de la seconde partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                         |
| `dqk/dt`  | Flottant  | Dérivée par rapport au temps de la troisième partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                       |
| `dphi/dt`  | Flottant  | Dérivée par rapport au temps de l'angle de roulis/gîte du navire par rapport au sol (BODY/NED)                                                                                       |
| `dtheta/dt`  | Flottant  | Dérivée par rapport au temps de l'angle de tangage/assiette par rapport au sol (BODY/NED)                                                                                       |
| `dpsi/dt`  | Flottant  | Dérivée par rapport au temps de l'angle de lacet par rapport au sol (BODY/NED)                                                                                       |

Il est important de noter que les dérivées des angles d'Euler sont calculées selon la convention d'angles `z, y', x''` (`id = 12` dans le tableau des conventions). Les autres conventions ne sont pas prises en charge pour le moment.

En plus des dérivées des états, le serveur renvoie dans la section "extra_observations" du JSON les valeurs des variables demandées dans la section "requested_output" de la requête.

Exemple de sortie:

```json
{
  "dx_dt": 0.1,
  "dy_dt": -0.156,
  "dz_dt": 10,
  "du_dt": 0,
  "dv_dt": 0,
  "dw_dt": 0,
  "dp_dt": 0,
  "dq_dt": 0,
  "dr_dt": 0,
  "dqr_dt": 0,
  "dqi_dt": 0,
  "dqj_dt": 0,
  "dqk_dt": 0,
  "dphi_dt": 0,
  "dtheta_dt": 0,
  "dpsi_dt": 0,
  "extra_observations": {
    "Fz(gravity,body,NED)": 2.135e3,
    "My(non-linear hydrostatic (exact),body,body)": 4.984e4
  }
}
```

La représentation textuelle de ces nombres flottants est faite de façon unique (et non
exacte) : cela signifie que si les flottants sont différents (binairement),
leur représentation textuelle sera différente. Cela signifie également que si xdyn lit
la représentation textuelle et la traduit en binaire, on retrouvera bien la valeur binaire
initiale. En d'autres termes, la fonction flottant -> texte est injective. Cela
n'implique pas qu'elle soit bijective, puisque si l'on part d'une
représentation textuelle, que l'on convertit en binaire pour reconvertir ensuite en
texte on ne retrouvera pas nécessairement le texte initial.

L'interface gRPC est décrite par le [fichier proto](https://developers.google.com/protocol-buffers/docs/proto3) suivant :

~~~~{.protobuf}
include({{model_exchange.proto}})
~~~~

### Description des entrées/sorties pour une utilisation en "Co-Simulation" (x(t) -> [x(t), ...,x(t+Dt)])

| Entrées    | Type                                   | Détail                                                                                                                                                  |
| ---------- | -------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `Dt`       | Flottant strictement positif           | Horizon de simulation (en secondes). La simulation s'effectue de t0 à t0 + Dt, où t0 est la date du dernier élément de la                               |
|            |                                        | liste `states`, par pas de `dt`, où `dt` est spécifié sur la ligne de commande. L'état t0 est donc présent à                                            |
|            |                                        | la fois dans les entrées et dans les sorties, avec la même valeur.                                                                                      |
| `states`   | Liste d'éléments de type « État »      | Historique des états jusqu'au temps courant `t`. Si les modèles utilisés ne nécessitent pas d'historique, cette liste peut n'avoir qu'un seul élément.  |
| `commands` | Liste de clefs-valeurs (dictionnaire)  | État des actionneurs au temps `t`. Commande au sens de xdyn (modèle d'effort commandé) au temps t0 (début de la                                        |
|            |                                        | simulation, i.e. date du dernier élément de la liste `states`). Le plus souvent, correspond à l'état interne                                            |
|            |                                        | d'un modèle d'actionneur (safran ou hélice par exemple) dans xdyn et dont on souhaite simuler la dynamique                                             |
|            |                                        | en dehors d'xdyn. |
| `requested_output` | Liste de chaînes de caractères  | Valeurs calculées par xdyn à renvoyer par le serveur (par exemples, les composantes des forces forces, avec la même syntaxe que pour la section 'output' du fichier YAML d'entrée) |

Chaque élément de type « État » est composé des éléments suivants:

| État  | Type      | Détail                                                                                                                                                                                          |
| ----- | --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `t`   | Flottant  | Date de l'état                                                                                                                                                                                  |
| `x`   | Flottant  | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY |
| `y`   | Flottant  | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY |
| `z`   | Flottant  | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY |
| `u`   | Flottant  | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                     |
| `v`   | Flottant  | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                     |
| `w`   | Flottant  | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                     |
| `p`   | Flottant  | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                         |
| `q`   | Flottant  | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                         |
| `r`   | Flottant  | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                         |
| `qr`  | Flottant  | Partie réelle du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                                     |
| `qi`  | Flottant  | Première partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                        |
| `qj`  | Flottant  | Seconde partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                         |
| `qk`  | Flottant  | Troisième partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                       |

Exemple d'entrée:

```json
{
  "Dt": 10,
  "states": [
    {
      "t": 0,
      "x": 0,
      "y": 8,
      "z": 12,
      "u": 1,
      "v": 0,
      "w": 0,
      "p": 0,
      "q": 1,
      "r": 0,
      "qr": 1,
      "qi": 0,
      "qj": 0,
      "qk": 0
    },
    {
      "t": 0,
      "x": 0.1,
      "y": -0.156,
      "z": 10,
      "u": 0,
      "v": 0,
      "w": 0,
      "p": 0,
      "q": 0,
      "r": 0,
      "qr": 0,
      "qi": 0,
      "qj": 0,
      "qk": 0,
    }
  ],
  "commands": {
    "beta": 0.1
  },
  "requested_output": [
    "Fz(gravity,body,NED)",
    "My(non-linear hydrostatic (exact),body,body)"
  ]
}
```

La sortie est une liste d'éléments de type « État augmenté » contenant
l'historique des états de `t0` à `t0 + Dt` par pas de `dt`.

| État     | Type      | Détail                                                                                                                                                                                          |
| -------- | --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `t`      | Flottant  | Date de l'état augmenté                                                                                                                                                                         |
| `x`      | Flottant  | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY |
| `y`      | Flottant  | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY |
| `z`      | Flottant  | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur entre l'origine du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) et l'origine du repère BODY |
| `u`      | Flottant  | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                     |
| `v`      | Flottant  | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                     |
| `w`      | Flottant  | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse du navire par rapport au sol (BODY/NED).                                                     |
| `p`      | Flottant  | Projection sur l'axe X du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                         |
| `q`      | Flottant  | Projection sur l'axe Y du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                         |
| `r`      | Flottant  | Projection sur l'axe Z du [repère NED](#rep%C3%A8re-de-r%C3%A9f%C3%A9rence-ned) du vecteur vitesse de rotation du navire par rapport au sol (BODY/NED).                                         |
| `qr`     | Flottant  | Partie réelle du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                                     |
| `qi`     | Flottant  | Première partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                        |
| `qj`     | Flottant  | Seconde partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                         |
| `qk`     | Flottant  | Troisième partie imaginaire du quaternion définissant la rotation du navire par rapport au sol (BODY/NED)                                                                                       |
| `phi`    | Flottant  | Angle d'Euler phi. Lire note ci-après                                                                                                                                                           |
| `theta`  | Flottant  | Angle d'Euler theta. Lire note ci-après                                                                                                                                                           |
| `psi`    | Flottant  | Angle d'Euler psi. Lire note ci-après                                                                                                                                                           |

La signification exacte des angles d'Euler dépend de la convention d'angle
choisie dans le fichier YAML d'entrée de xdyn (voir la section correspondante
dans la documentation). Cette sortie est fournie pour faciliter
le travail du client du serveur, mais n'est pas utilisée en interne par xdyn.

Exemple de sortie:

```json
{
  "t": [10,10.1],
  "x": [0,0.0999968],
  "y": [8,8],
  "z": [12,12.0491],
  "u": [1,0.897068],
  "v": [0,0],
  "w": [0,1.07593],
  "p": [0,0],
  "q": [1,1],
  "r": [0,0],
  "qr": [1,0.99875],
  "qi": [0,0],
  "qj": [0,0.0499792],
  "qk": [0,0],
  "phi": [0,0],
  "theta": [0,0.1],
  "psi": [0,0],
  "extra_observations": {
    "Fz(gravity,body,NED)": [2.135e3, 2.135e3],
    "My(non-linear hydrostatic (exact),body,body)": [4.984e4, 5.247e4]
  }
}
```

Comme pour le "model exchange", la représentation textuelle de ces nombres
flottants est faite de façon unique (et non exacte) : cela signifie que si les
flottants sont différents (binairement), leur représentation textuelle sera
différente. Cela signifie également que si xdyn lit la représentation textuelle
et la traduit en binaire, on retrouvera bien la valeur binaire initiale. En
d'autres termes, la fonction flottant -> texte est injective. Cela n'implique
pas qu'elle soit bijective, puisque si l'on part d'une représentation
textuelle, que l'on convertit en binaire pour reconvertir ensuite en texte on
ne retrouvera pas nécessairement le texte initial.

L'interface gRPC est décrite par le [fichier proto](https://developers.google.com/protocol-buffers/docs/proto3) suivant :

~~~~{.protobuf}
include({{cosimulation.proto}})
~~~~
